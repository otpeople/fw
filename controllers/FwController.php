<?php

namespace fw\controllers;

use fw\core\Controller;
use fw\core\Log;
use fw\core\Assets;
use fw\core\Cryptor;


class FwController extends Controller {


  //показывает файл
  public function showFile($img) {

    $file = Cryptor::decrypt($img);

    $imgsize = getimagesize($file);

    $this -> render('showFile',[
      'img'   => Assets::img($file),
      'width' => $imgsize[0],
      'height'=> $imgsize[1],
    ]);

  }


  //редактирует файл
  public function cropFile($img) {

    $file = Cryptor::decrypt($img);


    if(isset($_POST["data"])) {

      $data = json_decode($_POST["data"],true);

      Assets::crop($file, $data["x"], $data["y"], $data["width"], $data["height"]);

    } else {

      $this -> render('cropFile',[
        'img' => Assets::img($file),
        'crypt' => $img,
      ]);

    }

  }


  public function livereload() {

    $tick = 0.25;

    $time = 0;

    $files = json_decode($_POST["files"]);
    $assets = json_decode($_POST["assets"]);


    for($i=0;$i<(50*$tick);$i+=$tick) {

      //очищаем кеш для filemtime
      clearstatcache();

      //Log::debug($files);

      foreach($files as $file) {

        if(!file_exists($file)) continue;

        $filemtime = filemtime($file);

        if($filemtime > $time) {

          if($i==0) {
            $time = $filemtime;
          } else {

            //если обновлены стили - подкружаем новые без перезагрузки
            if(strpos($file, '.css') || strpos($file, '.styl')) {

              //Log::debug($assets);

              Assets::clear();
              foreach($assets as $asset) {
                //Log::debug($asset);
                Assets::add(...$asset);
              }


              $_css = [];
              foreach(Assets::$files["css"] as $css) {
                $css = str_replace(DIR.'/public/', '/', $css);
                //Log::debug($css);
                $_css[] = $css;
              }
              $this->json([
                'type' => 'css',
                'data' => $_css,
              ]);
              $this->response->send();
              exit;
            } else {
              $this->json(['type' => 'reload']);
              $this->response->send();
              exit;
            }

          }

        }



      }
      usleep($tick*1000000);

    }



  }


  public function off($title, $message) {
    $this->render('off',[
      'title' => $title,
      'message' => $message,
    ]);
  }




}
