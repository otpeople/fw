<?php

namespace fw\controllers;

use fw\core\Controller;
use fw\core\Access;
use fw\core\Table;
use fw\core\App;
use fw\help\Func as f;
use fw\core\File;
use fw\core\Log;
use fw\tables\Logins;


class LoginController extends Controller {

  public $public = [];

  public function index($model, $fields) {


    if(strpos($this->request->server->get('REQUEST_URI'), 'logout') !== false) {
      header("Location: /");
      exit;
    }


    $model = f::table($model);


    if($_SERVER['REQUEST_METHOD'] == 'POST') {
      $user = $this -> check($model, $fields);
      if($user) {
        $this -> login($model, $user);
      } else {
        $this -> error($model, $fields);
      }
    }
    else {
      $this -> form($model, $fields);
    }



  }

  //ошибка входа
  private function error($model, $fields) {
    $ff = [];
    $def = [];
    foreach($fields as $f) {
      $ff[] = $model->fields[$f]["title"];
      if($model->fields[$f]['type'] != 'password') {
        $def[$f] = $_POST[$f];
      }
    }
    $error = "Ошибка: ".implode(' / ', $ff);

    //сохраняем лог попытки входа
    Logins::init() -> insert([
      'ip'   => $_SERVER["REMOTE_ADDR"],
      'agent' => $_SERVER["HTTP_USER_AGENT"],
      'time'  => date('Y-m-d H:i:s'),
      'module'=> MOD,
      'model' => str_replace('\\','\\\\',$model->class),
      "user"  => 0,
      "comment" => json_encode($_POST, JSON_UNESCAPED_UNICODE)
    ]);

    //перенаправляем опять на форму
    $this -> form($model, $fields, $error, $def);

  }

  //страница со входом
  private function form($model, $fields, $error=null, $def=[]) {


    $ff = [];
    foreach($fields as $field) {
      if(isset($model->fields[$field])) {
        $ff[] = [
          'name'  => $field,
          'title' => $model->fields[$field]["title"],
          'type'  => $model->fields[$field]["type"],
        ];
      } else {
        App::err(500);
      }
    }


    //устанавливаем токен для входа, который действует 300 сек
    $token = File::setCache(true, 300);


    $this -> render("index",[
      'fields' => $ff,
      'token'  => $token,
      'error'  => $error,
      'def'    => $def
    ]);

  }

  //проверяет введенные данные
  private function check($model, $fields) {



    //если не передан токен
    if(!isset($_POST["token"])) {
      Log::warn("Модуль ".MOD.": Попытка входа без POST токена");
      return false;
    }

    //если не передан
    $params = [];
    foreach($fields as $field) {

      //если не введено одно из полей
      if(!isset($_POST[$field]) || empty($_POST[$field])) {
        Log::warn("Модуль ".MOD.": При авторизации не указано поле $field");
        return false;
      }

      $params[$field] = $_POST[$field];

      //если есть лишние символы
      //if(!preg_match('^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$', $_POST[$field])) {
      //  return false;
      //}

      $model -> is($field, $_POST[$field]);
    }

    //прооверяем кеш и удаляем его
    if(!File::getCache($_POST["token"], true, false)) {
      Log::notice("Модуль ".MOD.": При авторизации временного файла на сервере не обнаружено\n\t\t\t". json_encode($_POST));
      return false;
    }

    //проверяем данные в таблице
    if($id = $model -> one('id')) {
      return $id;
    } else {
      Log::notice("Модуль ".MOD.": При авторизации неверно указаны имя/пароль \n\t\t\t". json_encode($params));
      return false;
    }



  }

  //осуществляет вход
  private function login($model, $user) {

    $logins = Logins::init();

    //создаем файл сессии
    $dev = ($user === 1) ? true : false;
    $session = File::setCache([
      'ip' => $_SERVER["REMOTE_ADDR"],
      'agent' => $_SERVER["HTTP_USER_AGENT"],
      'user'  => $user,
      'dev'  => $dev
    ], 3600*24*30);  // на месяц


    //логируем успешную попытку входа

    $logins -> clear() -> insert([
      'ip'   => $_SERVER["REMOTE_ADDR"],
      'agent' => $_SERVER["HTTP_USER_AGENT"],
      'time'  => date('Y-m-d H:i:s'),
      'module'=> MOD,
      'model' => str_replace('\\','\\\\',$model->class),
      'user'  => $user,
      'status'  => 1,
      'session' => $session,
    ]);

    // TODO передалать на HttpFoundation

    //устанавливаем куку
    setcookie(Access::SESSION, $session, time()+(3600*24*30), '/');



    header("Location: ".$_SERVER["REQUEST_URI"]);
    exit;

  }
}
