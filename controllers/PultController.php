<?php

namespace fw\controllers;

use fw\core\Controller;
use fw\core\Access;
use fw\core\Log;

use fw\models\pult\TabsModel;
use fw\models\pult\PultWs;

abstract class PultController extends Controller {





  //главная страница
  public function index() {

    $err = [];
    if(!isset($this->title))         $err[] = ['title','Заголовок главной страницы'];
    if(!isset($this->chatUsers))     $err[] = ['chatUsers','Таблица пользователей'];
    if(!isset($this->chatGroups))   $err[] = ['chatGroups','Таблица групп для чата'];
    if(!isset($this->chatRooms))     $err[] = ['chatRooms','Таблица комнат для чата'];
    if(!isset($this->chatMessages)) $err[] = ['chatMessages','Таблица сообщений для чата'];

    if(!empty($err)) {
      foreach($err as $e) {
        Log::emerg("fw\controller\PultController - не указана переманная '{$e[0]}' ({$e[1]})");
      }
      return false;
    }


    $this -> render('fw/pult/index',[
      'user' => Access::getUserId(),
      'title' => $this->title,
    ]);

  }


  //получить доступные вкладки
  public function gettabs() {
    $this->json(TabsModel::get());
  }


  public function ws() {

    $this->websocket(PultWs::class, [
      'chatUsers'      => $this->chatUsers,
      'chatGroups'      => $this->chatGroups,
      'chatRooms'      => $this->chatRooms,
      'chatMessages'    => $this->chatMessages,
    ]);
  }


  //выход из системы
  public function logout() {
    Access::logout();
    header("Location: /");
  }



}
