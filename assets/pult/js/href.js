$(function() {

  //включаем fastclick
  if(FastClick) {
    FastClick.attach(document.body);
  }

  var doc, tab, modal, full, dialog, isOpenModal=false, modalZindex=2000;


    //при клике на элемент
  $(document).on("click", "[href]", function(e) {

    var target, href;

    //если это не тег А
    if(e.target.tagName != 'A') {


      //определяем target и ссылку
      target = ($(this).attr("target")) ? $(this).attr("target") : 'modal';
      href   = $(this).attr('href');

      //если href пустой
      if(!href) {
        e.preventDefault();
        e.stopPropagation();
        return false;
      }

      else if(href.indexOf('javascript')+1) {
        return false;
      }

      //если нужно открыть в iframe
      else if(target.indexOf("#") + 1) {
        var el = $(target);
        if($(el).is("iframe")) {
          $(el).attr("src",href);
        } else if(el.is("div")) {
          el.html('<iframe src="'+href+'" frameborder="0" width="100%" height="100%">');
        }

        e.preventDefault();
        e.stopPropagation();
        return false;
      }

        //определяем документ
      if(!doc) {
        doc = (window.top) ? window.top.document : window.document;
      }


      //вводим полный url ссылки
      if(href.charAt(0) !== '/' && href.substr(0, 4) !== 'http') {
        path = location.pathname;
        if(path.substr(-1) !== '/') {
          path = path.split('/')
          delete path[path.length-1]
          path = path.join('/')
        }
        href = path+href;
      }

      if(target == 'modal') {
        openModal(href);
      }
      else if(target == 'full') {
        openFull(href);
      }

    }

  });


  //открыть в модальном окне
  function openModal(href) {

      //создаем модель если она еще не создана
    //if(!modal) {

    //смотрим сколько всего modal-frames есть в body чтобы определить zIndex
    zIndex = 2000;
    $(doc).find('.fw-modal-frame').each(function() {
      zIndex++;
    });


    modal = $('<div style="z-index:'+zIndex+'" class="modal modal-fw fade" role="dialog" aria-labelledby="modal" aria-hidden="true">\
      <div class="modal-dialog">\
        <div class="modal-content">\
          <div class="modal-header">\
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
            <h4 class="modal-title"></h4>\
          </div>\
          <div class="modal-body body">\
            \
          </div>\
        </div>\
      </div>\
    </div>').appendTo( $(doc).find('body') );

    //при закрытии модального окна
    modal.on('hidden.bs.modal', function() {
      modal.find('.modal-dialog').css('opacity',0).find('iframe').remove();
      modal.delay(500).queue(function() {
        $(this).dequeue().remove();
      });
      isOpenModal = false;
    });

    //}

    modal.modal();

    $('.modal-backdrop')
      .css({zIndex: zIndex-1})
      .html('<i class="fa fa-circle-o-notch fa-spin"></i>').insertAfter(modal);
    modal.parent().addClass('modal-open');


    var iframe = $('<iframe class="fw-modal-frame" height="300"></iframe>').appendTo(modal.find('.modal-body'));
    var dialog = modal.find('.modal-dialog');


    iframe.attr('src', href).load(function() {



      var c = $(this).contents();
      $(c[0]).ready(function() {

        var win = iframe.get(0).contentWindow;
        win.modal = modal;


        //считываем параметры из фрейма
        var body   = c.find("body");
        var width  = body.width()  + 40; if(width == 40) { width = 1040; }
        var height = body.outerHeight();
        var title  = c[0].title;
        modal.find('.modal-title').html('<i></i> '+ title);

        body.prev().append('<style>html,body{overflow:hidden !important;width:100% !important}</style>');


        //выставляем высоту у iframe
        iframe.attr('height',height);

        //плавное появление диалогового окна
        dialog.animate({'opacity' : 1 },300);
        dialog.width(width);

        //убираем крутилку у бекдропа
        $('.modal-backdrop').html('');

        isOpenModal = true;

      });




    });


  }

    //октрыть во весь экран
  function openFull(href) {
    if(!full) {
      if($(doc).find('#fw-full-back').is('div')) {
        full = $(doc).find('#fw-full-back');
        console.log('find');
      } else {
        console.log('create');
        full = $('<div id="fw-full-back"><iframe></iframe><div class="close">&times;</div></div>').appendTo( $(doc).find('body') );
        full.find(".close").bind('click', function() {
          full.hide().find('iframe').removeAttr('src');
        });

        full.find('iframe').load(function() {
          $(this).contents().find('body').css({'background-color':'transparent'});
        });

      }

    }

    full.show().find('iframe').attr('src',href);

  }

    //открыть в диалоговом окне
  function openDialog($href) {

  }


  //если меняется размер содержимого модали - меняем iframe
  setInterval(function() {
    if(isOpenModal) {
      var iframe = $(doc).find('iframe.fw-modal-frame').last();
      var h = iframe.contents().find('body').height();
      var i = Number(iframe.attr('height'));
      if(h != i) { iframe.attr('height',h); }
    }
  }, 300);

});
