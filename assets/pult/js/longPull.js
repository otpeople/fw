;(function($){
	$.longPull = function(p) {
		
			//параметры по умолчанию
		if(!p) { p = {}; }
		if(!p.time) { p.time = 5000; }	//время одной итерации
		if(!p.url)  { console.log("URL not specified"); return false; }	//url
		if(!p.success) { p.success = function(data) { console.log(data); } }	//функция при получении данных
		if(!p.vars) { p.vars = null; }
		var get = function(step) {
			$.ajax({
				url: p.url,
				beforeSend : function(xhr) {
					if(step != 0) {
						return xhr.setRequestHeader('X-PULL-TIME', p.time);
					} else {
						return xhr.setRequestHeader('X-PULL-TIME', 0);
					}
				},
				success: function(data,stat,xhr) {
				
						//нет изменений - запускаем заного
					if(xhr.status == 304) {
						setTimeout(function() { get(1); }, p.time);
					}
					
						//есть ответ - обрабатываем
					else {
						p.success(data, p.vars);
						setTimeout(function() { get(1); }, p.time);
					}
				},
				error: function() {
					setTimeout(function() { get(1); }, p.time);
				}
				
			});
		}
		
		get(0);
		
	};
})(jQuery);