;(function($){
	$.toAngular = function(ctrl, model, data) {
		var el = document.querySelector('[ng-controller='+ctrl+']');
		var scope = angular.element(el).scope();
		scope.$apply(function () {
			scope[model] = data;
		});
	};
	
	
	
	
})(jQuery);


$(document).ready(function() {
	
	fw = window.angular.module('fw', []);


	fw.directive("fwLongPull", function() {
		return function(scope, element, attr) {
			
			$.longPull({
				url : attr["fwLongPull"],
				vars : scope,
				success : function(data) {
					scope.$apply(function() {
						scope.data = data;
					});
				}
			});
			
		};
	});
	
	
});

