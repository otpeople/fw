window.generateId = function (l) {
		if(!l) { l = 32; }
		var str = '';
		for(i=0;i<l;i++) {
			str += 'x';
		}
        return str.replace( /[xy]/g, function ( c ) {
            var r = Math.random() * 16 | 0;
            return ( c == 'x' ? r : ( r & 0x3 | 0x8 ) ).toString( 16 );
        } );
    };
$(function() {
	
	
	
	
	
	(function($){
		jQuery.fn.dataStorage = function(options){
		
			options = $.extend({
			
			}, options);
			
			var make = function(){
				
				var name = $(this).attr('data-storage');
		
				//пытаемся получить объект со всеми названиями
				var storage = localStorage.getItem('data-storage');
				if(!storage) {
					localStorage.setItem('data-storage',JSON.stringify({}));
					storage = localStorage.getItem('data-storage');
				}
				storage = JSON.parse(storage);
				
				if(storage[name] != null) {
					$(this).val(storage[name]);
					$(this).trigger('change');
				}
				
				//при срабатывании change
				$(this).change(function() {
					var name = $(this).attr('data-storage');
					var val = $(this).val();
					var storage = localStorage.getItem('data-storage');
					storage = JSON.parse(storage);
					storage[name] = $(this).val();
					localStorage.setItem('data-storage',JSON.stringify(storage));
				});
			};
		
			return this.each(make); 
		};
	})(jQuery);
	
	
	
	$("[data-storage]").each(function() {
		$(this).dataStorage();
	});
	
	
	
	
	
	
	
	$("[title]").each(function() {
		var placement = "auto"
		if($(this).attr("data-placement")) {
			placement = $(this).attr("data-placement");
		}
		
		$(this).tooltip({
			placement : placement,
			container: "body"
		});
			
	});
	
	
	
	
		//запрещаем покидать страницу через backspace
	$(window).bind("keydown",function(e) {
		if (e.which === 8 && !$(e.target).is("input, textarea, [contenteditable]")) {
			e.preventDefault();
		}
	});
	
	
		//панели с поиском
	$(".panel-search").each(function() {
			
		$(this).find(".panel-heading").append('<div class="search"><i class="fa fa-search"></i><input></div>');
		
			//при клике на иконку с поиском
		$(this).find(".search").bind("click",function() {
			if($(this).attr("class").indexOf("active") == -1) { 
				
							
				var 	search = $(this),
					input  = $(this).find("input"),
					head   = $(this).parent(),
					body   = $(this).parent().next();
					items  = $(this).parent().next().children();
				
					//отображаем инпут
				search.addClass('active'); 
				
				input
				.css("background-color",head.css("background-color"))
				.focus()
				.focusout(function() {
					search.removeClass("active");
				})
				.bind("keyup",function(e) {
					
					if(e.keyCode == 27) { $(this).val(""); }
					
					var val = $(this).val().toLowerCase();
					
					if(val.length == 0) {
						items.show();
					}
					else {
						items.each(function() {
							if( $(this).text().toLowerCase().indexOf(val) +1 ) {
								$(this).show();
							} else {
								$(this).hide();
							}
						});
					}
					
					
				});
				
			}
		});
		
		
		
		
	});
	
	
	$(".panel-crud").each(function() {
		
		var href = $(this).attr("href");
		$(this).removeAttr("href");
		
		if( $(this).attr("create") == null || $(this).attr("create") == "true" ) {
			$(this).find(".panel-heading").append('<div class="block-right" href="'+href+'" title="Создать"><i class="fa fa-plus"></i></div>');
		}
		
	});
	
	
	$(".selectpicker").selectpicker({
		container: 'body',
		selectedTextFormat: "count"
	});
	
	
	
	$(".bootstrap-select").find(".bs-searchbox input").keydown(function(e) {
		var 	active 	= $(this).parent().next().find("li.active"),
			next 	= null,
			prev 	= null,
			temp		= null,
			ul		= $(this).parent().next();
		
			
		
			//если ничего не выбрано - выбираем первый элемент
		if(e.keyCode == 38 || e.keyCode == 40) {
			if(!active.is("li")) {
				active = ul.find("li").first().addClass("active");
				return true;
			}
		} else {
			return true;
		}
		
		
			//определяем следующий и предыдущие элементы
		temp = active;
		while(next==null) {
			if(temp.next().is("li")) {
				if(temp.next().is(".hidden")) {
					temp = temp.next();
				} else {
					next = temp.next();
				}
			} else {
				next = active;
			}
		}
		temp = active;
		while(prev==null) {
			if(temp.prev().is("li")) {
				if(temp.prev().is(".hidden")) {
					temp = temp.prev();
				} else {
					prev = temp.prev();
				}
			} else {
				prev = active;
			}
		}
		
		
			//двигаем активный элемент
		if(e.keyCode == 38) {
			active.removeClass("active");
			active = prev.addClass("active");
		} else if(e.keyCode == 40) {
			active.removeClass("active");
			active = next.addClass("active");
		} else if(e.keyCode == 13) {
			//if(!active.is(".selected")) {
			//	var t = $(this);
			//	setTimeout(function() {
			//		t.trigger($.Event("keydown", {which:40, keyCode: 40}));
			//	}, 10);
			//}
		}
		
		
			
			//прокручиваем к активному элементу по необходимости
		var h = ul.height();
		var s = ul.scrollTop();
		var o = active.position().top;
	
		var scrl = o + s - h / 2 - (active.height()/2);
		
		ul.scrollTop(scrl);
		
		
	})
	
	.parent().parent().find("ul li").mouseover(function() {
		$(this).parent().find('li').removeClass("active");
		$(this).addClass("active");
	});
	
	$(".selectpicker[multiple]").each(function() {
		
	
		var btn = $(this).next('.bootstrap-select').find('.dropdown-toggle').css({width:'calc(100% - 40px)'}).parent().append('<button class="input-group-addon btn btn-default deselectAll" style="width:38px;height:32px"><i class="fa fa-remove"></i></button>').find(".deselectAll");
		
		btn.bind("click", function(e) {
			
			$(this).parent().prev("select").selectpicker('deselectAll').selectpicker('hide').selectpicker('show');
			
			e.preventDefault();
			e.stopPropagation();
		});
	});
	
	
	
	
	
	
		//если мобильные устройства
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		$('.selectpicker').selectpicker('mobile');
	}
	
	
	
	
	
	
	//кнопка "скачать на hightcharts"
	$('.chart').each(function() {
		
		var h, w, t, l;
		
		_this = this;
		
		h = $(this).height();
		w = $(this).width();
		
		t = $(this).position().top + 5;
		l = $(this).position().left + w - 5;
		
		

		//$(this).css('position', 'relative');
		
		var save = $('<a class="fa fa-floppy-o"></a>')
			.css({
				position: 'absolute',
				top: t,
				right: 10,
				zIndex: 999999,
				fontSize: '1.3em'
			})
			.insertBefore($(this));

		var print = $('<a class="fa fa-print"></a>')
			.css({
				position: 'absolute',
				top: t,
				right: 35,
				zIndex: 999999,
				fontSize: '1.3em'
			})
			.insertBefore(save);
		
		var canvas = $('<canvas hidden width="'+w+'px" height="'+h+'px"></canvas>')
			.insertAfter($(this));
		
		
		//при клике на иконку "Печать"
		print.click(function() {
			$(this).next().trigger('mouseover');
			var dataUrl = $(this).next().next().next().get(0).toDataURL(); //attempt to save base64 string to server using this var  
			var windowContent = '<!DOCTYPE html>';
			windowContent += '<html>'
			windowContent += '<head><title>Print canvas</title></head>';
			windowContent += '<body>'
			windowContent += '<img src="' + dataUrl + '">';
			windowContent += '</body>';
			windowContent += '</html>';
			var printWin = window.open('','','width='+$(window).width()+',height=600');
			printWin.document.open();
			printWin.document.write(windowContent);
			printWin.document.close();
			printWin.focus();
			printWin.print();
			printWin.close();
		});
		
		//при наводе на иконку "Сохранить"
		save.mouseover(function() {
			
			
			$(this).next().find('.highcharts-tooltip').css('opacity',0).delay(2000).queue(function() {
				$(this).css('opacity',1).dequeue();
			});
			
			var icon = $(this);
			var chart = icon.next().find('.highcharts-container').html()
			var title = icon.next().find('.highcharts-title').html()
			var canvas = icon.next().next().get(0)
			
			if(!title) {
				title = "Chart";
			}
			
			canvg(canvas, chart);
			icon.attr('download', title);
			icon.attr('href', canvas.toDataURL());
		});
		
		
		
		
	});
	
	
	
	
	
	
});


new Clipboard('[data-clipboard-text]');
