fw = angular.module('fw')

fw.directive 'tooltipRight', ->
  restrict: 'A'
  link: (scope, element, attr) ->
    $(element).attr('title', scope.$eval(attr.tooltipRight))
      .attr('data-placement', 'right')
      .attr('data-container', 'body')
      .hover ->
        if $(window).width() >= 768
          $(element).tooltip('show')
      , ->
        $(element).tooltip('hide')

fw.directive 'tooltipLeft', ->
  restrict: 'A'
  link: (scope, element, attr) ->
    $(element).attr('title', scope.$eval(attr.tooltipLeft))
      .attr('data-placement', 'left')
      .attr('data-container', 'body')
      .hover ->
        if $(window).width() >= 768
          $(element).tooltip('show')
      , ->
        $(element).tooltip('hide')

fw.directive 'tooltipTop', ->
  restrict: 'A'
  link: (scope, element, attr) ->
    $(element).attr('title', scope.$eval(attr.tooltipTop))
      .attr('data-placement', 'top')
      .attr('data-container', 'body')
      .hover ->
        if $(window).width() >= 768
          $(element).tooltip('show')
      , ->
        $(element).tooltip('hide')

fw.directive 'tooltipBottom', ->
  restrict: 'A'
  link: (scope, element, attr) ->
    $(element).attr('title', scope.$eval(attr.tooltipBottom))
      .attr('data-placement', 'bottom')
      .attr('data-container', 'body')
      .hover ->
        if $(window).width() >= 768
          $(element).tooltip('show')
      , ->
        $(element).tooltip('hide')

$(document).click ->
  $('.tooltip.fade').remove()

fw.directive 'fwLoading', ->
  scope:
    fwLoading: '='
  link: (scope, element, attr) ->

    # id = md5(JSON.stringify($(element)))
    id = 'fwLoading'+String(Number.random(0, 1000))

    if !$('#'+id).is('div')
      $('body').append('<div id="'+id+'" class="fw-loading" style="">
        <div class="loader">Loading...</div></div>')


    scope.$watch 'fwLoading', ->
      if scope.fwLoading
        el = $(element)
        t = el.offset().top
        l = el.offset().left
        w = el.outerWidth()
        h = el.outerHeight()
        $('#'+id).css
          top: t
          left: l
          width: w
          height: h
        .fadeIn(300)
        .find('.loader').css
          top: (h / 2) - 100
        #console.log 'open'
      else
        $('#'+id).fadeOut(300)
        #console.log 'close'
