fw = angular.module 'fw', ['ngWebSocket', 'ui-notification', 'ng-sortable',
 'xeditable', 'ui.select', 'ngStorage', 'ngDialog']


fw.run (editableOptions, uiSelectConfig) ->
  editableOptions.theme = 'bs3'
  uiSelectConfig.theme = 'bootstrap'
  uiSelectConfig.resetSearchInput = true
  uiSelectConfig.appendToBody = true

fw.config (ngDialogProvider) ->
  ngDialogProvider.setDefaults
    className: 'ngdialog-theme-plain'



fw.factory 'fwSocket', ($http, $websocket, $timeout, Notification) ->


  #класс для angular-переменных
  class Angular

    # колбеки
    _callbacks: {}

    constructor: (@name, @ws) ->
      @ready = false
      @ids = {}
      @data = [] #все элементы хранятся здесь
      @oldData = []
      #@get() #получить данные с сервера
      #метод change - вызывает _change у WS
      @_change = (->
        $timeout =>
          @ws._change(@name)
        , 500
      ).throttle(1000)

      @_load = (->
        $timeout =>
          @ws._load(@name)
        , 500
      ).throttle(1000)


    ###################
    # Основные методы #
    ###################

    #отправляет на сервер
    send: (type, data...) ->
      @ws.send('angular', @name, type, data)

    #получает все данные по переменной с сервера
    get: ->
      if @ws.connect
        @send('getData', null)
      else
        $timeout =>
          @get()
        , 1000

    #добавляет элемент к общему массиву
    append: (one) ->

      #выставляем на него нужные свойства и методы
      one._parent = @
      one._error  = {}
      one.save = ->
        data = {}
        for prop, val of @
          if @._old[prop] != undefined and @._old[prop] != val
            @._old[prop] = val
            data[prop] = val

        #отправляем данные на сервер
        @_parent.send('saveOne', {id: @id, data: data})
        @_parent._change()
      one.delete = ->
        @_parent.send('deleteOne', @id)
        @_parent._change()

      #сохраняем старые значения
      values = []
      one._old = {}
      for prop, val of one
        if prop.charAt(0) != '_' then one._old[prop] = val

      #добавляем его к общему массиву
      @data.push one

      #вызываем метод change у WS
      @_change()

    #обновляет элемент в общем массиве
    update: (one) ->
      #находим этот существующий элемент
      old = @getById one.id

      #изменяем только те свойства, которые отличаются
      for prop, val of one
        if old[prop] != val
          #есть проблема с массивами!!!!
          old[prop] = val

      #вызываем метод change у WS
      @_change()

    #добавить новый элемент
    insert: (data, callback) ->
      @add(data, callback)
    add: (data ,callback) ->

      callbackId = generateId()
      @_callbacks[callbackId] = callback

      #удаляем лишние свойства и методы
      delete data._create
      delete data._update
      delete data._delete
      delete data._change
      delete data.save
      delete data._parent
      delete data._old
      delete data._from
      delete data._error

      # console.log data

      @send('addOne', data, callbackId)




    #############
    # ON-методы #
    #############

    # когда с сервера пиходит запрос на выполнение колбека
    onCallback: (callback, id) ->


      # если такой колбек найден - выполняем и удаляем
      if @_callbacks[callback]
        # если передан
        if id?
          obj = @data.find (n) ->
            String(n.id) == String(id)
          @_callbacks[callback](obj)
        else
          @_callbacks[callback]()
        delete @_callbacks[callback]


    #получаем все данные
    onGetData: (data) ->

      #вызываем метод load у WS
      @_load()

      #получаем копию прошлых id-шников
      ids = JSON.parse JSON.stringify @ids

      for one in data

        #есле такой элемент был раньше
        if ids[one.id]
          @update one  #обновляем элемент в @data
          delete ids[one.id]
        else
          @append one  #добавляем элемент в @data
          @ids[one.id] = true  #сохраняем, что такой элемент у нас есть




      #если остались элементы, в ids - значит они были удалены
      for id of ids
        @onDeleted id

      #помечаем что загрузка выполнена
      if !@ready then @ready = true




    #при ошибки сохронения
    onErrorOne: (data) ->
      text = ''
      for id, err of data.data
        text += err.message
        text += "\n"
      Notification.error(text)
    #  one = @getById(data.id)
    #  for id, err of data.data
    #    prop = err.field
    #    msg  = err.message
    #    if !one._error[prop]
    #      one._error[prop] = []
    #    one._error[prop].push(msg)

    #когда нужно обновить один
    onUpdateOne: (data) ->
      one = @getById(data.id)
      for prop, val of data.data
        one[prop] = val

    #при неуспешном добавлении
    onAddError: (data) ->
      text = ''
      for id, err of data
        text += err.message
        text += "\n"
      Notification.error(text)

    #при успешном добавлении
    onAddSuccess: (data) ->
      if @addSuccess then @addSuccess()

    #при удалении одного элемента
    onDeleted: (id) ->
      id = Number id
      # Удаляем из массива значений
      for one, i in @data
        if one? and one.id == id
          @data.splice(i, 1)
      # Удаляем из массива id
      delete @ids[id]
      return




    ##########################
    # Вспомогательные методы #
    ##########################


    #получает элемент по id
    getById: (id) ->
      for one in @data
        if one.id == id
          return one


  class Ws
    constructor: (@_path, timeout) ->
      @_getPort(timeout)
      @_ping()


    #добавляем колбеки при открытии соединения
    _openCallbacks: []
    open: (callback) -> @_openCallbacks.push callback


    #выполнить колбек, когда загружается одна переменная
    _loadCallbacks: []
    load: (vars..., callback) ->
      @_loadCallbacks.push([vars, callback])

    #срабатывает при изменении одной переменной
    _load:(v) ->
      #вызываем нужные колбеки
      for one in @_loadCallbacks
        #если в массиве с переменными есть такая переменная
        if v in one[0]
          #выполняем колбек
          do one[1]

    #выполнить колбек, когда изменится одна из переменных
    _changeCallbacks: []
    change: (vars..., callback) ->
      @_changeCallbacks.push([vars, callback])

    #срабатывает при изменении одной переменной
    _change:(v) ->
      #вызываем нужные колбеки
      for one in @_changeCallbacks
        #если в массиве с переменными есть такая переменная
        if v in one[0]
          #выполняем колбек
          do one[1]


    #выполнить колбек, когда будет готова переменная с сервера
    #_servers: {}
    _readyCallbacks: []
    ready: (vars..., callback) ->
      @_readyCallbacks.push([vars, callback])
      #-> @_servers[v] = callback
    _ready: (v) ->
      #console.log v

      @_vars.push(v) #добавляем переменную к загруженным

      #проверяем какие колбеки можно активировать
      for callback, i in @_readyCallbacks
        if callback?
          OK = true
          #проходимся по каждой переменной колбека
          for one in callback[0]
            unless one in @_vars
              OK = false
              break
          #если для этого колбека все переменные загружены
          if OK
            do callback[1] #вызываем колбек
            delete @_readyCallbacks[i] #удаляем этот колбек

    _vars: []

    _ping: ->
      $timeout =>
        if @connect then @_socket.send 'ping'
        @_ping()
      , 5000
    connect: false
    _getPort: (timeout=500) ->
      $http.get(@_path)
      .success (data) =>
        @_port = data.port

        #через пол-секунды поключаемся к ws-серверу
        $timeout =>
          @_connect()
        , timeout
      .error (data) =>
        $timeout =>
          @_getPort()
        , 5000

      #возвращаем текущий объект
      return @
    _connect: ->
      #console.log 'connect'
      #соединяемся
      @_socket = $websocket("ws://#{window.location.host}:#{@_port}")

      #при открытии соединения
      @_socket.onOpen =>
        @connect = true
        #вызываем все зарагистрированыне колбеки
        do callback for callback in @_openCallbacks

      #при закрытии соединения переподключаемся через 5 сек
      @_socket.onClose =>
        #console.log 'Close'
        @connect = false
        $timeout =>
          @_getPort()
        , 5000


      #при новом сообщении
      @_socket.onMessage (data) =>


        #если это ping
        if data.data == 'ping'
          return false

        data = JSON.parse(data.data)


        action = data[0]

        if action == 'angular'


          name = data[1][0]
          action = data[1][1]
          action = 'on' + action.charAt(0).toUpperCase() + action.substr(1)
          data = data[1][2]


          newVar = no

          #если переменная раньше не была создана
          if !@[name]
            @[name] = new Angular(name, @)
            newVar = yes

          #выполняем экшен у переменной
          if @[name][action]
            @[name][action](data...)

          #говорим что переменная загрузилась
          @_ready(name) if newVar


        ####ЗДЕСЬ####
        ####ЗДЕСЬ####
        ####ЗДЕСЬ####
        else if action == '__userId'
          @userId = data[1][0]
        else
          #находим, какой экшен должен обработать сообщение
          action = 'on' + action.charAt(0).toUpperCase() + action.substr(1)

          #console.log 'Action:', action
          if @[action]
            @[action](data[1]...)

    #отправляет данные на сервер
    send: (type, data...) ->
      if @connect
        @_socket.send(JSON.stringify([type, data]))
      else
        Notification.error 'Нет связи с сервером...'


fw.factory 'fwDialog', ($compile, $timeout) ->

  class FwDialog
    constructor: (o) ->
      @title = o.title
      @template = o.template
      @scope = o.scope
      @doc = if window.top then window.top.document else window.document
      @width = o.width
      @render()
      $timeout =>
        @refresh()
      ,100

    getZindex: ->
      zIndex = 2000
      $(@doc).find('.fw-modal-frame').each ->
        zIndex++
      zIndex

    close: ->
      @modal.modal('hide')

    refresh: ->
      height = @iframe.contents().find('body').outerHeight()
      if height?
        @iframe.attr 'height', height
        $timeout =>
          @refresh()
        , 1000

    render: ->
      zIndex = @getZindex()
      modal = '<div style="z-index:'+zIndex+'" class="modal modal-fw fade"
        role="dialog" aria-labelledby="modal" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"
                aria-hidden="true">&times;</button>
              <h4 class="modal-title">'+@title+'</h4>
            </div>
            <div class="modal-body body">
            </div>
          </div>
        </div>
      </div>'

      # отрисовываем модаль
      @modal = $(modal).appendTo($(@doc).find('body'))
      @modal.modal()

      $timeout =>
        @modal.find('.modal-dialog').css('opacity', 1)
      , 500

      if @width?
        @modal.find('.modal-dialog').width @width

      # переносим modal-backdrop в нужный фрейм
      $('.modal-backdrop').css('z-index', zIndex-1)
        .appendTo($(@doc).find('body'))

      # отрисовываем iframe
      iframe = '<iframe class="fw-modal-frame" src="javascript:\'\'"></iframe>'
      @iframe = $(iframe)
      @iframe.appendTo(@modal.find('.modal-body'))

      # отрисовываем template
      template = window.document.getElementById @template
      template = $compile('<div>'+$(template).html()+'</div>')(@scope)
      @iframe.contents().find('body').append(template)

      # отрисовываем стили
      head = $('head').html()
      @iframe.contents().find('head').append(head)
      .append '<style>html,body{overflow:hidden !important;
        width:100% !important}</style>'


      # назначаем событие при закрытии модали
      @modal.on 'hidden.bs.modal', =>
        @modal.remove()


fw.directive 'fwModel', ->
  link: (scope, element, attrs) ->
    model = attrs['fwModel'].split('.')
    size = model.length

    if size == 1
      $(element).change ->
        scope[model[0]] = $(@).val()
        scope.$apply =>
          scope[model[0]] = $(@).val()
      scope[model[0]] = element.val()

    else if size == 2
      $(element).change ->
        scope[model[0]][model[1]] = $(@).val()
        scope.$apply =>
          scope[model[0]][model[1]] = $(@).val()
      scope[model[0]][model[1]] = element.val()

fw.filter 'moment', -> (str, format) ->
  if str?
    format ?= 'DD MMM YYYY'
    moment(str).format(format)
  else
    null

fw.filter 'numeral', -> (str, format) ->
  if str?
    format ?= '0,0'
    numeral(str).format(format)
  else
    null







fw.directive 'contenteditable', ($sce) ->
  restrict: 'A'
  require: '?ngModel'
  link: (scope, element, attrs, ngModel) ->
    if !ngModel then return false
    ngModel.$render = ->
      element.html($sce.getTrustedHtml(ngModel.$viewValue || ''))
    element.on 'blur keyup change', ->
      scope.$evalAsync(read)
    read = ->
      html = element.html()
      if attrs.stripBr && html == '<br>'
        html = ''
      ngModel.$setViewValue(html)
    read()

fw.directive 'title', ->
  restrict: 'A'
  link: (scope, element, attrs) ->
    $(element).attr('data-container', 'body')
    $(element).hover ->
      $(element).tooltip('show')
    , ->
      $(element).tooltip('hide')

fw.directive 'icon', () ->
  link: (scope, element, attr) ->
    scope.icon = attr.icon
    $(element).addClass('fw-icon').html('<i class="fa fa-'+attr.icon+'"></i>')
