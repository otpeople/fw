do ajax = ->
  $.ajax
    url:  '/fw/livereload'
    type: 'post'
    data: 'files='+window.FwLiveReloadFiles+'&assets='+window.FwLiveReloadAssets
    timeout: 60000
    success: (data) ->
      #если пришел ответ - перезагрузить
      if data.type == 'reload'
        window.location.reload()

      #если пришел ответ - обновить css
      else if data.type == 'css'
        files = data.data
        #проходимся по каждому пришедшему css файлу
        for one in data.data
          #находим стиль на странице
          if $('link[href="'+one+'"]').is('link')
            $('link[href="'+one+'"]').attr('data-exist', "yes")
          #если стиль не найден - добавляем в head
          else
            $('<link rel="stylesheet" href="'+one+'" data-exist="true">').appendTo($('head'))
        #удаляем не актуальные стили
        $('link[rel="stylesheet"]').each ->
          href = $(this).attr('href')
          remove = true
          for one in files
            if one == href
              remove = false
          if remove
            $(this).remove()
      setTimeout ->
        do ajax
      ,1000
    error: () ->
      setTimeout ->
        do ajax
      ,1000
