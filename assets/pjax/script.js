var pjax = {
	container:"body",	//контенер, куда будет загружено
	selector : "a",	//ссылки, которые нужно обрабатывать
	inload: false,
	ishide: false,
	isInit: false,
	before : function() {},
	after  : function() {},
	animate : {
		block : {
			in: null,
			out:null,
			speed: 0.7,
			effect:null,
		},
		modal : {
			in: null,
			out:null,
			speed: 0.7,
			effect: null,
		}
	},
	href: null,
	scrollTop: false,
	timeout: 20000,	//время ожидания
	metrika: null, 	//переменная с объектом яндекс-метрики
	title : {
		after: null,
		separator: ' ',
	},
	transition: function(href, title) {
		
		//console.log(window.ga);
		
			//если нужно добавить title
		if(this.title.after) {
			title = title + this.title.separator + this.title.after;
		}
		
		document.title = title;
		
		if(this.metrika != null) {
			//console.log(window.location.href+ ' == ' + href+ ' == '+ title);
			this.metrika.hit(window.location.href, title, href);
		}
		
		history.pushState(null, title, href);
		this.location = window.location.pathname;
	},
		//загружает полученный html в документ
	html : function(html, title) {
		
			//указывает, что данные не находсятся на этапе загрузки
		this.inload = false;
		
			//если html нужно загрузить в документ
		if(this.ifmodal == false) {
			
				//если все еще не скрыто
			if(pjax.ishide == true) {
				setTimeout(function() {
					pjax.html(html, title); //повторяем через .1 секунду
				}, 100);
				return false;
			} else {
					//говорим, что совершен переход
				this.transition(this.href, title);
					
						//добавялем html на страницу
				$('<div class="pjax in">'+html+'</div>')
				.appendTo($(this.container))
				.delay(100).queue(function() {
					$(this).dequeue().removeClass("in");
				});
			}
		}
			//если html нужно загрузить в модаль
		else {
			pjax.modal.load(html, title);
		}
		
		this.init();
		this.after();
	},
	
	prev : {
		href : window.location.href,
		title: ""
	},
	
	init : function() {
		
		if(this.isInit == false) {
			this.before();
			this.after();
			this.isInit = true;
		}
		
		
			//если поддерживается history 	
		if(window.history) {
			
				//назначаем клики на нужные селекотры
			$(document).find(this.selector).unbind("click").bind("click", function(e) {
				
					//только если нет таргета
				if(!$(this).attr("target")) {
					var href = $(this).attr("href");
					
					if(href) {
						
						if($(this).attr("data-modal") == null) {
							pjax.load(href);
						} else {
							pjax.load(href,true);
						}
						e.stopPropagation();
					}
					
					e.preventDefault();
				}
				
			});
		}
		
		
			//если не создана модал - создаем
		if(!this.modal) {
			this.createModal();
		}
		
		this.cackleInit();
		
	},
	load: function(href,target) {
		
			//если сейчас не происходит другая загрузка
		if(href && pjax.inload == false) {
			
			
			if(href.indexOf(location.protocol +'//'+ location.host) == -1) {
				href = location.protocol +'//'+ location.host + href;
			}
			
			this.prev.href = window.location.href;
			this.prev.title = $("head title").text();
			this.href = href;
			
			
				//для обычных ссылкок
			if(target == null) { 
				
				
					//если открыта модаль -> скрываем
				if(pjax.ifmodal == true) {
					pjax.modal.modal("hide");
				}
				
				
				pjax.before();
				
				
				pjax.ishide = true;
				
					//анимация убывания
				var time = parseFloat( $(this.container).children(".pjax").addClass("out").css("transition-duration") ) * 1000;
				setTimeout(function() {
					$('.pjax.out').remove();
					pjax.ishide = false;
					
						//прокрутка наверх
					if(pjax.scrollTop !== false) {
						if($("body").scrollTop() > pjax.scrollTop) {
							$("body").scrollTop(pjax.scrollTop);
						}
					}
					
				}, time);
				
			
				
			
					//указываем что сейчас блок в загрузке
				pjax.inload = true;

				$.ajax({
					type : "post",
					url  : href,
					timeout: pjax.timeout,
					beforeSend : function(xhr) {
						return xhr.setRequestHeader('X-PJAX','true');
					},
					success : function(data) {
						pjax.html(data.html, data.title);
					},
					error: function() {
						pjax.html("Возможно нет подключения к интернету");
					}
				});
				
			}
			
			
				//для ссылок, которые должны открываться в модали
			else {
				
				pjax.before();
				
				pjax.modal.show();
				
				pjax.inload  = true;
				
				$.ajax({
					type : "post",
					url  : href,
					timeout: pjax.timeout,
					beforeSend : function(xhr) {
						return xhr.setRequestHeader('X-PJAX','true');
					},
					success : function(data) {
						pjax.html(data.html, data.title);
					},
					error: function() {
						pjax.modal.hide();
						pjax.html("Возможно нет подключения к интернету");
					}
				});
				
				
				
			}
			
				
		}
		
	},
	
	
	location : window.location.pathname,
	change   : function() {
		
		if(window.location.pathname != pjax.location) {
			
			if(this.ifmodal == true) {
				this.modal.hide();
			} else {
				pjax.load(window.location.pathname);
			}
		}
	},
	
	
		//модаль
	ifmodal : false,
	modal   : null,
	createModal: function() {
		
		
		$("body").append('<div role="dialog" id="pjax-modal">\
			<div class="back"></div>\
			<div class="dialog container">\
				<header><div class="title">Заголовок модали</div><span class="remove">&times;</span></header>\
				<div class="content">123</div>\
				<footer></footer>\
			</div>\
		</div>');
		
		
		this.modal = {
			modal : $("#pjax-modal"),
			back  : $("#pjax-modal .back"),
			title : $("#pjax-modal .title"),
			close : $("#pjax-modal .close"),
			content:$("#pjax-modal .content"),
			dialog: $("#pjax-modal .dialog"),
			hide : function() {
				
				$("body").removeClass("static");
				
				pjax.ifmodal = false;
				
					//убираем задный фон
				var backTime = parseFloat(this.back.css("transition-duration")) * 1000;
				this.back.removeClass("in").delay(backTime).queue(function() {
					$(this).dequeue();
				});
					
					//анимируем modal (убывание)
				var modTime = parseFloat(this.dialog.addClass("out").css("transition-duration")) * 1000;
				this.dialog.delay(modTime).queue(function() {
					$(this).parent().hide();
					$(this).dequeue().hide().removeClass("out").children(".content").html('');
				});
				
				
					//говорим что совершен переход
				pjax.transition(pjax.prev.href, pjax.prev.title);
				
				pjax.before();
				pjax.after();
				
			},
			show : function() {
				pjax.before();
				$("body").addClass("static");
				pjax.ifmodal = true;
				this.modal.show().animate({scrollTop: 0},100);
				setTimeout(function() {
					pjax.modal.back.addClass("in");
				}, 10);
			},
			load : function(content, title) {
					
					//загружаем html в модальное окно
				this.content.html(content);
				
					//изменяем title
				this.title.text(title);
				
					//говорим что совершен переход
				pjax.transition(pjax.href, title);
				
					//показываем модальное окно
				this.dialog.show().addClass("in").delay(100).queue(function() {
					$(this).removeClass("in").dequeue();
				});
					
					//изменяем высоту заднего/черного фона
				this.back.delay(500).queue(function() {
					var h = $(this).next().height()+500;
					if(h < $(window).height()) { h = $(window).height(); }
					$(this).dequeue().height( h );
				});
				
			}
		};

		$("#pjax-modal .remove").add("#pjax-modal .back").bind("mousedown touchstart", function(e) {
			pjax.modal.hide();
			e.preventDefault();
			e.stopPropagation();
		});
		
		this.modal.title.bind("touchstart", function(e) {
			pjax.modal.hide();
			e.preventDefault();
			e.stopPropagation();
		})
				
	},
	
	
	
		//комментарии
	cackleId 		: false,
	cackleCreated	: false,
	cackleInit 	: function() {
		
			//если указан id cackle
		if(this.cackleId != false) {
			
				//если комментарий еще не создан
			if(this.cackleCreated == false) {
				
				cackle_widget = window.cackle_widget || [];
				(function() {
					var mc = document.createElement('script');
					mc.type = 'text/javascript';
					mc.async = true;
					mc.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cackle.me/widget.js';
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(mc, s.nextSibling);
				})();
				
				this.cackleCreated = true;
			}
			
				//если находит .cackle -> грузим туда комментарий
			if(this.ifmodal == true) {
				if($("#pjax-modal").find(".cackle").is("div")) {
					this.cackle($("#pjax-modal").find(".cackle"));
				}
			}
			else {
				if($(this.container).find(".cackle").is("div")) {
					this.cackle($(this.container).find(".cackle"));
				}
			}
			
			if($("#mc-chat").is("div")) {
				cackle_widget = window.cackle_widget || [];
				cackle_widget.push({widget: 'Chat', id: this.cackleId});
			}
			
		}
	},
	
		//загружает комментарии в элемент
	cackle : function(el,type) {
		
		if(!type) { type = "comment"; }
		
		var id = Math.random();
		id = "c"+String(id).replace(".","");
		el.attr("id",id);
		
		url = location.href;
		
		if(type == "comment") {
		
			cackle_widget = [];
			cackle_widget.push({widget: 'Comment', id: this.cackleId, url: url, container: id});
			
			try { Cackle.bootstrap(true); } catch(e) { }
			
		}
	}
		
}

//следит за изменением location
setInterval(function() {
	pjax.change();
}, 500);



