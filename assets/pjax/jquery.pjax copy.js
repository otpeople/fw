pjax = {
	container:"body",	//контенер, куда будет загружено
	selector : "a",	//ссылки, которые нужно обрабатывать
	inload: false,
	ishide: false,
	before : function() {},
	after  : function() {},
	animate : {
		block : {
			in: null,
			out:null,
			speed: 0.7,
			effect:null,
		},
		modal : {
			in: null,
			out:null,
			speed: 0.7,
			effect: null,
		}
	},
	scrollTop: false,
	timeout: 20000,	//время ожидания
	title: function(title) {
		
		if(this.ifmodal == false) {
			$("head title").html(title);
		} else {
			$("head title").html(title);
			this.modal.find(".modal-title").text(title);
		}
	},
	html : function(html) {
		if(this.ifmodal == false) {
			
			if(pjax.ishide == true) {
				setTimeout(function() {
					pjax.html(html);
				}, 100);
			} else {
				$(pjax.container).html(html).show().addClass(pjax.animate.block.in).css({
					"-webkit-animation-duration" : pjax.animate.block.speed.in+"s",
					"animation-duration" : pjax.animate.block.speed.in+"s"
				}).delay(pjax.animate.block.speed.in * 1000).queue(function() {
					$(this).removeClass(pjax.animate.block.in).next().css({marginTop:0});
					$(this).dequeue();
				});
				pjax.init();
				
			}
		} else {
			
				
			pjax.modal.find(".modal-dialog").addClass(pjax.animate.modal.in).css({
				"-webkit-animation-duration" : pjax.animate.modal.speed.in+"s",
				"animation-duration" : pjax.animate.modal.speed.in+"s"
			}).show().delay(pjax.animate.modal.speed.in * 1000).queue(function() {
				$(this).removeClass(pjax.animate.modal.in).dequeue();
			});
			
			
			this.modal.find(".modal-body").html(html);
			this.modal.find(".spinner").hide();
			pjax.init();
			
		}
							
	},
	
	prev : {
		href : window.location.href,
		title: ""
	},
	
	
	init : function() {
		
		console.log(1);
		
		this.before();
		this.after();
		
			//если задано одно время - делаем массив [исчезанеи, появление]
		if(!this.animate.block.speed.out) {
			this.animate.block.speed = {
				in:  this.animate.block.speed,
				out: this.animate.block.speed
			};
		}
		if(!this.animate.modal.speed.out) {
			this.animate.modal.speed = {
				in:  this.animate.modal.speed,
				out: this.animate.modal.speed
			};
		}
		
		
			//если поддерживается history 	
		if(window.history) {
			
				//назначаем клики на нужные селекотры
			$(document).find(this.selector).unbind("click ").bind("click ", function(e) {
				
					//только если нет таргета
				if(!$(this).attr("target")) {
					var href = $(this).attr("href");
					
					if(href) {
						
						if($(this).attr("data-modal") == null) {
							pjax.load(href);
						} else {
							pjax.load(href,$(this).attr("data-modal"));
						}
						e.stopPropagation();
					}
					
					e.preventDefault();
				}
				
			});
		}
		
		
			//если не создана модал - создаем
		if(!this.modal) {
			this.createModal();
		}
		
		this.cackleInit();
		
	},
	load: function(href,target) {
			//если сейчас не происходит другая загрузка
		if(href && pjax.inload == false) {
			
			this.prev.href = window.location.href;
			this.prev.title = $("head title").text();
			
			//для обычных ссылкок
			if(target == null) { 
				
					//прокручиваем наверх, если нужно
				if(this.scrollTop !== false) {
					setTimeout(function() {
						$("html, body").animate({scrollTop:0},pjax.scrollTop);
					}, pjax.animate.block.speed.out*1000);
					
				}
				
				
					//если открыта модаль -> скрываем
				if(pjax.ifmodal == true) {
					pjax.modal.modal("hide");
				}
				
				
					//изменяем history
				history.pushState(null,null,href);
				pjax.before();
				pjax.location = window.location.pathname;
				
				
				
					//анимация убывания
				if(pjax.animate.block.out) {
					var container = $(pjax.container);
					pjax.ishide = true;
					
					
					container.addClass("animated").css({
						"-webkit-animation-duration" : pjax.animate.block.speed.out+"s",
						"animation-duration" : pjax.animate.block.speed.out+"s"
					}).addClass(pjax.animate.block.out).delay(pjax.animate.block.speed.out * 1000).queue(function() {
						pjax.ishide = false;
						$(this).next().css({marginTop:$(this).height()});
						$(this).removeClass(pjax.animate.block.out).hide().dequeue();
					});
				}
				
				pjax.inload = true;
				
				$.ajax({
					type : "post",
					url  : href,
					timeout: pjax.timeout,
					beforeSend : function(xhr) {
						return xhr.setRequestHeader('X-PJAX','true');
					},
					success : function(data) {
						pjax.title(data.title);
						pjax.html(data.html);
						pjax.inload = false;
					},
					error: function() {
						pjax.title("Ошибка...");
						pjax.html("Возможно нет подключения к интернету");
						pjax.inload = false;
					}
				});
				
			}
			
			
				//для ссылок, которые должны открываться в модали
			else {
					//изменяем history
				history.pushState(null,null,href);
				pjax.before();
				pjax.location = window.location.pathname;
				
				
				var modalWidth;
				
				if(target == "true" || target == "auto") {
					modalWidth = "auto";
				}
				else {
					modalWidth = target + "px";
				}
				
			
				pjax.modal.modal("show").find(".modal-dialog").hide().next().show();
				
				pjax.ifmodal = true;
				pjax.inload  = true;
				
				$.ajax({
					type : "post",
					url  : href,
					timeout: pjax.timeout,
					beforeSend : function(xhr) {
						return xhr.setRequestHeader('X-PJAX','true');
					},
					success : function(data) {
						pjax.title(data.title);
						pjax.html(data.html);
						pjax.inload = false;
					},
					error: function() {
						pjax.modal.modal("hide");
						pjax.title("Ошибка...");
						pjax.html("Возможно нет подключения к интернету");
						pjax.inload = false;
					}
				});
				
				
				
			}
			
				
		}
		
	},
	
	
	location : window.location.pathname,
	change   : function() {
		
		if(window.location.pathname != pjax.location) {
			
			if(this.ifmodal == true) {
				this.modal.modal("hide");
			} else {
				pjax.load(window.location.pathname);
			}
		}
	},
	
	
		//модаль
	ifmodal : false,
	modal   : null,
	createModal: function() {
		
		$("body").append('<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" id="pjax_modal" style="">\
			<div class="container">\
				<div class="modal-dialog animated" style="width:100%">\
					<div class="modal-content">\
						<div class="modal-header">\
							<button type="button" class="close" id="modalClose" data-dismiss="modal" aria-hidden="true">&times;</button>\
							<h4 class="modal-title" id="modalTitle">Название модали</h4>\
						</div>\
						<div class="modal-body" id="modalBody">\
						</div>\
					</div>\
				</div>\
				<div class="spinner"></div>\
			</div>\
		</div>');
		
		
		$("#pjax_modal").find(".modal-header").bind("touchstart click",function(e) {
			pjax.modal.modal("hide");
			e.preventDefault();
			e.stopPropagation();
		});
		
		this.modal = $("body").find("#pjax_modal");
		
		
		this.modal.bind("hide.bs.modal",function(e) {
			
				//если модаль сейчас загружается
			if(pjax.inload == true) {
				e.preventDefault();
				return false;	
			}
			
				//пока модаль будет закрываться => делаем другие ссылки неакивными
			pjax.inload = true;
			
			
			
			$(this).find(".modal-dialog").addClass(pjax.animate.modal.out).css({
				"-webkit-animation-duration" : pjax.animate.modal.speed.out+"s",
				"animation-duration" : pjax.animate.modal.speed.out+"s"
			}).delay(pjax.animate.modal.speed.out * 1000).queue(function() {
				$(this).find(".modal-body").html("");
				$(this).find(".modal-title").html("Загрузка...");
				$(this).removeClass(pjax.animate.modal.out).dequeue();
				pjax.inload = false;
			});
			pjax.ifmodal = false;
			
			pjax.title(pjax.prev.title);
			
			history.pushState(null,null,pjax.prev.href);
			pjax.location = window.location.pathname;
			
			pjax.before();
			pjax.after();
			
		});
		
		
	},
	
	
	
		//комментарии
	cackleId 		: false,
	cackleCreated	: false,
	cackleInit 	: function() {
		
			//если указан id cackle
		if(this.cackleId != false) {
			
				//если комментарий еще не создан
			if(this.cackleCreated == false) {
				
				cackle_widget = window.cackle_widget || [];
				(function() {
					var mc = document.createElement('script');
					mc.type = 'text/javascript';
					mc.async = true;
					mc.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cackle.me/widget.js';
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(mc, s.nextSibling);
				})();
				
				this.cackleCreated = true;
			}
			
				//если находит .cackle -> грузим туда комментарий
			if(this.ifmodal == true) {
				if($("#pjax_modal").find(".cackle").is("div")) {
					this.cackle($("#pjax_modal").find(".cackle"));
				}
			}
			else {
				if($(this.container).find(".cackle").is("div")) {
					this.cackle($(this.container).find(".cackle"));
				}
			}
			
			
			
		}
	},
	
		//загружает комментарии в элемент
	cackle : function(el) {
		
		var id = Math.random();
		id = "c"+String(id).replace(".","");
		el.attr("id",id);
		
		url = location.href;
		
		cackle_widget = [];
		cackle_widget.push({widget: 'Comment', id: this.cackleId, url: url, container: id});
		
		try { Cackle.bootstrap(true); } catch(e) { }
	}
		
}

//следит за изменением location
setInterval(function() {
	pjax.change();
}, 500);
