	//sortable
;(function(e){var t,n=e();e.fn.sortable=function(r){var i=String(r);r=e.extend({connectWith:false},r);return this.each(function(){if(/^enable|disable|destroy$/.test(i)){var s=e(this).children(e(this).data("items")).attr("draggable",i=="enable");if(i=="destroy"){s.add(this).removeData("connectWith items").off("dragstart.h5s dragend.h5s selectstart.h5s dragover.h5s dragenter.h5s drop.h5s")}return}var o,u,s=e(this).children(r.items);var a=e("<"+(/^ul|ol$/i.test(this.tagName)?"li":"div")+' class="sortable-placeholder">');s.find(r.handle).mousedown(function(){o=true}).mouseup(function(){o=false});e(this).data("items",r.items);n=n.add(a);if(r.connectWith){e(r.connectWith).add(this).data("connectWith",r.connectWith)}s.attr("draggable","true").on("dragstart.h5s",function(n){if(r.handle&&!o){return false}o=false;var i=n.originalEvent.dataTransfer;i.effectAllowed="move";i.setData("Text","dummy");u=(t=e(this)).addClass("sortable-dragging").index()}).on("dragend.h5s",function(){if(!t){return}t.removeClass("sortable-dragging").show();n.detach();if(u!=t.index()){t.parent().trigger("sortupdate",{item:t})}t=null}).not("a[href], img").on("selectstart.h5s",function(){this.dragDrop&&this.dragDrop();return false}).end().add([this,a]).on("dragover.h5s dragenter.h5s drop.h5s",function(i){if(!s.is(t)&&r.connectWith!==e(t).parent().data("connectWith")){return true}if(i.type=="drop"){i.stopPropagation();n.filter(":visible").after(t);t.trigger("dragend.h5s");return false}i.preventDefault();i.originalEvent.dataTransfer.dropEffect="move";if(s.is(this)){if(r.forcePlaceholderSize){a.height(t.outerHeight())}t.hide();e(this)[a.index()<e(this).index()?"after":"before"](a);n.not(a).detach()}else if(!n.is(this)&&!e(this).children(r.items).length){n.detach();e(this).append(a)}return false})})}})(jQuery);

	//для работы с файлами
$(function() {

		//выставляет номера файлам по порядку
	function sortFiles(token) {
		var container = $('.fw-form-files[form="'+token+'"] .files');
		var i = 0;
		container.find(".file").each(function() {
			if($(this).attr('class').indexOf("removed") == -1) {
				i++;
				$(this).find(".number").text(i);
			} else {
				$(this).find(".number").text('');
			}
		});

	}

		//получает и отображает файлы
	function getFiles(token) {

		$.ajax({
			url : location.href,
			type: "post",
			beforeSend : function(xhr) {
				return xhr.setRequestHeader('X-FORM-AJAX', 'getfiles');
			},
			data : "token="+token,
			success: function(data){


				data = JSON.parse(data);

					//контейнер для файлов
				var container = $('.fw-form-files[form="'+token+'"] .files');

				var html = "";
				for(file in data) {
					file = data[file];

					var blockImg = container.find('[data-file="'+file.name+'"]');

					if(blockImg.is("div")) {

						var last = blockImg.find('footer').attr('data-url');

						if(last != file.img) {
							blockImg.find('footer').attr('data-url',file.img).css('background-image','url('+file.img+')');
						}


					} else {
						html += '<div class="file" data-file="'+file.name+'">';
							html += '<header>';
								html += '<i class="fa fa-remove remove" title="Удалить"></i>';
								html += '<i class="fa fa-pencil crop" title="Редактировать" href="/fw/CropFile/'+encodeURIComponent(file.crypt)+'" target="full"></i>';
								html += '<i class="fa fa-arrows sort" title="Сортировать" style="float:left"></i>';
							html += '</header>';
							html += '<footer data-url="'+file.img+'" style="background-image: url('+file.img+')"><div class="view" target="full" href="/fw/showFile/'+encodeURIComponent(file.crypt)+'"><i class="fa fa-eye"></i></div></footer>';
							html += '<div class="red">Удалено</div>';
							html += '<div class="number"></div>';
						html += '</div>';

					}

				}

				container.append(html);

				container.find('.sort').unbind('mousedown').bind("mousedown",function(e) {
					$(this).parent().parent().parent().next('.dropzone').hide();
				}).bind("dragend",function() {
					var el = $(this);
					setTimeout(function() {
						el.parent().parent().parent().next('.dropzone').show();
					}, 100);
				});

				$("body").bind("mouseup",function() {
					setTimeout(function() {
						container.next('.dropzone').show();
					}, 100);
				});


					//sortable
				container.sortable('destroy').sortable({
					handle: '.sort'
				}).children().unbind('drag').bind("drag",function() {

					var height = $(this).height();
					var width  = $(this).outerWidth();
					$(this).parent().find(".sortable-placeholder").css({
						height:height,
						width: width,
						display: "inline-block"
					});
				}).bind('dragend', function() {
					container.next('.dropzone').show().removeClass("full");
					sortFiles(token);
				});


				container.find(".remove").unbind('click').bind("click",function() {
					var parent = $(this).parent().parent();

					if(parent.attr("class").indexOf("removed")+1) {
						var field = parent.removeClass("removed").parent().parent();
					} else {
						var field = parent.addClass("removed").parent().parent();
					}
					field.removeClass("has-error");
					field.children("p").remove();
					sortFiles(token);
				});


				sortFiles(token);



			}
		});
	}

		//инициализирует поля фалов
	$(".fw-form-files").each(function() {



		var element = $(this);
		var token   = $(this).attr("form");
		var id      = "id" + String(Math.random()).substr(2, 10);

		$(this).find('.fw-form-files-get').bind('click', function() {
			getFiles(token);
		});

			//инициализируем dropzone
		$(this).find(".dropzone").attr("id",id);
		var dropzone = new Dropzone('#'+id, {
			url: location.href,
			dictDefaultMessage: "+",
			paramName : token,
			sending: function(file, xhr, fd){
				return xhr.setRequestHeader('X-FORM-AJAX', 'addfile');
			}
		});

			//при успешной загрузки файлов
		dropzone.on("complete", function(file) {
			getFiles(token);
			dropzone.removeFile(file);
		});

			//рисуем плюсик в dropzone
		$(this).find(".dropzone .dz-message").html('<i class="fa fa-plus" style="font-size:5em"></i>');


			//
		$(this).find(".dropzone").bind('drop mouseup', function() {
			$(".fw-form-files .dropzone").removeClass("full");
		});

			//при обнаружении перетаскивания на body
		$("body").bind("dragenter", function() {
			if(element.is(":visible")) {
				element.find(".dropzone").addClass("full");
			}
		}).bind("mouseout mouseup drop",function() {
			$(".fw-form-files .dropzone").removeClass("full");
		});



			//получаем файлы
		getFiles(token);


	});

});

	//для работы с формой
$(function() {


	$('.fw-field').each(function() {
		$(this).on('focusout', function() {
			validate($(this));
		});
	});

		//теги
	$(".fw-form-tags").each(function() {
		$(".fw-form-tags").tagsinput();
	});

		//булевы
	$('input[data-toggle="toggle"]').each(function() {
		$(this).bootstrapToggle({
			on: 'Да',
			off: 'Нет'
		});
	});


		//html
	$(".fw-form-html").each(function() {

			$(this).summernote({
				//minHeight: 300,
				height: 200,
				lang: 'ru-RU',
				toolbar: [
				    //[groupname, [button list]]
				    ['misc', ['undo','redo']],
				    ['style', ['style']],
				    ['style', ['bold', 'italic', 'underline', 'clear']],
				    ['font', ['strikethrough', 'superscript', 'subscript']],
				    ['fontsize', ['fontsize']],
				    ['height', ['height']],
				    ['color', ['color']],
				    ['para', ['ul', 'ol', 'paragraph']],
				    ['insert', ['link', 'picture', 'hr', 'table', 'video']]
				]
			});

		});

	$('[fw-form-delete]').on('click', function() {

		var token = $(this).attr('fw-form-delete');
		var btn = $(this);


		if(confirm("Вы уверены, что хотите удалить?")) {

			btn.attr('disabled','disabled').html('<i class="fa fa-spin fa-circle-o-notch"></i> Удаляем...');
			$.ajax({
				url : location.href,
				beforeSend : function(xhr) {
					return xhr.setRequestHeader('X-FORM-AJAX', 'delete');
				},
				type: "post",
				data : "form-token="+token,
				success: function(data){
					btn.html('<i class="fa fa-check"></i> Удалено!').removeClass('btn-danger').addClass('btn-success');
					reload($('form#'+token));
				},
				error : function() {
					btn.html('Ошибка...').removeClass('btn-danger').addClass('btn-warning');
				}
			});
		}


	});

	$('.fw-form').submit(function(e) {

		var form = $(this);
		var btn  = form.find('button');

			//html - editable
		$(".fw-form-html").each(function() {
			$($(this).attr("data-for")).text($(this).summernote('code'));
		});

			//данные формы
		var data = form.serialize();

			//токен
		data += '&form_token='+form.attr('id');

		    //файлы
		$(".fw-form-files").each(function() {
			var files = [];
			$(this).find(".file").each(function() {
				if($(this).attr("class").indexOf("removed") == -1) {
					files.push($(this).attr("data-file"));
				}
			});


			files = files.join(",");
			data += "&"+$(this).attr("data-name")+"="+files;

		});


		var btnHtml = btn.html();


		btn.attr("disabled","disabled").html('<i class="fa fa-spin fa-circle-o-notch"></i> Сохраняем...');

		    //отправляем
		$.ajax({
			url : location.href,
			beforeSend : function(xhr) {
				return xhr.setRequestHeader('X-FORM-AJAX', 'save');
			},
			type: "post",
			data: data,
			success: function(data) {


				data = JSON.parse(data), i=0;

				for(field in data) { i++; }

				//если валидация прошла
				if(i == 0) {
					btn.html('<i class="fa fa-check"></i> Сохранено!').removeClass('btn-primary').addClass('btn-success');
					reload(form);
				}
				else {
					for(field in data) {
						var field = data[field];
						var message = field.message;
						if(field.field != null) {
							field = field.field;
						} else {
							field = "form-full-errors";
						}

						var group = $('.fw-field[data-form="'+form.attr('id')+'"][data-name="'+field+'"]');
						group.find('.fw-error').html('');
						addError(group,message);

					}


					btn.removeAttr('disabled').html(btnHtml);
				}

				/*

				data = JSON.parse(data);

				var i = 0;

				for(field in data) {
					var field = data[field];

					var message = field.message;
					if(field.field != null) {
						field = field.field;
					} else {
						field = "form_errors";
					}

					var group = activeForm.find('#field_'+field);
					group.children("p").remove();
					addError(group,message);
					form.children("button").removeAttr("disabled");
					i++;
				}



				else if(activeForm.attr("data-type") == "login") {
					activeForm.children("button").remove();
					activeForm.append('<p class="next_next">Следующая папытка через 3</p>');
					setTimeout(function() {
						activeForm.find(".next_next").text("Следующая папытка через 2");
					}, 1000);
					setTimeout(function() {
						activeForm.find(".next_next").text("Следующая папытка через 1");
					}, 2000);
					setTimeout(function() {
						activeForm.find(".next_next").text("Следующая папытка через 0");
						location.reload();
					}, 3000);
				}

				*/
			},
			error: function(data) {
				var field = $('.fw-field[data-form="'+form.attr('id')+'"][data-name="form-full-errors"]');
				addError(field,"Ошибка...Проверьте подключение к интернету");
			}
		});


		e.preventDefault();
	});


	var activeForm;
	$(".fw-form").each(function() {


			//при клике на кнопки
		$(this).find(".save").bind("click", function(e) {

			activeForm = $(this).parent();

			submitForm(activeForm,"update");

			e.preventDefault();
		});
		$(this).find(".delete").bind("click", function(e) {

			if(confirm("Вы уверены, что хотите это удалить?")) {

				activeForm = $(this).parent();

				submitForm(activeForm,"delete");

			}
			e.preventDefault();
		});
		$(this).find(".create").bind("click", function(e) {

			activeForm = $(this).parent();

			submitForm(activeForm,"create");

			e.preventDefault();
		});
		$(this).find(".login").bind("click", function(e) {

			activeForm = $(this).parent();

			submitForm(activeForm,"login");

			e.preventDefault();
		});


			//вилидация
		if($(this).attr("data-type") != "login") {
			$(this).find(".form-group").bind("focusout",function() {
				validate($(this));
			});
		}

			//добавляет summernote (html editor)
		$(this).find(".fw-html").each(function() {

			$(this).summernote({
				//minHeight: 300,
				height: 350,
				lang: 'ru-RU',
				toolbar: [
				    //[groupname, [button list]]
				    ['misc', ['undo','redo']],
				    ['style', ['style']],
				    ['style', ['bold', 'italic', 'underline', 'clear']],
				    ['font', ['strikethrough', 'superscript', 'subscript']],
				    ['fontsize', ['fontsize']],
				    ['height', ['height']],
				    ['color', ['color']],
				    ['para', ['ul', 'ol', 'paragraph']],
				    ['insert', ['link', 'picture', 'hr', 'table']]
				]
			});

		});

	});


		//выполняет перезагрузку фрейма/ скрытие модали
	function reload(form) {


		var reload = form.attr("fw-reload");
		if(reload != null) {

			setTimeout(function() {
				modal.modal("hide");
			}, 300);

			if(reload != " ") {

				var frames = $("iframe",top.document);
				var frame;
				var src;
				var x = 0;


				frames.each(function() {
					src = $(this).attr("src");
					if(src != null) {
						if(src.replace(/\//g, '') == reload.replace(/\//g, '')) {
							return $(this).attr("src",src);
						}
					}
				});

				if(frame != null) {
					frame.attr("src",frame.attr("src"));
				} else {
					frames.each(function() {
						$(this).contents().find("iframe").each(function() {
							src = $(this).attr("src");
							if(src != null && src.replace(/\//g, '') == reload.replace(/\//g, '')) {
								return $(this).attr("src",src);
							}
						});
					});
				}

			}

		}


		if(window.form_after_save) {
			window.form_after_save();
		}


	}

		//проверяет форму
	function validate(field) {

		field.removeClass("has-error");
		field.find('.fw-error').html('');

		if(field.children("input.form-control").is("input")) {

			var input = field.children("input")
			var val = input.val();

				//проверка на минимум и максимум
			var max = Number(input.attr("max"));
			var min = Number(input.attr("min"));


			if((val.length < min || val.length > max) && max != 0) {
				addError(field,'Необходимо от '+min+' до '+max+' символов');
			}
			else if(val.length != 0){

					//проверка на регулярные выражения
				var validators = JSON.parse(field.attr("data-validators"));

				for(var i=0;i<validators.length; i++) {
					var validator = validators[i];
					var re1 = new RegExp(validator.pattern);

					if(!re1.test(val)) {
						addError(field,validator.message);
					}
				}
			}

			if(input.attr("type") == "password") {
				var pass = null;
				input.parent().children('input[type="password"]').each(function() {
					if(pass == null) {
						pass = $(this).val();
					}
					else {
						if(pass != $(this).val()) {
							addError(field,"Пароли не совпадают");
						}
					}
				});
			}

		}

		else if(field.children("select.form-control").is("select")) {
			var select = field.children("select.form-control");
			var val 	 = select.val();
			var max 	 = select.attr("max");
			var min    = select.attr("min");
			var len;
			if(val == null) { len = 0;}
			else {

				if(max == 1) {
					if(val == 0) { len = 0; } else { len = 1; }
				} else {
					len = val.length;
				}

			}


			if(len > max || len < min) {
				if(min == max) {
					addError(field,"Нужно выбрать "+min);
				} else {
					addError(field,"Нужно выбрать от "+min+" до "+max);
				}

			}
		}

	}

		//выводит сообщения об ошибках
	function addError(field,message) {
		field.addClass("has-error");
		field.find('.fw-error').append(message+'<br>');
	}


});
