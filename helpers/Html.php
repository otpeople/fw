<?php
namespace fw\helpers;

use fw\core\Log;

class Html {

  public static function tag($tag,$attrs=[],$content="") {
    $res = [];

    foreach($attrs as $attr => $val) {
      if(is_array($val)) {
        foreach($val as $attr1 => $val1) {
          $res[] = $attr.'-'.$attr1.'="'.$val1.'"';
        }
      } else {
        $res[] = $attr.'="'.$val.'"';
      }
    }
    

    $res = '<'.$tag.' '.implode(" ", $res).'>';

    $no = ["img","input","br","hr"];
    if(!in_array($tag, $no)) {
      $res .= $content.'</'.$tag.'>';
    }
    return $res;
  }

  public static function select($attr=[],$options=[],$values=[]) {

    if(!is_array($values)) { $values = [$values]; }

    //html контент
    $content = [];

    foreach($options as $option => $value) {

      //если это группа (optgroup)
      if(is_array($value)) {

        $content[] = '<optgroup label="'.$option.'">';
        foreach($value as $opt => $val) {
          $content[] = '<option value="'.$opt.'"';
          if(in_array($opt, $values)) $content[] = ' selected';
          $content[] = '>'.$val.'</option>';
        }
        $content[] = '</optgroup>';

      }

      //если это обычные ключ=>значение
      else {
        $content[] = '<option value="'.$option.'"';
        if(in_array($option, $values)) {
          $content[] = ' selected';
        }
        $content[] = '>'.$value.'</option>';
      }

    }
    $content = implode("", $content);


/*
    //определяем что нужно разбить на группы
    $groups = false;
    foreach($options as $opt) {
      if(is_array($opt)) $groups = true;
    }


    if($groups === true) {

      foreach($options as $group => $opts) {

        $content[] = '<optgroup label="'.$group.'">';

        Log::debug($opts);

        foreach($opts as $option => $value) {
          $content[] = '<option value="'.$option.'"';
          if(in_array($option, $values)) {
            $content[] = ' selected';
          }
          $content[] = '>'.$value.'</option>';
        }

        $content[] = '</optgroup>';

      }
      $content = implode("", $content);

    } else {

      foreach($options as $option => $value) {
        $content[] = '<option value="'.$option.'"';
        if(in_array($option, $values)) {
          $content[] = ' selected';
        }
        $content[] = '>'.$value.'</option>';
      }
      $content = implode("", $content);

    }

*/
    return self::tag("select",$attr,$content);

  }

  public static function button($attr=[],$content="") {
    return self::tag("button",$attr,$content);
  }

    //кнопки bootstrap
  public static function btn($style,$title="",$icon="",$size="") {

      //класс
    $class = "btn btn-".$style;
    if(!empty($size)) { $class .= " btn-".$size;}

      //иконка и текст
    $content = $title;
    if(!empty($icon)) {
      $content = '<span class="glyphicon glyphicon-'.$icon.'"></span> '.$content;
    }

    return self::tag("button",["class"=>$class],$content);

  }


}



?>
