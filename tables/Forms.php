<?php
namespace fw\tables;

use fw\core\Table;

class Forms extends Table {
	
	public $fields = [
		'token' => [
			'title' 	=> "Токен",
			'size'	=> 64,
		],
		'model' => [
			'title' => "Модель",
		],
		'model_id' => [
			'title' => "Номер поля в модели",
		],
		'fields' => [
			'title' => "Доступные поля",
		],
		'create' => [
			'title' => "Создать",
			'type' => "bool",
		],
		'update' => [
			'title' => "Обновить",
			'type' => "bool",
		],
		'delete' => [
			'title' => "Удалить",
			'type' => "bool",
		],
		'files' => [
			'title' => "Файлы",
			'type' => "text",
		],
		'ratio' => [
			"title" => "Соотношение сторон",
			"type"  => "float",
		],
		'default' => [
			'title' => "Значения по умолчанию",
		],
	];
	
	public $trigger = false;
	
}