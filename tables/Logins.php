<?php
namespace fw\tables;

use fw\core\Table;

class Logins extends Table {
	
	public $fields = [
		'session' => [
			'title' => 'Уникальный номер сессии',
		],
		'ip' => [
			'title' => 'IP-адресс',
		],
		'agent' => [
			'title' => 'Браузер',
		],
		'user' => [
			'title' => 'Пользователь',
			'type'  => 'int',
		],
		'module' => [
			'title' => 'Модуль приложения',
		],
		'model' => [
			'title' => 'Модель входа',
		],
		'time' => [
			'title' => 'Время входа',
			'type'  => 'dt',
		],
		'active' => [
			'title' => 'Время последней активности',
			'type'  => 'dt',
		],
		'comment' => [
			'title' => 'Комментарий',
		],
		'status' => [
			'title' => 'Статус',
			'type'  => 'bool',
		],
	];
}	
?>