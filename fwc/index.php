<?php

//перенос строки
define('N', "\n");
define('FWCVERSION', '1.0.1');


//если не переданы никакие аргументы
if(isset($argv[1])) {
  if($argv[1] == 'init') {
    require('Init.php');
    $obj = new Init();
    $obj -> index();
    exit;
  }
  elseif($argv[1] == 'clone') {
    require('Init.php');
    $obj = new Init();
    $obj -> _clone();
    exit;
  }
} else {
  echo "fw-console v ".FWCVERSION." \n"; exit;
}

//подключаем файл fwc.php
if(file_exists(getcwd().'/fwc.php')) {
  require(getcwd().'/fwc.php');
}
