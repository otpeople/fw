<?php

class Init {

  public $title = "Create new app";

  public $pdo;

  public $db;

  //получает строку от пользователя
  private function get($str, $def=null) {

    //если не указано значение по умолчанию
    if(!isset($def)) {
      echo $str.': ';
      $var = trim(fgets(STDIN));
      if(empty($var)) {
        return get($str);
      } else {
        return $var;
      }
    }

    //если заданы возможные значения
    elseif(is_array($def)) {
      echo $str.' ['.implode('/', $def).']';
      $var = trim(fgets(STDIN));
      if(empty($var) || !in_array($var, $def) ) {
        return get($str, $def);
      }
      else {
        return $var;
      }
    }


    //если указано значение по умолчанию
    else {
      echo $str.' ['.$def.']: ';
      $var = trim(fgets(STDIN));
      if(empty($var)) {
        return $def;
      } else {
        return $var;
      }
    }
  }



  public function index($x=null) {

    //получем валидное название приложения
    $app = false;
    while(!$app) {
      $app = $this -> testApp( $this->get('Application name') );
    }

    //проверяем чтобы можно было создать такую базу данных
    $db = false;
    while(!$db) {
      $db = $this -> testDb( $this->get('DB name',$app) );
    }

    //создаем приложения
    $this -> create($app, $db);

  }



  //проверяет - можно ли создавать такое приложени
  public function testApp($app) {

    exec('ls', $ls);

    //проверяем чтобы не было такой папки
    if(in_array($app, $ls)) {
      echo "This dir already exists\n"; return false;
    }
    return $app;
  }

  //проверяем - можно ли создавать такую базу данных
  public function testDb($db) {

    $user = 'root';
    $pass = 'root';

    //проверяем нет ли такой базы данных
    try {
      $pdo = new PDO("mysql:host=127.0.0.1", $user, $pass);
    } catch(PDOException $e) {}

    if(!isset($pdo)) { echo "Error MySql connect\n"; return false; }


    //получаем имеющиеся базы данных
    $dbs = $pdo -> query('show databases');
    $dbs -> setFetchMode(PDO::FETCH_ASSOC);

    $dbList = [];
    while($row = $dbs->fetch()) {
      $dbList[] = $row["Database"];
    }

    //если такая база уже есть
    if(in_array($db, $dbList)) {
      echo "DB '$db' already exists\n";
      return false;
    }


    $this -> pdo = $pdo;
    $this -> db  = $db;

    return $db;
  }

  //создаем базу данных
  public function createDb($app, $db) {

    $dir = getcwd();

    //создаем базу данных
    $this->pdo -> exec("CREATE DATABASE `$db` DEFAULT CHARACTER SET `utf8`;");

    //создаем конфиг базы данных
    $this->template("$dir/$app/config/db.php", 'db', ['name' => $db, 'user' => 'root', 'pass' => 'root']);

  }

  //создает каркас приложения
  public function create($app, $db) {

    $dir = getcwd();

    //создаем нужные папки и файлы
    exec("mkdir $app $app/public $app/public/assets $app/config $app/console");
    // TODO убрать эту строку если при fwc init все работает
    // exec("touch $app/composer.json $app/public/index.php $app/config/key.php $app/config/db.php");

    //создаем composer.json
    $this -> template("$dir/$app/composer.json", 'composer', ['app' => $app]);

    //создаем index.php
    $this -> template("$dir/$app/public/index.php", 'index');

    //создаем ключ
    $this -> template("$dir/$app/config/key.php", 'key', ['key' => $this->genPass(40)]);

    //создаем ini_set.php
    $this -> template("$dir/$app/ini_set.php", 'ini_set');

    //создаем fwc.php (консольная)
    $this -> template("$dir/$app/fwc.php", 'fwc');

    //создаем config/router - роутер для приложения
    $this -> template("$dir/$app/config/router.php", 'router');

    //создаем config/local - для локальных настроек приложения
    $this -> template("$dir/$app/config/local.php", 'local');

    //создаем .htaccess
    $this -> template("$dir/$app/public/.htaccess", 'htaccess');

    //создаем git-репозиторий
    exec("cd $app; git init > /dev/null 2>&1");

    //создаем .gitignore
    $this->template("$dir/$app/.gitignore", 'gitignore');

    //создаем базу данных
    $this -> createDb($app, $db);

    // делаем git init и первый коммит
    exec("git init");
    exec("cd $app; git add .; git commit -m 'App Init';");

    if($this->get('Do composer install?', 'y') === 'y') {
      system("cd $app; composer install");
    }

    //предлагаем создать удаленный репозиторий
    echo $this->color("Application '$app' created!", 'green')."\n";
    echo $this->color("cd $app", 'cyan')." - move to app`s folder\n";
    echo $this->color("fwc remote/init", 'cyan')." - create remote repository\n";

  }

  //клонирует удаленный репозиторий
  public function _clone() {

    //проверяет соединение с удаленным сервером
    $success = false;
    while(!$success) {

      $ip   = $this->get('Remote Server IP','188.120.226.13');
      $user   = $this->get('Remote Server Username','mrmasly');

      //exec("ssh $user@$ip ls", $ls, $status);
      exec("ssh $user@$ip 'echo 2>&1' && echo 'OK' || echo 'NOK'", $res, $status);

      if($res[1] == "OK") {
        $success = true;
      }
      else {
        echo "Failed\n";
      }
    }

    //получаем папку, с которой будем клонировать
    $path = $this->get('Remote Server Path');
    $path = explode('/', $path);
    $path = array_diff($path, ['']);
    $app = $path[sizeof($path)-1];
    $path = implode('/', $path);


    echo "Cloning into '$app'...\n";
    exec("git clone $user@$ip:$path > /dev/null 2>&1");

    $path = explode("/", $path);
    $dir = getcwd();


    //делаем composer install
    echo "Doing composer install...\n";
    exec("cd $app; composer install > /dev/null 2>&1");


    //создаем БД

    $db  = false;
    while(!$db) {
      $db = $this -> testDb( $this->get('DB name',$app) );
    }
    $this -> createDb($app, $db);

    //создаем config/local - для локальных настроек приложения
    $this -> template("$dir/$app/config/local.php", 'local');

    echo "App cloned successfully!\n";

  }



  //генерирует пароль из нужной длины
  private function genPass($number) {
    $arr = ['a','b','c','d','e','f',
      'g','h','i','j','k','l',
      'm','n','o','p','r','s',
      't','u','v','x','y','z',
      'A','B','C','D','E','F',
      'G','H','I','J','K','L',
      'M','N','O','P','R','S',
      'T','U','V','X','Y','Z',
      '1','2','3','4','5','6',
      '7','8','9','0'
    ];

    $pass = "";
    for($i = 0; $i < $number; $i++) {
      $index = rand(0, count($arr) - 1);
      $pass .= $arr[$index];
    }
    return $pass;
  }


  private function template($file, $template, $vars=[]) {

    $fp = fopen($file, 'w');

    $content = file_get_contents("templates/$template");
    $keys = [];
    $vals = [];
    foreach($vars as $id => $val) {
      $keys[] = "{{$id}}";
      $vals[] = $val;
    }

    $content = str_replace($keys, $vals, $content);

    fwrite($fp, $content);

    fclose($fp);

  }

  //выводит информацию цветом
  public function color($str, $color=null, $bgColor=null) {

    $colored_string = "";

    // Check if given foreground color found
    if(isset($this->colors[$color])) {
      $colored_string .= "\033[" . $this->colors[$color] . "m";
    }
    // Check if given background color found
    if(isset($this->bgColors[$bgColor])) {
      $colored_string .= "\033[" . $this->bgColors[$bgColor] . "m";
    }

    // Add string and end coloring
    return $colored_string .=  $str . "\033[0m";

  }

  private $colors = [
    'black' => '0;30',
    'dark_gray' => '1;30',
    'blue' => '0;34',
    'light_blue' => '1;34',
    'green' => '0;32',
    'light_green' => '1;32',
    'cyan' => '0;36',
    'light_cyan' => '1;36',
    'red' => '0;31',
    'light_red' => '1;31',
    'purple' => '0;35',
    'light_purple' => '1;35',
    'brown' => '0;33',
    'yellow' => '1;33',
    'light_gray' => '0;37',
    'white' => '1;37',
  ];

  private $bgColors = [
    'black' => '40',
    'red' => '41',
    'green' => '42',
    'yellow' => '43',
    'blue' => '44',
    'magenta' => '45',
    'cyan' => '46',
    'light_gray' => '47',
  ];


}

// > /dev/null 2>&1
