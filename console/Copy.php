<?php

namespace fw\console;

use fw\core\Console;
use fw\core\Log;
use fw\core\File;


class Copy extends Console {


  //копировать какие-либо файлы с других проектов
  public function index() {

    //выбор модуля
    $module = $this->getModule();

    //определяемся с проектом
    $dirs = scandir(dirname(DIR));
    $projects = [];
    foreach($dirs as $id => $dir) {
      if($dir{0} == '.') continue;
      $projects[] = $dir;
    }
    $project = $this->stdin('Из какого проекта будем копировать?', $projects);


    //что будем копировать
    $types = [
      'Контроллеры', 'Таблицы', 'Модели', 'Консольные',
    ];
    $type = $this->stdin('Что будем копировать?', $types);

    if($type == 'Модели')       $type = 'models';
    elseif($type == 'Контроллеры')   $type = 'controllers';
    elseif($type == 'Таблицы')     $type = 'tables';
    elseif($type == 'Консольные')   $type = 'console';

    $dir1 = dirname(DIR)."/$project/*/$type/*.php";
    $dir2 = dirname(DIR)."/$project/*/$type/*/*.php";

    $all = array_merge(glob($dir1), glob($dir2));

    //если тип - консоль
    if($type == 'console') {
      $all = glob(dirname(DIR)."/$project/console/*.php");
    }


    foreach($all as $id => $one) {
      $all[$id] = str_replace(dirname(DIR)."/$project", '', $one);
    }


    $files = $this->stdin("Выберите файлы", $all, true);


    foreach($files as $id => $file) {

      //если тип - контроллер
      if($type == 'controllers') {

        //определяем нужные переменные с файла
        $controller = explode('/', $file);
        $controller = $controller[sizeof($controller)-1];
        $controller = str_replace('.php', '', $controller);
        $name = strtolower(str_replace('Controller', '', $controller));
        $views = dirname(DIR)."/$project/".explode('/', $file)[1].'/views/'.$name;
        $models = dirname(DIR)."/$project/".explode('/', $file)[1].'/models/'.$name;
        $file = dirname(DIR)."/$project".$file;


        //получаем новое имя контроллера
        $newName = $this->stdin($controller, $name);
        $newController = ucfirst($newName).'Controller';
        $newFile = DIR.'/'.$module.'/controllers/'.$newController.'.php';
        $newViews = DIR.'/'.$module.'/views/'.$newName.'/';
        $newModels = DIR.'/'.$module.'/models/'.$newName.'/';

        //копируем файл контроллера
        copy($file, $newFile);

        //копируем папку представления
        if(file_exists($views)) {
          exec("cp -Rf $views $newViews"); }

        //копируем папку моделей
        if(file_exists($models)) {
          exec("cp -Rf $models $newModels"); }


        //echo $this->color('OK', 'green');


      }

      elseif($type == 'console') {

        $name = explode('/', $file);
        $name = $name[sizeof($name)-1];
        $name = str_replace('.php', '', $name);
        $name = strtolower($name);
        $file = dirname(DIR)."/$project".$file;

        $newName = $this->stdin($name, $name);
        $newFile = DIR.'/console/'.ucfirst(strtolower($name)).'.php';

        //копируем файл
        copy($file, $newFile);

      }

       else {


        $name = explode('/', $file);
        unset($name[0], $name[1], $name[2]);
        $name = implode('/', $name);
        $name = str_replace('.php', '', $name);
        $file = dirname(DIR)."/$project".$file;

        $newName = $this->stdin($name, $name);
        $newFile = DIR.'/'.$module.'/'.$type.'/'.$newName.'.php';

        File::mkdir($newFile);
        copy($file, $newFile);


      }
    }



  }









}
