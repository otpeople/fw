<?php

namespace fw\console;

use fw\core\Console;
use fw\core\Db;
use fw\core\Log;

class Update extends Console {

  public function index($type=null, ...$params) {

    if(!isset($type)) {

      $this -> tables();

    } elseif(method_exists($this, $type)) {
      $this -> $type(...$params);
    }


  }



  //обновляет все таблицы
  public function tables($onlyModule=null, $onlyTable=null) {
    
    $models = [];

    //таблицы фреймворка
    $files = array_diff(scandir(FW."/tables"), ['.', '..', '.DS_Store']);
    foreach($files as $file) {
      $model = str_replace(".php", '', $file);
      if($model == $file) { continue; }
      if(isset($onlyModule) && $onlyModule != 'fw') { continue; }
      $models[] = "\\fw\\tables\\$model";
    }

    //сканируем главную директория, чтобы узнать какие есть модули
    $modules = array_diff(scandir(DIR), ['..','.','.git','config','console','public','vendor']);
    foreach($modules as $module) {
      $file = DIR.'/'.$module;

      if(!is_dir($file)) { continue; }

      $files = DIR.'/'.$module .'/tables';
      if(!file_exists($files)) { continue; }

      $files = array_diff(scandir($files), ['..','.','.DS_Store']);
      foreach($files as $file) {
        $model = str_replace(".php", '', $file);
        if($model == $file) { continue; }

        //если указан модуль и эта модель не в этом модуле
        if(isset($onlyModule) && $module != $onlyModule) { continue; }

        //если указана модель и это не та модель
        if(isset($onlyTable) && strtolower($model) != strtolower($onlyTable)) { continue; }


        $models[] = "\\$module\\tables\\$model";
      }

    }


    $tables = Db::getTables();


    if(isset($onlyModule)) {
      foreach($tables as $table => $time) {
        $modName = strtolower(explode('_', $table)[0]);
        if($onlyModule != $modName) {
          //если это не нужный модуль
          unset($tables[$table]);
        }
      }
    }



    foreach($models as $model) {
      if(class_exists($model)) {
        $model = new $model();
        $table = $model -> table;

        $colorTable = $this->color($table, "cyan");


        //если таблицы нет - создаем
        if(!isset($tables[$table])) {
          echo "Table $colorTable ... ";
          $this -> _createTable($model);
          echo $this->color("created!", 'green')."\n";
        }

        else {
          $ft = filemtime($model->file);

          //еслу нужно обновить
          if($tables[$table] != $ft) {
            echo "Table $colorTable ... ";
            $this -> _changeTable($model);
            echo $this->color("updated!", 'yellow')."\n";
          }


          //удаляем название таблицы из массива
          unset($tables[$table]);

        }


      }
    }


    //удаляем таблицы, к которым нет моделей
    if(!empty($tables) && !isset($onlyTable)) {
      echo "\nThere is tables that have no file:\n";
      foreach($tables as $table => $time) {
        echo "\t$table\n";
      }
      //спрашиваем, нужно ли их удалять
      if($this->stdin('To delete this tables?','y') == 'y') {

        //удаляем эти таблицы
        foreach($tables as $table => $time) {
          Db::query("DROP table `$table`");
        }
      }
    }



  }

  //обновляет фреймворк
  public function fw() {
    $this -> exec('composer update mrmasly/fw');
  }





  //создает таблицу в БД
  private function _createTable($table) {

    //получаем поля для sql-таблицы
    list($fields, $index) = $this -> _tableFieldsHelper($table);

    $fields = implode(",\n\t", $fields);
    $dop = [];
    foreach($index as $field => $ind) {
      if($ind === 'PRIMARY KEY') $dop[] = "PRIMARY KEY (`$field`)";
      elseif($ind === 'UNIQUE KEY') $dop[] = "UNIQUE KEY `$field` (`$field`)";
    }

    $dop = implode(",\n\t", $dop);


    $str = "\nCREATE TABLE IF NOT EXISTS `".$table->table."` ( \n\t".$fields. ",\n\t$dop\n) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '".filemtime($table->file)."'";

    //Log::debug($str);
    //echo $str;
    Db::query($str);

  }

  //изменяет таблицу в БД
  private function _changeTable($table) {

    //получаем поля для sql-таблицы
    list($fields, $index) = $this -> _tableFieldsHelper($table);

    //получаем все поля
    $colums = Db::query("SHOW COLUMNS FROM ".$table->table);

    //проходимся по всем полям таблицы
    while($row = mysqli_fetch_assoc($colums)) {

      $field = $row["Field"];

      //если теперь такого поля нет - удаляем его
      if(!isset($fields[$field])) {
        $str = "ALTER TABLE ".$table->table." DROP COLUMN `".$field."`";
        Db::query($str);
      }

      //иначе обновляем его
      else {
        $str = "ALTER TABLE ".$table->table." MODIFY `".$field."` ".str_replace('`'.$field.'`', "", $fields[$field]);
        unset($fields[$field]);
      }

      //echo $str."\n";
      Db::query($str);

    }

    //оставшиеся поля - новые - сохраняем их
    foreach($fields as $field => $f) {
      $str = "ALTER TABLE ".$table->table." ADD ".$fields[$field];
      //echo $str."\n";
      Db::query($str);
    }


    //получаем все индексы таблицы
    $tableIndex = Db::query("SHOW INDEX FROM ".$table->table);

    //проходимся по всем индексам таблицы
    while($row = mysqli_fetch_assoc($tableIndex)) {
      $field = $row['Column_name'];

      //если индекс не существует - удаляем из БД
      if(!isset($index[$field])) {
        $str = "ALTER TABLE `".$table->table."` DROP INDEX `$field`;";
        Db::query($str);
      }

      //если индекс существует - удаляем из массива
      else unset($index[$field]);

    }

    //сохраняем каждый индекс, которого не было в БД
    foreach($index as $field => $ind) {
      $str = "ALTER TABLE `".$table->table."` ADD UNIQUE INDEX (`$field`);";
      Db::query($str);
    }



    //сохраняем время изменения таблицы (время файла таблицы)
    $str = "ALTER TABLE `".$table->table."` COMMENT = '".filemtime($table->file)."';";
    //echo $str."\n";
    Db::query($str);


  }


  //формирует sql-поля из fields тпблицы
  private function _tableFieldsHelper($table) {


    $index = [];

    $sql = [];
    foreach($table->fields as $field => $param) {

      // комментарий
      $comment = $param["title"];

      // тип
      $type = $param["sql"]["type"];

      // размер
      $size = ($param["sql"]["size"] === null) ? '' : '('.$param["sql"]["size"].')';

      // null не null
      $null = ($param["sql"]["null"] === null) ? '' : ( ($param["sql"]["null"]) ? 'NULL' : 'NOT NULL' );

      // default
      $default = ($param["sql"]['default'] === null) ? '' : "DEFAULT '".$param["sql"]["default"]."'";
      if(isset($param["sql"]["increment"])) $default = 'AUTO_INCREMENT';

      // index
      if($param['sql']["index"]) {
        $index[$field] = $param['sql']["index"];
      }

      $sql[$field] = "`$field` $type $size $null $default COMMENT '$comment'";

    }
    return [$sql, $index];


  }






}
