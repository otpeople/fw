<?php

namespace fw\console;

use fw\core\{Console, App, File, Log};

class Remote extends Console {

  protected $server; //иформация о сервере
  protected $remote; //строка-путь к серверу для ssh
  protected $offFile; //файл, который говорит о том что приложение выключено
  protected $php;

  protected function getPhp() {
    if(!isset($this->php)) {

      //определяем имя и директорию проекта на сервере
      $server = $this->server;
      $remote = $this->remote;
      $path = $server["path"];

      //получаем локальные настройки сервера для определения php
      $remoteDir = $remote.":".$path."/config";
      $remoteFiles = $this->exec("ssh $remote 'cd $path/config; ls'");
      //если найден config/local.php на сервере
      if(in_array('local.php', $remoteFiles)) {
        $remoteFile = $remoteDir."/local.php";
        $tmpDir = DIR.'/files/'.md5(uniqid());
        File::mkdir($tmpDir);
        //копируем его во временную папку
        $this->exec("rsync -a $remoteFile $tmpDir");
        $tmpFile = $tmpDir.'/local.php';
        $config = require($tmpFile);
        if(isset($config["php"])) {
          $php = $config["php"];
        } else {
          $php = 'php';
        }
        //удаляем временную папку
        $this->exec("rm -rf $tmpDir");

      } else {
        $php = 'php';
      }
      $this->php = $php;
    }
    return $this->php;
  }


  //конструктор
  public function __construct() {
    $this->server = App::getConfig('server');

    if($this->server) {
      $server = &$this->server;
      $this->remote = $server["user"].'@'.$server["ip"];
      $this->offFile = DIR.'/off.txt';
    }
  }

  //распределитель
  public function index($method, ...$args) {

    if(method_exists($this, $method)) {
      $this -> $method(...$args);
    } else {
      Log::err("fw\console\Db::$method - команда не найдена");
    }

  }


  //отправить файлы
  public function get($dir, $flag="a", $delete=false) {

    // добавляем / в начале
    if($dir{0} != '/') { $dir = "/".$dir; }

    // если передан не абсолютный путь - делаем его абсолютным
    if(strpos($dir, DIR) === false) { $dir = DIR.$dir;}

    //определяем директорию на сервере
    $server = $this->remote.':'.$this->server['path'].str_replace(DIR, '', $dir);

    // директория на локалке
    $dir = dirname($dir);

    //копируем
    $rsync = "rsync -$flag $server $dir";

    if($delete) { $rsync .= " --delete"; }

    $this -> exec($rsync);

  }

  //получить файлы
  public function send($dir, $flag="a", $delete=false) {

    // добавляем / в начале
    if($dir{0} != '/') { $dir = "/".$dir; }

    // если передан не абсолютный путь - делаем его абсолютным
    if(strpos($dir, DIR) === false) { $dir = DIR.$dir;}

    //определяем директорию на сервере
    $server = $this->remote.':'.$this->server['path'].dirname(str_replace(DIR, '', $dir)).'/';

    //копируем
    $rsync = "rsync -$flag $dir $server";

    if($delete) { $rsync .= " --delete"; }

    $this -> exec($rsync);

  }

  //выполняет команду на удаленном сервере
  public function make(...$args) {

    if(empty($args)) { echo 'Please, use commands'; exit; }

    $args = implode(' ', $args);

    //определяем имя и директорию проекта на сервере
    $server = $this->server;
    $remote = $this->remote;
    $path = $server["path"];

    $php = $this->getPhp();

    //собираем команду
    $command = "ssh $remote 'cd $path; $php fwc.php $args'";

    system($command);

  }




  //выключаем сервер
  public function off($afterTime=10, $offTime="5-10") {

    // записываем файл off.json
    $file = DIR.'/off.json';
    File::write($file, json_encode([
      'title'   => "Запланированный технический перерыв",
      'message' => "Через $afterTime минут на сервере будут проводится технические работы, которые продляться $offTime минут.\nСпасибо за понимание!",
      'off'     => false,
    ]));

    // отправляем файл на сервер
    $this->send($file);

    // удаляем файл на локальной
    unlink($file);

    // ожидаем, пока не придет время
    while($afterTime > 0) {
      echo "До остановки: ".$this->color($afterTime, 'cyan')." минут\n";
      sleep(60);
      $afterTime--;
    }

    // создаем файл
    File::write($file, json_encode([
      'title' => 'Запланированный технический перерыв',
      'message' => "На сервере проводятся технические работы, которые продлятся $offTime минут.\nПриносим свои извинения за доставленные неудобства.\nСпасибо за понимание!",
      'off' => true,
    ]));

    // отправляем файл на сервер
    $this->send($file);

    // удаляем файл на локальной
    unlink($file);

    // останавливаем ws-серверы
    $this->do('kill');

    echo $this->color('Приложение остановлено!', 'green')."\n";

  }

  //включаем сервер
  public function on() {

    //определяем имя и директорию проекта на сервере
    $server = $this->server;
    $remote = $this->remote;
    $path = $server["path"];

    //удаляем файл off.json на сервере
    $command = "ssh $remote 'cd $path; rm off.json'";
    system($command);

  }


  // создать удаленый репозиторий
  public function init() {

    // получаем название приложения
    $app = str_replace(dirname(DIR).'/', '', DIR);

    //проверяем чтобы не было удаленных соединений
    exec("cd ".DIR."; git remote", $remote);
    if(!empty($remote)) {
      echo $this->color("Remote repository already exists", 'red')."\n";
      exit;
    }

    // запрашиваем информацию о удалыенном сервере,
    // пока не получится подключиться
    $success = false;
    while(!$success) {

      $ip   = $this->stdin('Remote Server IP','188.120.226.13');
      $user = $this->stdin('Remote Server Username','mrmasly');

      //exec("ssh $user@$ip ls", $ls, $status);
      exec("ssh $user@$ip 'echo 2>&1' && echo 'OK' || echo 'NOK'", $res);

      if($res[1] == "OK") {
        $success = true;
      }
      else {
        echo $this->color("Connection error", 'red')."\n";
      }
    }


    //проверяет путь на удаленном сервере (и создает нужные папки)
    $success = false;
    while(!$success) {
      $path = $this->stdin('Remote Server Path', "apps/$app");
      $path = explode("/", $path);
      $path = array_diff($path, ['']);


      //полный путь
      $full = [];
      $ls = []; //временный список папок

      //получаем первый список файлов
      exec("ssh $user@$ip ls", $res);
      $ls = $res;
      $i = 0;
      $arr = [];

      foreach($path as $p) {
        $i++;

        // $p - название текущей папки
        // $f - полный путь к текущей папке
        $full[] = $p;
        $f = implode("/", $full);

        //если папки нет - создаем
        if(!in_array($p, $ls)) {
          exec("ssh $user@$ip mkdir $f");
          echo "mkdir ".$this->color($f, 'cyan')." \n";
        }

        exec("ssh $user@$ip ls $f", $arr[$i]);
        $ls = $arr[$i];
      }


      $path = implode("/", $path);


      //если эта папка действительно пуста
      if(empty($ls)) {
        $success = true;
      } else {
        echo "$path - not empty\n";
        $success = false;
      }

    }

    //создаем файл config/server.php с данными о сервер
    $file = DIR.'/config/server.php';
    $content = "<?php return [";
    $content.= "\n  'ip'   => '$ip',";
    $content.= "\n  'user' => '$user',";
    $content.= "\n  'path' => '$path'";
    $content.= "\n];";
    File::write($file, $content);

    // делаем коммит с этим новым файлом
    exec("git add config/server.php; git commit -m 'Added remote repository';");


    // инициализируем git удаленно
    exec("ssh $user@$ip 'cd $path; git init'");
    exec("git remote add origin mrmasly@188.120.226.13:apps/reotan");
    //разрешаем перезапись текущей ветки
    exec("ssh $user@$ip 'cd $path; git config receive.denyCurrentBranch ignore'");

    //создаем хук на сервере
    $dir = DIR.'/files/tmp';
    File::mkdir($dir);
    $file = $dir.'/post-receive';
    $content = "#!/bin/bash\ncd ..\nGIT_DIR='.git'\ngit reset --hard\n";
    $content.= "if [ -f composer.json ]; then\n  composer install\nfi";
    $fp = fopen($file, 'w');
    fwrite($fp, $content);
    fclose($fp);
    exec("scp $file $user@$ip:$path/.git/hooks");
    exec("ssh $user@$ip chmod +x $path/.git/hooks/post-receive");
    unlink($file);

    //создаем базу данных на сервер
    $passdb = $this->genPass(20);
    $userdb = "user_".strtolower($this->genPass(10));
    echo "Your username: $userdb\n";
    echo "Your password: $passdb\n";
    echo "Add new DB [$app] to server with the password\n";

    //проверяем что введенная БД действительно есть на сервере
    $success = false;
    while(!$success) {
      $db = $this->stdin('Your DB name', $app);

      //пробуем сделать дамп базы в /dev/null
      exec("ssh $user@$ip 'mysqldump --no-data -u$userdb -p$passdb $db > /dev/null 2>&1'", $r, $s);

      //если не выдаст ошибку - значит все в порядке
      if($s == 0) { $success = true; }
      else { echo $this->color("can't connect to remote DB", 'red')."\n"; }
    }

    //делаем первый commit и push
    echo $this->color("Uploading...", 'cyan')."\n";
    exec("git push origin master > /dev/null 2>&1");

    //провисываем настройки DB на сервере
    $file = DIR.'/files/tmp/db.php';
    $content = "<?php return [";
    $content.="\n  'type' => 'mysql',";
    $content.="\n  'host' => '127.0.0.1',";
    $content.="\n  'name' => '$db',";
    $content.="\n  'user' => '$userdb',";
    $content.="\n  'pass' => '$passdb',";
    $content.="\n];";
    File::write($file, $content);
    exec("scp $file $user@$ip:$path/config");
    unlink($file);

    // все получилось!
    echo $this->color('Remote repository added successfully!', 'green')."\n";



  }



  //генерирует пароль из нужной длины
  private function genPass($number) {
    $arr = ['a','b','c','d','e','f',
      'g','h','i','j','k','l',
      'm','n','o','p','r','s',
      't','u','v','x','y','z',
      'A','B','C','D','E','F',
      'G','H','I','J','K','L',
      'M','N','O','P','R','S',
      'T','U','V','X','Y','Z',
      '1','2','3','4','5','6',
      '7','8','9','0'
    ];

    $pass = "";
    for($i = 0; $i < $number; $i++) {
      $index = rand(0, count($arr) - 1);
      $pass .= $arr[$index];
    }
    return $pass;
  }



}
