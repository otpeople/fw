<?php

namespace fw\console;


use fw\core\Console;
use fw\core\Log;
use fw\core\App;

use Jobby\Jobby;

use Jenner\Crontab\Mission;
use Jenner\Crontab\Crontab;


class Cron extends Console {


  public function index() {

    //определяем путь к php файлу
    $php = App::getConfig('local', 'php');
    if(!$php) {
      exec("which php", $php);
      $php = $php[0];
    }

    $log = DIR.'/logger.log';


    //смотрим консольные скрипты текущего приложения
    $scripts = [];
    foreach(scandir(DIR.'/console') as $one) {
      if(strpos($one, '.php')) { $scripts[] = str_replace('.php', '', $one);}
    }


    //массив для хранения всех крон-задач
    $tasks = [];

    foreach($scripts as $one) {

      //получаем объект
      $obj = "\\console\\$one";
      $obj = new $obj();

      //смотрим есть ли крон-задание
      if(!isset($obj->cron)) continue;

      //если передана строка, значит это относится к экшену index
      if(is_string($obj->cron)) {  $obj->cron = ['index' => $obj->cron]; }

      //проходимся по каждому заданию в этом объекте
      foreach($obj->cron as $action => $time) {

        //смотрим чтобы метод существовал
        if(!method_exists($obj, $action)) {
          Log::err("console\\$one: вызов несуществуещего метода $action по крону");
          continue;
        }

        //строим bash-строку запроса
        $cmd = "cd ".DIR."; $php fwc.php $one/$action";

        //сохраняем задание
        $tasks[] = new Mission($one, $cmd, $time);

      }



    }


    //запускаем все задания
    $crontab_server = new Crontab(null, $tasks);
    $crontab_server->start(time());



  }

}
