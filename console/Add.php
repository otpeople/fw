<?php

namespace fw\console;

use fw\core\{Console, File};
use fw\console\Update;

class Add extends Console {


  public function index($action, ...$vars) {
    if(method_exists($this, $action)) {
      $this -> $action(...$vars);
    }
  }

  //создает модель
  public function table($module=null, $name=null) {

    //получаем название модуля
    $module = $this -> getModule($module);

    //получаем имя таблицы
    $success = false;
    while(!$success) {
      if(!isset($name)) {
        $name = $this->stdin('Table name');
      }
      $name = ucfirst($name);
      $file = DIR."/$module/tables/$name.php";
      //смотрим чтобы такой таблицы не существовало
      if(file_exists($file)) {
        echo "Table $name already exists\n";
        $name = null;
      } else {
        $success = true;
      }
    }

    //спрашиваем поля
    $fields = $this->stdin('Fields');
    $fields = explode(" ", $fields);
    foreach($fields as $id => $field) {
      unset($fields[$id]);
      $fields[$field] = ['title' => ''];
    }


    //на всякий случай (для связей) получаем все другие таблицы
    $tables = scandir(DIR."/$module/tables");
    foreach($tables as $id => $table) {
      if(!strpos($table, '.php')) {
        unset($tables[$id]);
        continue;
      }

      $tables[$id] = str_replace('.php', '', $table);
    }



    //добавляем к полям заголовки
    if($this->stdin('Add any information about fields','y') == 'y') {

      $add = ['title', 'type', 'relation'];
      $add = $this -> stdin('What do you want to add to fields', $add, true);

      foreach($fields as $id => $field) {

        echo "\nField $id\n";

        if(in_array('title', $add)) {
          echo "\t";
          $field["title"] = $this->stdin('title');
        }


        if(in_array('type', $add)) {
          echo "\t";
          $type = $this->stdin("type", 'varchar');
          if($type != 'varchar') {
            $field["type"] = $type;
          }
        }

        //связи
        if(in_array('relation', $add)) {
          echo "\t";
          if($this->stdin('relation','no') != 'no') {
            echo "\t";
            $table = $this -> stdin('table', $tables);
            echo "\t";
            $more  = $this -> stdin('index/title','id/title');
            $more = explode('/', $more);
            $more = [
              'table' => $table,
              'index' => $more[0],
              'title' => $more[1],
            ];
            $field["relation"] = $more;
          }
        }



        $fields[$id] = $field;

      }

    }


    $content = '';
    $t = '  ';
    foreach($fields as $field => $cont) {
      $content.= $t.$t."'".$field."' => [";
      foreach($cont as $id => $val) {
        $content.= "\n".$t.$t.$t."'".$id."' => ";
        if(is_array($val)) {
          $content.= "[";
          foreach($val as $i => $v) {
            $content.= "\n".$t.$t.$t.$t."'$i' => '$v',";
          }
          $content.= "\n".$t.$t.$t."],";
        } else {
          $content.= "'$val',";
        }

      }
      $content.= "\n".$t.$t."],\n";
    }

    //создаем файл таблицы
    $this -> template($file, 'Table', ['module'=>$module, 'name'=>$name, 'fields' => $content]);

    //создаем таблицу в БД
    require($file);
    (new Update()) -> tables($module, $name);


  }


  //создает модуль
  //TODO при добавлении модуля создавать .gitignore (files/tables)
  public function module($name=null) {
    if(!isset($name)) {
      $name = $this->stdin('Module name');
    }

    //проверяем существует ли уже такой модуль
    $dir = DIR."/$name";
    if(file_exists($dir)) {
      echo "Module $name exists"; exit;
    } else {

      //добавляем в composer.json
      $composer = $this->dir.'/composer.json';
      $array = json_decode(file_get_contents($composer), true);

      //смотрим - может какие модули нужно удалить
      foreach($array['autoload']['psr-4'] as $appname => $dirname) {
        if(!file_exists(DIR.'/'.$dirname)) {
          unset($array['autoload']['psr-4'][$appname]);
        }
      }
      //добавлям название нового модуля
      $array['autoload']['psr-4']["$name\\"] = $name;
      file_put_contents($composer,
        str_replace('\\/', '/', json_encode($array, JSON_PRETTY_PRINT)));
      $this -> exec("cd ".$this->dir."; composer dumpautoload", false);

      //создаем папку
      mkdir($dir);

      //создаем нужные папки в модуле
      $dirs = ['tables','models','controllers','files','views'];
      foreach($dirs as $one) {
        mkdir("$dir/$one");
      }

      //спрашиваем - нужна ли авторизация модуля
      if($this->stdin('To add authorization in the module?','yes') == 'yes') {


        //создаем модель
        $tableName = 'Users';
        $tableGroupName = $tableName.'Group';
        $fileTable = "$dir/tables/$tableName.php";
        $fileTableGroup = "$dir/tables/$tableGroupName.php";
        $this->template($fileTable, 'UsersTable', [
          'module' => $name,
          'table' => $tableName,
        ]);
        $this->template($fileTableGroup, 'UsersGroupTable', [
          'module' => $name,
          'table' => $tableGroupName,
        ]);

        //подключаем сгенерированные файлы
        require($fileTable);
        require($fileTableGroup);

        //генерируем таблицы в БД
        $table = new Update();
        $table->tables('fw');
        $table->tables($name, $tableName);
        $table->tables($name, $tableGroupName);

        //создаем файл access
        $fileAccess = "$dir/access.php";
        $this->template($fileAccess, 'Access', [
          'table' => $tableName,
          'group' => $tableGroupName,
        ]);

        //создаем администратора
        $adminName = $this->stdin('Admin name', 'admin');
        $adminTitle = $this->stdin('Admin title', 'Администратор');
        $usersTable = "\\$name\\tables\\$tableName";
        $users = new $usersTable();

        //чтобы пароль был правильной длины
        $success = false;
        while(!$success) {
          $adminPass = $this->stdin('Admin pass');
          $success = $users->insert([
            'name' => $adminName,
            'pass' => $adminPass,
            'title' => $adminTitle,
          ]);
          if(!$success)
            echo "A minimum of 7 characters (including Latin 1 letter)\n";
        }


      }

      echo "Module created!\n";


    }


  }


  //создает контроллер
  //TODO при создании файла вместо табов использовать пробелы
  public function controller($module=null, $name=null) {

    //получаем название модуля
    $module = $this -> getModule($module);

    //получаем имя контроллера
    $success = false;
    while(!$success) {
      if(!isset($name)) {
        $name = $this -> stdin('Controller name');
      }
      $name = ucfirst(strtolower($name));
      $file = DIR."/$module/controllers/{$name}Controller.php";
      //смотрим чтобы такаго контроллера не существовало
      if(file_exists($file)) {
        echo "Controller $name already exists\n";
        $name = null;
      } else {
        $success = true;
      }

    }

    //создаем файл контроллера
    $this -> template($file, 'Controller', ['module'=>$module, 'name'=>$name]);


    //создаем папку для представлений
    $lName = strtolower($name);
    $viewsDir = DIR."/$module/views/$lName";
    mkdir($viewsDir);

    //создаем файл с первым представление
    $this -> template($viewsDir."/index.php", 'View', ['name' => $name]);

  }




}
