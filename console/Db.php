<?php

namespace fw\console;

use fw\core\App;
use fw\core\File;
use fw\core\Log;
use fw\core\Console;
use fw\console\Remote;


class Db extends Console {

  public $tables = true; // нужно синхронизировать сами таблицы
  public $files  = true; //нужно синхронизировать файлы


  public $tmpDir;  //папка для временных файлов
  public $db;      //информация о БД
  public $remote; //объект класса fw\console\Remote;

  // если не пустое - то только эти таблицы нужно синхронизировать
  public $only;

  function __construct() {

    //определяем папку, куда будут скалдыватся дампы
    $this->tmpDir = DIR.'/files/tmp/tmp_sync';
    File::mkdir($this->tmpDir);

    // получаем config DB
    $this->db = App::getConfig('db');

    // fw\console\Remote для синхронизации с сервером
    $this->remote = new Remote();
  }


  //распределитель
  public function index($method, ...$vars) {

    if(method_exists($this, $method)) {
      $this -> $method(...$vars);
    } else {
      Log::err("fw\console\Db::$type - команда не найдена");
    }

  }




  //загрузить таблицы на сервер
  public function send(...$keyWords) {

    // получаем названия таблиц
    $tables = $this->choseTables(...$keyWords);

    // проходимся по каждой таблице
    foreach ($tables as $table) {

      echo $this->color($table, 'cyan')."\n";
      $this->progress(0, 5);

      // синхронизируем таблицы
      if($this->tables) {
        // делаем дамп
        $this->dump($table);
        $this->progress(1, 5);

        // Отправляем .sql файл
        $this->remote->send($this->_getTableFile($table));
        $this->progress(2, 5);

        // Принимаем и загружаем .sql файл на сервере
        $this->remote->make('db', 'load', $table);
        $this->progress(3, 5);

        // Удаляем .sql файл
        $this->clear($table);
        $this->remote->make('db', 'clear', $table);
        $this->progress(4, 5);
      }

      // синхронизируем файлы
      if($this->files) {
        // Отправляем файлы таблицы
        $files = $this->_getTableDir($table);
        $this->remote->send($files);
      }

      $this->progress(5, 5);

    }

  }

  //копирует базу на локальную машину
  public function get(...$keyWords) {

    // получаем названия таблиц
    $tables = $this->choseTables(...$keyWords);

    // проходимся по каждой таблице
    foreach ($tables as $table) {

      echo $this->color($table, 'cyan')."\n";
      $this->progress(0, 5);

      // синхронизируем таблицы
      if($this->tables) {
        // делаем дамп
        $this->remote->make('db', 'dump', $table);
        $this->progress(1, 5);

        // Принимаем .sql файл
        $this->remote->get($this->_getTableFile($table));
        $this->progress(2, 5);

        // Принимаем и загружаем .sql файл на локальной машине
        $this->load($table);
        $this->progress(3, 5);

        // Удаляем .sql файл
        $this->clear($table);
        $this->remote->make('db', 'clear', $table);
        $this->progress(4, 5);
      }

      // синхронизируем файлы
      if($this->files) {
        // Отправляем файлы таблицы
        $files = $this->_getTableDir($table);
        $this->remote->get($files);
      }

      $this->progress(5, 5);

    }

  }





  //создает дампы нужной таблицы
  public function dump($table) {

    $file = $this->_getTableFile($table);

    $db = $this->db;
    $user = $db["user"];
    $pass = $db["pass"];
    $name = $db["name"];

    $command = "mysqldump -u$user -p$pass $name $table > $file";
    $this->exec($command);

  }

  // загрузить данные из .sql фала в БД
  public function load($table) {

    $file = $this->_getTableFile($table);

    // получаем конфиги БД
    $db = $this->db;
    $user = $db["user"];
    $pass = $db["pass"];
    $name = $db["name"];

    $command = "mysql -u{$user} -p{$pass} {$name} < $file";
    exec($command);

  }

  // удаляет .sql файл таблицы
  public function clear($table) {
    $file = $this->_getTableFile($table);
    if(file_exists($file)) {
      unlink($file);
    }
  }





  // получить имя файла таблицы
  private function _getTableFile($table) {
    return $this->tmpDir.'/'.$table.'.sql';
  }

  // получить директорию таблицы (где храняться файлы таблиц)
  private function _getTableDir($table) {

    //определяем название таблицы в CamelCase ($tableCC)
    $mod = explode('_', $table)[0];
    $mod = strtolower($mod);
    $tableCC = "";
    $upperNext = false;
    for($i=strlen($mod);$i<strlen($table);$i++) {
      $char = $table{$i};
      if($char !== '_') {
        $tableCC .= ($upperNext) ? strtoupper($char) : $char;
      }
      $upperNext = ($char === '_') ? true : false;
    }

    // возвращаем путь к папку с таблицами
    return DIR."/$mod/files/tables/$tableCC";

  }





  //помогает определить, какие таблицы нужно дампить
  private function choseTables(...$keyWords) {

    // если есть установленные, обязательные таблицы - возвращаем их
    if(isset($this->only)) {
      return $this->only;
    }

    //получаем все модули
    $modules = json_decode(file_get_contents(DIR.'/composer.json'), true)['autoload']['psr-4'];
    unset($modules['console\\']);
    if(isset($modules['fw\\'])) {
      unset($modules['fw\\']);
    }
    $modules = array_values($modules);

    //сохраняем массив всех модулей
    $mods = $modules;

    //оставляем только нужные модули
    // $modules = $this->stdin('Chose modules', $modules, true);

    //смотрим все таблицы
    $tables = [];
    foreach($modules as $mod) {
      $dir = DIR.'/'.$mod.'/tables';
      foreach(scandir($dir) as $file) {
        if(strpos($file, '.php')) {
          $tables[] = $mod.' | '.str_replace('.php', '', $file);
        }
      }
    }

    // если переданы ключевые слова - оставляем только эти таблицы
    if(!empty($keyWords)) {
      $tables = array_map(function($table) use ($keyWords) {
        foreach ($keyWords as $word) {
          if(strpos(strtolower($table), strtolower($word)) !== false) {
            return $table;
          }
        }
        return null;
      }, $tables);
    }

    $tables = array_diff($tables, [null]);
    $tables = array_values($tables);

    $tables = $this -> stdin('Chose tables', $tables, true);

    //переводим название таблиц так, как они есть в БД
    $tables = array_map(function($table) {

      //получаем название модуля и таблицы
      list($module, $table) = explode(' | ', $table);

      //длина слова
      $lenght = strlen($table);

      $name = '';

      //проходимся по каждой букве таблицы
      for($i=0; $i < $lenght; $i++) {
        if($table{$i} == ucfirst($table{$i}) && $i != 0) {
          $name .= "_";
        }
        $name .=lcfirst($table{$i});
      }

      $name = strtoupper($module) . '_' . $name;

      return $name;

    }, $tables);



    return $tables;

  }


}
