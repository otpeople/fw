<?php

namespace fw\console;

use fw\core\{Console, Date, File};

class Log extends Console {


  public $watch = false;

  //статусы состояния в логах
  public $status = [
    0 => [
      'type' => 'emerg',
      'color' => 'red',
    ],
    1 => [
      'type' => 'alert',
      'color' => 'purple',
    ],
    2 => [
      'type' => 'crit',
      'color' => 'red',
    ],
    3 => [
      'type' => 'err',
      'color' => 'red',
    ],
    4 => [
      'type' => 'warn',
      'color' => 'yellow',
    ],
    5 => [
      'type' => 'notice',
      'color' => 'light_cyan'
    ],
    6 => [
      'type' => 'info',
      'color' => 'cyan',
    ],
    7 => [
      'type' => 'debug',
      'color' => 'blue',
    ],
  ];


  // выполняем архивацию в полночь
  public $cron = [
    'archive' => '0 0 * * *',
  ];

  // архивиреут логи
  public function archive() {
    $file = DIR.'/logger.log';
    if(file_exists($file)) {

      $str = file($file)[0];
      if($str{0} == '[') {
        $str = substr($str, 1);
        $str = explode(' ', $str)[0];
      } else {
        $str = explode('T', $str)[0];
      }
      $str = Date::parse($str)->format('Y-m-d');

      if($str != date('Y-m-d')) {

        $dist = DIR.'/files/logs/'.$str.'.log';
        File::mkDir($dist);
        rename($file, $dist);
        File::write($file, '');
      }
    }
  }

  // очистить лог файл
  public function clear() {
    File::write(DIR.'/logger.log', '');
  }

  public function index($one) {

    $this->archive();

    $types = []; //типы


    $watch = ($this->flag('w')) ? true : false;
    if($one == 'all') $this->types = range(0, 7);

    $this->date = date('Y-m-d');


    $file = DIR.'/logger.log';
    if(!file_exists($file)) {
      sleep(1);
      return $this->index($one);
    }


    //если типы не указаны - спросим какие типы нам нужны
    if(empty($this->types)) {
      $types = [];
      foreach($this->status as $id => $one) {
        $types[$id] = $one["type"];
      }
      $types = $this->stdin('Types', $types, true);
      foreach($this->status as $id => $one) {
        if(in_array($one["type"], $types)) {
          $this -> types[] = $id;
        }
      }
    }



    $array = [];

    $strings = [];


    while(true) {

      $cont = file($file);

      $logId = 0;

      foreach($cont as $x => $one) {

        //если эта строка уже обрабатывалась
        if(isset($strings[$x])) {
          continue;
        } else {
        //если строка не обрабатываласб - запоминаем и парсим ее
          $strings[$x] = true;
          $this -> parseString($one);
        }

      }

      //завершаем или повторяем цикл
      if(!$watch) {
        break;
      } else {
        usleep(500000);
      }

    }


  }


  private function parseString($str) {

    static $type; //тип ошибки

    //строка Zend-лога
    if(strpos($str, $this->date) === 0) {
      list($type, $str) = $this->parseZendLogString($str);
    }

    //ненужный PHP Stack trace
    elseif(strpos($str, "PHP Stack trace") !== false ||
    strpos($str, 'PHP   ') !== false ||
    strpos($str, 'Stack trace') !== false) {
      $type = 999;
    }
    //строка PHP-ошибки
    elseif(strpos($str, '[') === 0) {
      list($type, $str) = $this->parsePhpLogString($str);
    }


    //если тип подходит
    if(in_array($type, $this->types)) {
      echo $str;
    }



  }



  //распарсивает строку Zend Loggera
  private function parseZendLogString($str) {
    $str = explode(' ', $str);
    $type = (int) str_replace(['(',')', ':'], '', $str[2]);
    $time = substr($str[0], 11, 8);

    //оставляем только нужную информацию в строке
    unset($str[0]);
    unset($str[1]);
    unset($str[2]);

    $str = implode(" ", $str);

    //парсим название файла и номер строки
    $str = $this -> parseFileAndLine($str);

    $status = strtoupper($this->status[$type]['type']);
    $color  = $this->status[$type]["color"];
    $str = $this->color("$time $status ($type)", $color) . ": " . $str;

    return [$type, $str];

  }

  //парсит строку стандартного PHP error log
  private function parsePhpLogString($str) {

    $str = explode(' ', $str);

    if(!isset($str[5])) {
      $type = 5;
    } elseif( $str[4].' '.$str[5] == 'Parse error:') {
      $type = 0;
    } elseif( $str[4] == 'Warning:') {
      $type = 4;
    } elseif( $str[4] == 'Notice:') {
      $type = 5;
    } else {
      $type = 3;
    }

    //время
    $time = $str[1];

    //оставляем только нужную информацию в строке
    unset($str[0]);
    unset($str[1]);
    unset($str[2]);
    unset($str[3]);

    $str = implode(" ", $str);

    //парсим название файла и номер строки
    $str = $this -> parseFileAndLine($str);

    $status = strtoupper($this->status[$type]['type']);
    $color  = $this->status[$type]["color"];
    $str = $this->color("$time $status ($type)", $color) . ": " . $str;


    return [$type, $str];
  }


  //парсит путь к файлу и номер строки
  private function parseFileAndLine($str) {

    $str = str_replace("in isset", "in_isset", $str);

    $exp = explode(' in ', $str);

    //если пути к файлу нет - сразу возвращаем строку
    if(!isset($exp[1])) {
      return $str;
    }

    //смотрим есть ли номер строки
    $str = $exp[0];
    $path = $exp[1];
    $path = str_replace(DIR, '', $path);
    $exp = explode(' on line ', $path);

    // если номер строки пишется так: ':32'
    if(!isset($exp[1])) {
      if(strpos($exp[0], ':') !== false) {
        $exp = explode(':', $exp[0]);
      }
    }

    //если номера строки нет - сразу возвращаем строку
    if(!isset($exp[1])) {
      $path = $this->color($path, 'light_cyan');
      $str .= " in $path";
      return $str;
    }



    //добавляем номер строки в строку
    $path = $exp[0];
    $line = $exp[1];

    $path = $this->color($path, 'light_cyan');
    $line = $this->color($line, 'light_cyan');
    $str .= " in $path on line $line";

    return $str;

  }



}
