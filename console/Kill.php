<?php

namespace fw\console;

use fw\core\Console;
use fw\core\Log;

class Kill extends Console {


  public function index() {

    //получаем все порты
    $file = DIR.'/config/ports.json';
    if(!file_exists($file)) return false;

    $ports = json_decode(file_get_contents($file), true);

    foreach($ports as $port) {

      //смотрим процессы, запущенные на этом порту
      exec('lsof -i :'.$port, $pids);
      unset($pids[0]);
      foreach($pids as $pid) {

        $pid = array_values(
          array_diff(
            explode(" ", $pid),
          [''])
        );

        //останавливаем процесс
        if($pid[0] == 'php' || $pid[0] == 'php-cgi' || $pid[0] == 'httpd') {
          //exec("kill ".$pid[1]. " > /dev/null 2>&1");
          exec("kill -9 ".$pid[1]. " > /dev/null 2>&1");
          //Log::debug("kill ".$pid[1]. " > /dev/null 2>&1");
        }
      }

    }


    

  }


}
