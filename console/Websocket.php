<?php

namespace fw\console;

use fw\core\App;
use fw\core\Console;
use fw\core\Log;


class Websocket extends Console {


  public function index($path) {

    // 0-модуль, 1-контроллер, 2-экшен
    list($module, $controller, $action) = explode("/", $path);

    define("MOD", $module);

    App::run($module, $controller, $action);

  }


}
