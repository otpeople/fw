<?php

namespace fw\help;

use Yandex\Disk\DiskClient;
use fw\core\Date;
use fw\core\File;
use fw\core\Log;

class Dataload {


  protected $type; //тип yandex/fs/dropbox
  protected $token; //токен
  protected $disk;  //объект диска

  //ссылка на объект _File, когда он выполняет обработку
  public $file;

  public $array = [];



  //в конструктор передаем тип и токен
  //(если тип - файловая система, то вместо токена передаем путь к базовой директории)
  public function __construct($type, $token) {

    //проверка на формат
    if($type == 'yandex' || $type == 'fs') {
      $this->type = $type;
    } else {
      $this -> error('Неверный формат в конструкторе');
      return false;
    }

    //Проверка и подключение Яндекс Диска
    if($type == 'yandex') {

      //чтобы был указан токен
      if(!isset($token)) {
        $this->error("Не указан токен для Яндекс Диска");
        return false;
      }

      //пытаемся подключиться
      $this->disk = new DiskClient($token);


    }

    //Проверка файловой системы
    if($type == 'fs') {
      if(!file_exists($token)) {
        $this->error("Не найдена директория $token");
        return false;
      }

      $this->disk = $token;

    }



  }





  //сканирует нужную папку
  public function scan($dir) {

    if(!isset($this->type)) return false;

    //определяем название метода
    $method = 'scan'.ucfirst($this->type);
    if(method_exists($this, $method)) {
      return $this->$method($dir);
    }
  }


  //сканирует нужную папку в Яндкс Диске
  public function scanYandex($dir) {

    //массив с результатами
    $res = [];

    //подключаемся к Яндекс Диску
    $content = $this->disk->directoryContents($dir);

    //проходимся по каждому файлу
    foreach($content as $one) {
      $res[] = new _File([
        'href' => $one["href"],
        'date' => substr($one["creationDate"], 0, 10),
        'time' => substr($one["creationDate"], 11, 8),
        'name' => $one["displayName"],
        'type' => $one["resourceType"],
        'from' => &$this->type,
        'disk' => &$this->disk,
      ], $this);
    }

    //возвращаем массив со всеми файлами
    return $res;

  }

  //сканирует нужную папку в файловой системе
  public function scanFs($dir) {

    //массив с результатами
    $res = [];

    //определяем директорию для сканирования
    $dir = str_replace('//', '/', $this->disk.'/'.$dir);
    if(substr($dir, -1) !== '/') $dir = $dir.'/';


    if(!file_exists($dir)) return [];

    foreach(scandir($dir) as $one) {

      if($one{0} == '.') continue;

      $file = $dir.$one;


      $res[] = new _File([
        'href' => $file,
        'date' => date('Y-m-d', filemtime($file)),
        'time' => date('H:i:s', filemtime($file)),
        'name' => $one,
        'type' => (is_dir($file)) ? 'dir' : 'file',
        'from' => &$this->type,
        'disk' => &$this->disk,
      ], $this);

    }

    return $res;

  }






  //вывести в лог сообщение об ошибке
  public function error($msg) {

    Log::err("Загрузка данных: $msg");

  }





  //очистить временные файлы
  public function clear() {

    $dir = DIR.'/files/tmp';

    if(file_exists($dir)) {
      exec("cd $dir; rm -rf dataload");
    }

  }


  //получить все нужные файл
  public function get($key=null) {

    //если не передан ключ - возвращаем весь массив
    if(!isset($key)) return $GLOBALS["_dataload"];

    if(isset($GLOBALS["_dataload"][$key])) {
      return $GLOBALS["_dataload"][$key];
    } else {
      return [];
    }

  }


  //выполнить определенные действия со всем содержимым файлов
  public function make($key, $callback, $separator=false) {
    if(isset($GLOBALS["_dataload"][$key])) {
      foreach($GLOBALS["_dataload"][$key] as $file) {
        $file->make($callback, $separator);
      }
    } else {
      return false;
    }

  }


}


class _File {

  protected $dataload;

  public $href;
  public $date;
  public $time;
  public $name;
  public $type;
  public $tmp;

  public function __construct($array, $dataload) {

    foreach($array as $id => $val) {
      $this->$id = $val;
    }

    $this->dataload = $dataload;

  }


  //сохраняет содержимое файла во временный файл на сервере
  //сохраняет инфо файла в $GLOBALS['_dataload']
  public function save($array, $delete=false, $encoding='utf-8') {

    //сохраняем кодировку
    $this->encoding = $encoding;

    if(!isset($GLOBALS['_dataload'])) $GLOBALS['_dataload'] = [];

    if(!isset($GLOBALS['_dataload'][$array])) $GLOBALS['_dataload'][$array] = [];

    $GLOBALS['_dataload'][$array][] = $this;

    //копируем файл во временнею папку
    $dir = DIR.'/files/tmp/dataload/';
    File::mkdir($dir);
    $name = md5(uniqid());


      //если из файловой системы
      if($this->from == 'fs') {
        copy($this->href, $dir.$name);
      }

      //если из ЯндекДиска
      elseif($this->from == 'yandex') {
        $this->disk->downloadFile($this->href, $dir, $name);
      }

    $this->tmp = $dir.$name;

    //если нужно удалить файл - удаляем
    if($delete === true) $this->delete();

    return $this;
  }


  //удаляет файл из yandex/fs/dropbox
  public function delete($delete=true) {

    if($delete === false) return $this;

    //Если файл не сохранен - не удаляем
    if(!isset($this->tmp) || !file_exists($this->tmp)) return $this;

    //копируем временный файл в корзину
    $dir = DIR.'/files/trash/'.date('Y-m-d'); File::mkdir($dir);
    $name = $this->name.'_'.date('H-i-s');
    copy($this->tmp, $dir.'/'.$name);

    //удаляем настоящий файл

      //если из файловой системы
      if($this->from == 'fs') {
        unlink($this->href);
      }

      //если из ЯндекДиска
      elseif($this->from == 'yandex') {
        $this->disk->delete($this->href);
      }



    return $this;
  }


  //получить содержимое файла
  public function get() {

    return file($this->tmp);

  }



  //выполнить определенные действия с каждой строкой файла
  public function make($callback, $separator=null) {

    $this->dataload->file = $this;

    $content = $this->get();

    foreach($content as $str) {

      //если файл не в utf-8, переводим в utf-8
      if($this->encoding !== 'utf-8') {
        $str = iconv($this->encoding, 'utf-8', $str);
      }

      $str = trim($str);

      //пустую строку пропускаем
      if(empty($str)) continue;

      //если указан разделитель - делим строку на подстроки и передаем их как аргументы в колбек
      if(isset($separator)) {
        $str = explode($separator, $str);
      } else {
        $str = [$str];
      }

      if(sizeof($str) == 1) {
        continue;
      }


      if(is_array($callback)) {
        $object = $callback[0];
        $method = $callback[1];
        if(method_exists($object, $method)) {
          $object->$method(...$str);
        } else {
          $this->error("Не найден метод $method");
        }

      } else {
        $callback(...$str);
      }

      continue;



    }

    unset($content);

  }





}
