<?php

namespace fw\help;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

use fw\core\Log;
use fw\core\Access;
use fw\core\Db;

class Websocket implements MessageComponentInterface {

  private $ws; //модель
  private $access; //доступ из контроллера
  private $method; //метод контроллера
  private $moduleAccess; //данные для доступа к модулю


  public function __construct($ws, $access, $method) {

    $this -> ws = &$ws;
    $this -> access = $access;
    $this -> method = $method;

    //для проверки доступа модуля
    $file = DIR.'/'.MOD.'/access.php';
    if(file_exists($file)) {
      $this->moduleAccess = require($file);
    } else {
      $this->moduleAccess = null;
    }


    //тут будут храниться все подключения
    $this->ws->clients = new \SplObjectStorage;


    //тут будут храниться все id пользователей
    $this->ws->users = [];

    //получаем время изменения модели (для перезапуска если файл измениться)
    $this->time = $this->ws->_getTime();

  }



  //при пинге от клиента
  public function ping($conn) {


    //проверяем соединение с БД
    try {
      $conn->send('ping');
    } catch (\Exeption $e) {
      $this->ws->log("Ошибка соединения с БД"); exit;
      $conn -> close();
    }

  }


  //при открытии нового соединения
  public function onOpen(ConnectionInterface $conn) {


    //если это сам сервер подключился
    $host = $conn->WebSocket->request->getHost();
    if($host == '0.0.0.0') {
      return $this -> onPush($conn);
    }


    //проверяем на права доступа
    if(isset($this->moduleAccess)) {

      //получаем токен из cookie-файла
      $token = $conn->WebSocket->request->getCookie('access_'.MOD);

      //если cookie-файл отсутствует
      if(!isset($token)) {
        $this->ws->error("Попытка подключения пользователя - запрещено (нет токена)");
        $conn->notAccess = true;
        $conn->close();
      }

      //если нет доступа для модуля
      if(!Access::module($this->moduleAccess, $token)) {
        $this->ws->error("Попытка подключения пользователя - запрещено (нет доступа к модулю)");
        $conn->notAccess = true;
        $conn->close();
      }

      //проверяем на доступность контроллера
      if(!Access::to($this->access, $this->method, $token)) {
        $this->ws->error("Попытка подключения пользователя - запрещено (нет доступа к контроллеру)");
        $conn->notAccess = true;
        $conn->close();
      }

      //получаем id пользователя
      $conn->userId = Access::getUserId($token);

      //сохраняем id пользователя в users
      if(!in_array($conn->userId, $this->ws->users)) $this->ws->users[] = $conn->userId;

    }



    //сохраняем нового клиента
    $this->ws->clients -> attach($conn);

    //отсылаем клиенту его ID
    $this->ws->send($conn, '__userId', $conn->userId);

    //отслыаем клиенту все общие angular-переменные
    foreach($this->ws->angular as $angular) {
      if(!isset($this->conn)) {
        $angular->onGetData($conn);
      }
    }

    //вызываем метод open у модели
    if(method_exists($this->ws, 'open')) {
      $this->ws -> open($conn);
    }

  }


  //при получении сообщения от соединения
  public function onMessage(ConnectionInterface $conn, $msg) {

    if($msg == 'ping') {
      return $this->ping($conn);
    }

    list($type, $data) = json_decode($msg, true);


    //если запрос к ангулару
    if($type == 'angular') {


      $var1 = md5($data[0].$conn->resourceId);
      $var2 = md5($data[0]);

      $type = 'on'.ucfirst($data[1]);
      $data = $data[2];


      if(isset($this->ws->angular[$var1])) $var = $this->ws->angular[$var1];
      elseif(isset($this->ws->angular[$var2])) $var = $this->ws->angular[$var2];
      else $var = null;

      if(isset($var)) {
        if(method_exists($var, $type)) {
          $var -> $type($conn, ...$data);
        } else {
          $this->ws->error("попытка вызвать несуществующий angular-метод ($type)");
        }
      } else {
        $this->ws->error("нет такой angular-переменной (".$data[0].")");
      }



    } else {

      $method = 'on'.ucfirst($type);
      if(method_exists($this->ws, $method)) {
        $this->ws->$method($conn, ...$data);
      } else {
        $this->ws->error("попытка вызвать несуществующий метод $method");
      }

    }
  }


  //при закрытии соединения
  public function onClose(ConnectionInterface $conn) {

    //если это сервер - просто закрываем и все
    if(isset($conn->isServer) || isset($conn->notAccess)) {
      return false;
    }





    //удаляем все вотчеры для этого клиента
    foreach($this->ws->_watchers as $id => $watcher) {

      if(isset($watcher["conn"])) {
        if($watcher["conn"] == $conn) {
          unset($this->ws->_watchers[$id]);
        }
      }

    }


    //удаляем из списка подключенных клиентов
    $this->ws->clients->detach($conn);


    //Обновляем массив users
    $users = [];
    foreach($this->ws->clients as $client) {
      if($client != $conn && !in_array($client->userId, $users))
        $users[] = $client->userId;
    }
    $this->ws->users = $users;
    //unset($users);



    if(method_exists($this->ws, 'close')) {
      $this->ws -> close($conn);
    }


    //если подключений больше нет - завершаем
    if(empty($this->ws->users)) exit;
    /*

    //если это персональный WS и подключений больше нет - завершаем
    if($this->ws->personal && empty($users)) {
      exit;
    }

*/
  }

  //при ошибке
  public function onError(ConnectionInterface $conn, \Exception $e) {
    $this->ws->error($e->getMessage()); exit;
  }


  //при пуше с сервера
  private function onPush($conn) {

    //запоминаем, что это сервер
    $conn->isServer = true;

    //смотрим что нужно сделать
    $method =  str_replace('/', '', $conn->WebSocket->request->getPath());

    //если сервер просто проверяет держится ли соединения до сох пор
    if($method == 'ping') {

      //проверяем, чтобы файл был не изменен
      if($this->time != $this->ws->_getTime()) {
        $conn->send('');
        exit;
      }


      $conn -> send('OK');

    //если нужно остановить сервер
    } elseif($method == 'stop') {
      $conn -> send('OK');
      exit;

    //если нужно выполнить метод ангулара
    } elseif(strpos($method, 'angular=') === 0) {

      $var = str_replace('angular=', '', $method);
      if(isset($this->ws->angular[$var])) {
        $this->ws->angular[$var]->updateAllData();
      }

      $conn -> send('OK');

    //если это обычный пуш - вызываем нужный метод
    } else {

      if(method_exists($this->ws, $method)) {
        $this->ws -> $method();
      }


    }



    $conn -> close();

  }



}
