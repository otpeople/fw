<?php

namespace fw\help;

class Phone {

  public static $mobileOperators = [
    'biline'  => [903, 905, 906, 909, 951, 953, 960, 961, 962, 963, 964, 965, 966, 967, 968],
    'mts'     => [910, 911, 912, 913, 914, 915, 916, 917, 918, 980, 981, 982, 983, 984, 985, 987, 988, 989],
    'megafon' => [920, 921, 922, 923, 924, 925, 926, 927, 928, 929],
    'yota'    => [999],
  ];


  //переводит в формат мобильного телефона
  public static function mobile($phone,$start="8") {


    //выбираем номер если их несколько
    if(strpos($phone,',') !== false) {
      $phone = explode(',',$phone);
      if(strlen($phone[0]) >= strlen($phone[1])) {
        $phone = $phone[0];
      } else {
        $phone = $phone[1];
      }
    }
    if(strpos($phone,' ') !== false) {
      $phone = explode(' ',$phone);
      if(strlen($phone[0]) >= strlen($phone[1])) {
        $phone = $phone[0];
      } else {
        $phone = $phone[1];
      }
    }

      //удаляем все кроме цифр
    $phone = preg_replace('~[^0-9]+~','',$phone);

    //отсекаем номера у которых не 11 символов


    if(strlen($phone) == 10) {
      //если у номера 10 символов
      //если он просто без 8-ки вначале
      if(self::isNumberWithoutStart($phone)) {
        $phone = '8' . $phone;
      } else {
        return '';
      }
    } elseif(strlen($phone) == 11) {
      //если у номера 11 символов

    } else {
      return '';
    }


    $phone = substr_replace($phone, $start, 0, 1);


    return $phone;
  }

  //проверяет, что это мобильный номер без 8-ки вначале
  public static function isNumberWithoutStart($phone) {



    static $codes = [];

    if(empty($codes)) {

      foreach(self::$mobileOperators as $operator => $c) {
        foreach($c as $code) {
          $codes[$code] = $operator;
        }

      }
    }

    $start = (int) substr($phone, 0, 3);

    if(isset($codes[$start])) {
      return true;
    } else {
      return false;
    }


  }


}
