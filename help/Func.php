<?php

namespace fw\help;

class Func {

  //позвращает экземпляр модели
  public static function table($table, $module=null) {


    //если передана строка
    if(is_string($table)) {

      //если передана без namespace
      if(!strstr($table,"\\")) {

        //если не переда модуль - берем из константы
        if(!isset($module)) {
          $module = MOD;
        }

        $table = "\\".$module.'\\tables\\'.$table;

      }

      //если класс найден
      if(class_exists($table)) {
        $table = new $table();
        return $table;
      } else {
        return false;
      }

    } elseif(is_object($table)) {
      $table = clone $table;
    }

    return $table;


  }


}
