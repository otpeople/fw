<?php
namespace fw\core;

use fw\tables\ControllersAction;
use fw\tables\Controllers;
use fw\tables\Models;
use fw\tables\Logins;
use fw\core\File;
use fw\core\Log;



/**
 * Проверка доступа
 */
class Access {

  //const TOKEN = md5('access_'.MOD);
  const SESSION = 'access_'.MOD;


  //выполняет выход
  public static function logout($token=null) {

      //если не передан токен - значит завершаем сессию текущего пользователя
    if($token == null || ( isset($_COOKIE[self::SESSION]) && $_COOKIE[self::SESSION] == "token") ) {
      if(!isset($_COOKIE[self::SESSION])) { return false; }

      $token = $_COOKIE[self::SESSION];

      setcookie(self::SESSION,"0",time()-3600,'/');
    }

    //удаляем файл на сервере
    File::rmCache($token);

    //изменяем запись в БД
    $model = Logins::init() -> find("session",$token);
    if( $model->check() ) {
      $model -> update(["active" => 0]);
    }

  }


  //проверяет, существует ли сессия до сих пор
  public static function exist($token=null) {

    //если не передан токен - значит проверяем сессию текущего пользователя
    if($token == null) {
      if(!isset($_COOKIE[self::SESSION])) { return false; }
      $token = $_COOKIE[self::SESSION];
    }

    //если существует
    if(File::getCache($token)) {
      return true;
    } else {
      self::logout();
      return false;
    }

  }



  //обновляет время активности пользователя
  public static function active($token=null) {

      //если не передан токен - значит выполняем для текущего пользователя
    if($token == null) {
      if(!isset($_COOKIE[self::SESSION])) { return false; }

      $token = $_COOKIE[self::SESSION];

    }

      //обновляем время активности
    Logins::init() -> find("token",$token) -> update("active",date("Y-m-d H:i:s"));

  }


  //проверяет доступ к модулю
  public static function module($data, $token=null) {


    //если не передан токен - значит проверяем сессию текущего пользователя
    if($token == null) {
      if(!isset($_COOKIE[self::SESSION])) { return false; }
      $token = $_COOKIE[self::SESSION];
    }

    //если существует
    if(File::getCache($token)) {
      return true;

    //если не сущевтует
    } else {
      self::logout();
      return false;
    }

  }

  //получить id пользователя
  public static function getUserId($token=null) {

    if($token == null) {
      if(!isset($_COOKIE[self::SESSION])) { return false; }

      $token = $_COOKIE[self::SESSION];

    }

    $content = File::getCache($token);

    if($content) {
      return (int) $content['user'];
    } else {
      return false;
    }

  }

  public static function getGroups($user=null) {

    //если не указан пользователя - определяем автоматически
    if(!isset($user)) $user = self::getUserId();

    //


  }


  //проверяет, котрыть ли доступ к контроллеру
  public static function to($rule, $action, $token=null) {


    if(!isset($rule)) {
      return true;
    }

    if(isset($rule[$action])) {
      $rule = $rule[$action];
    } elseif(isset($rule['*'])) {
      $rule = $rule['*'];
    } else {
      return true;
    }

    if($rule == '*') {
      return true;
    }




    if(!isset($token)) {
      $token = $_COOKIE[self::SESSION];
    }


    $content = File::getCache($token);
    $user = $content["user"];


    //проверяем на доступ конкретному ползователю
    if(isset($rule["users"])) {
      if(!is_array($rule["users"])) {
        $rule["users"] = [$rule["users"]];
      }
      if(in_array($user, $rule["users"])) {
        return true;
      }
    }

    //проверяем доступ к группе
    if(isset($rule["groups"])) {

      if(!is_array($rule["groups"])) {
        $rule["groups"] = [$rule["groups"]];
      }

      $model  = '\\'.MOD.'\\tables\\Users';
      $model  = new $model();
      $groups = $model -> is('id',$user) -> one('groups');

      foreach($groups as $group) {
        if(in_array($group, $rule["groups"])) {
          return true;
        }
      }
    }

    return  false;


  }



}

?>
