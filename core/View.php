<?php
namespace fw\core;

use fw\core\App;
use fw\core\Assets;
use fw\core\File;
use fw\core\Cache;
use fw\core\Livereload;

class View {


  protected $code = 200;

  /**
   * Директория представления
   * @var string
   */
  protected $_dir;


  /**
   * Атрибуды для body
   * @var array
   */
  protected $body = [];


  /**
   * Атрибуды для html
   * @var array
   */
  protected $html = [];


  /**
   * Описание web-страница (тег <description> в <head>)
   * @var string
   */
  protected $description;


  /**
   * Заголовок html страницы
   * @var string
   */
  private $title;



  public $ngTemplate = [];


  /**
   * Иконка (favicon)
   * Относительный путь до иконки
   * @var string
   */
  private $icon;      //иконка (favicon)

  //подключает файл (так же добавляет его в отслеживаемые в livereload)

  /**
   * Подключает файл, и добавляет его в livereload
   * @method require
   * @param  [type] $file [description]
   * @return [type]       [description]
   */
  public function require($file) {
    if (!strpos($file, '.php')) {
      $file = $file.'.php';
    }
    $file = $this->_dir.'/'.$file;
    if(file_exists($file)) {
      if(LIVERELOAD) Livereload::addFile($file);
      require($file);
    } else {
      Log::err("Не найден файл $file");
    }
  }

  /**
   * Конструктор
   * @method __construct
   * @param  string      $view Название вида (файла)
   * @param  array       $vars Переменыне, которые будут использоваться в виде
   */
  function __construct(string $view, array $vars) {

    //html атрибуты по умолчанию
    $this->html['lang'] = 'ru';

    //если включен Livereload - добавляем текущий класс в отслеживаемые
    if(LIVERELOAD) Livereload::addClass(get_called_class());

    //определяем файл вида
    if(file_exists($view.'.php')) {
      $index = $view.'.php';
    } elseif(file_exists($view.'/index.php')) {
      $index = $view.'/index.php';
    } else {
      App::err(404);
    }

    //если включен Livereload - добавляем этот файл в отслеживаемые
    if(LIVERELOAD) Livereload::addFile($index);


    $this->_dir = dirname($index);

    //подключаем файл и сохраняем его содержимое в $content
    ob_start();
      extract($vars);
      require($index);
      $this->content = ob_get_contents();
    ob_end_clean();

    // TODO сделать чтобы PJAX работал
    // //если PJAX
    // if(isset($_SERVER['HTTP_X_PJAX'])) {
    //   Cache::$type = "json";
    //   header('Content-type: application/json');
    //   echo json_encode([
    //     "title" => $this -> title,
    //     "html"  => $this -> content,
    //   ]);
    //   return true;
    // }


    //TODO сделать чтобы работал $this->template
  }


  /**
   * Добавляет мета-теги в header
   * @method meta
   * @param  string|array $meta Мета-тег записанный в виде строки или масисва
   * @return @void
   */
  public function meta($meta) {
    Assets::addMeta($meta);
  }


  /**
   * Ресурсы (js|css), которые нужно доабить в документ
   * @method assets
   * @param  array $args название пакета с ресурсами или относительный путь
   * @return @void
   */
  public function assets(...$args) {
    foreach($args as $id => $arg) {
      //если это локальные файлы
      if(strpos($arg, "/")) {
        $args[$id] = $this->_dir."/".$arg;
        $args[$id] = str_replace('/./', '/', $args[$id]);
      }
    }

    Assets::add(...$args);

    // смотрим ngTemplates
    foreach ($args as $arg) {
      if(strpos($arg, "/") !== false) {
        $pattern = $arg.'**/*.ng.html';
        $templates = glob($pattern);
        foreach ($templates as $file) {
          $file = str_replace($this->_dir.'/', '', $file);
          $this->ngTemplate[$file] = $file;
        }
      }
    }



  }


  /**
   * Генерирует html-страницу
   * @method render
   * @return @void
   */
  public function render() {

    // устанавливаем код ответа
    App::$response->setStatusCode($this->code);

    //подключаем livereload assets
    if(LIVERELOAD) Assets::add(FW.'/assets/livereload');

    //выводим документ
    echo "<!doctype html>";
    echo '<html';
      foreach ($this->html as $key => $value) {
        echo ' '.$key.'="'.$value.'"';
      }
    echo '>';

      echo "<head>";

        //выводим иконку
        if(!empty($this->icon)) {
          $icon = str_replace("./", "", $this->_dir."/".$this->icon);
          if(file_exists($icon)) {
            echo '<link rel="icon" href="'.Assets::img($icon,[16,16]).'" type="image/x-icon">';
            echo '<link rel="apple-touch-icon" href="'.Assets::img($icon,[180,180]).'" >';
          }
        }

        //тег description
        if(!empty($this->description)) {
          echo '<meta name="description" content="'.$this->description.'">';
        }

        //выводим заголовок страницы
        echo '<title>'.$this->title.'</title>';

        //выводим дополнительные мата-теги
        echo Assets::metas();


        //выводим стили
        foreach(Assets::$files['css'] as $css) {
          $css = str_replace(DIR.'/public/', '/', $css);
          echo '<link rel="stylesheet" href="'.$css.'">'."\n";
        }


      echo "</head>";

      //<body>
      echo '<body';
      foreach ($this->body as $key => $value) {
        echo ' '.$key.'="'.$value.'"';
      }
      echo '>';

        //контент
        echo $this->content;

        //скрипты для Livereload
        if(LIVERELOAD) {
          echo "<script>var FwLiveReloadFiles = '".json_encode(Livereload::$files)."';</script>";
          echo "<script>var FwLiveReloadAssets = '".json_encode(Livereload::$assets)."';</script>";
        }

        //выводим все ngTemplates
        foreach($this->ngTemplate as $id => $file) {
          $files = [$file, $file.'.html', $file.'.php'];
          foreach($files as $f) {
            $f = $this->_dir.'/'.$f;
            if(file_exists($f)) {
              $file = $f;
              break;
            }
          }
          if(file_exists($file)) {
            echo '<script type="text/ng-template" id="'.$id.'">'."\n";
            require $file;
            echo '</script>';
          } else {
            $files = $this->_dir.'/'.$file;
            Log::err("Не найден файл $file");
          }
        }

        //выводим все JS скрипты
        foreach(Assets::$files['js'] as $js) {
          $js = str_replace(DIR.'/public/', '/', $js);
          echo '<script src="'.$js.'"></script>'."\n";
        }



      echo '</body>';
    echo "</html>";

  }

  // /**
  //  * Добавляет <script type="text/ng-template">
  //  *
  //  */
  // public function ngTemplate($file, $id) {
  //   if (!strpos($file, '.php')) {
  //     $file = $file.'.php';
  //   }
  //   $file = $this->_dir.'/'.$file;
  //   if(file_exists($file)) {
  //     if(LIVERELOAD) Livereload::addFile($file);
  //     $this->_ngTemplates[$id] = $file;
  //   } else {
  //     Log::err("Не найден файл $file");
  //   }
  // }


}
