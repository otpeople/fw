<?php

namespace fw\core;

use fw\core\Db;
use fw\core\Cache;
use fw\core\Form;
use fw\core\Angular;
use fw\core\Log;
use fw\core\Assets;
use fw\help\Func as F;

class Table {

  //конструкторы
  function __construct($id=null) {

    if(LIVERELOAD) Livereload::addClass(get_called_class());

    $this -> clear = $this -> sql;

    //запоминаем название текущего класса
    $this -> class = "\\".get_called_class();
    $this -> name  = explode("\\", $this -> class);
    $this -> name = $this->name[sizeof($this->name)-1];

    //запоминаем путь до текущего файла
    $this -> file = (new \ReflectionClass(get_called_class())) -> getFileName();

    //Подробно расписываем каждое поле
    $this -> _addFieldsChar();

    //директория для хранения файлов
    $dir = explode("\\", $this->class);
    $mod = $dir[1];
    $dir = $dir[sizeof($dir)-1];
    $dir = DIR.'/'.$mod."/files/tables/".$dir;
    $this->filesDir = $dir;
    unset($dir);

    //определяем имя таблицы
    if(!isset($this->table)) {

      $model = explode("\\", $this -> class);
      $model = array_values(array_diff($model, [""]));
      $table = array_pop($model);

      $lenght = strlen($table);

      for($i=0; $i < $lenght; $i++) {
        if($table{$i} == ucfirst($table{$i}) && $i != 0) {
          $this->table .= "_";
        }
        $this->table .=lcfirst($table{$i});
      }

      $this->table = strtoupper($model[0]) . '_' . $this->table;


    }

      //если был передан id - сохраняем его
    if(isset($id)) { $this -> id($id); }


  }
  public static function init($id=null) {
    $class = get_called_class();

    $class = new $class($id);

    return $class;
  }



  public $table;      //название текущей таблицы
  public $model;      //название текущей Модели
  public $class;      //абсолютное название класса
  public $name;      //название текущего класса (без namespace)
  public $file;      //абсолютное название файла
  public $fields;      //поля таблицы/модели
  public $register = false;//будет ли вестись регистр
  public $error = [];     //тут хранятся ошибки
  public $query;       //тут храниться последний запрос
  public $trigger = true;  //указывает, что при изменеияъ в модели нужно делать триггеры
  public $deleted = false; //Нужно ли искать так же среди удаленных
  public $syncFiles = true; // нужно ли синхронизировать файлы

  //составляющие запроса
  public $sql = [
    'id'    => [],
    'where' => [],    //условия запроса
    'fields'=> [],    //поля запроса
    'limit' => [],    //лимит
    'order' => [],    //сортировка
    'group' => [],    //группировка
    'index' => null,  //ключ результирующего массива
    'relations' => [],  //связи
    'array' => null,  //если нужно получть массив
    'check' => false,  //
    'one'   => null,  //получить только 1 массив в результате
    'cache' => ["time" => 0, "tables" => []],
    'types' => [    //поля, которые нужно будет распарсить согласно определенному типу
      'array' => [],
      'int'   => [],
      'files' => [],
      'json'  => [],
      'float' => [],
      'relation' => [],
      'password' => [],
      'bool' => [],
    ],
    'filters' => [],
  ];

  public $affected_rows;    //кол-во затронутых строк
  public $primary   = 'id';  //ключ таблицы
  public $separator = 'AND';  //разделитель

  protected $relations = [];  //связи с другими таблицами



  /**************************************
  * МЕТОДЫ ДЛЯ СБОРКИ WHERE В ЗАПРОСЕ ***
  **************************************/


  //собирает условие запроса
  public function where($x) {


      //если не массив - возращаем
    if(empty($x))     { return $this;}
    if(!is_array($x))   { return $this;}

    $where = &$this->sql["where"];
    $sql   = [];

    foreach($x as $field => $val) {

        //если такого поля нет
      if(!isset($this->fields[$field])) {

          //если есть символ +
        if(strpos($field, '+')) {
          $field = explode('+', $field);
          if(!isset($this->fields[$field[0]]) || !isset($this->fields[$field[1]])) {
            error_log("Field '$field' not exits in model ".$this->class);exit;
          } else {
            $field = 'ADDTIME(`'.$field[0].'`,`'.$field[1].'`)';
          }
        }
        else {
          error_log("Field '$field' not exits in model ".$this->class);exit;
        }

      }

      //если передана строка или число - преобразуем в массив
      if(is_string($val) || is_numeric($val)) { $val = [$val]; }


      foreach($val as $sign => $one) {
        if(is_numeric($sign)) { $sign = "="; }
        if(!is_array($one)) { $one = [$one];}

        if($sign == "between") {
          if(strpos($field, "ADDTIME")===false) {$field = "`".$field."`";}
          $sql[] = $field." ".$sign." '".$one[0]."' AND '".$one[1]."' ";
        } else {

          foreach($one as $o) {

            //if(is_array($o)) { error_log(print_r($x,true)); }


              //дата+время
            if(strpos($field, 'ADDTIME')!==false) {
              $sql[] = $field." ".$sign." '".$o."'";
            }
              //пароль
            elseif($this->fields[$field]["type"] == "password") {
              $sql[] = "`".$field."` ".$sign."PASSWORD('".md5($o).$this->getSecret()."')";
            }
              //зашифрованное поле
            elseif($this -> fields[$field]["encrypt"] == true) {
              $sql[] = "AES_DECRYPT(`".$this->table."`.`".$field."`, '".$this->getSecret()."') ".$sign." '".$o."' ";   // "AES_DECRYPT(`".$this->table."`.`".$field."`, '".$this->getSecret()."') ".$sign." '".$o."' ";
            }
              //теги
            elseif($this -> fields[$field]["type"] == "tags" && $sign == "=") {
              if(!empty($o)) {
                $sql[] = "`".$field."` LIKE '%#".$o."' OR `".$field."` LIKE '%#".$o.",%'";
              } else {
                $sql[] = "`".$field."` != '' ";
              }
            }
            elseif($this -> fields[$field]["type"] == "tags" && $sign == "!=") {
              if(!empty($o)) {
                $sql[] = "`".$field."` NOT LIKE '%#".$o."' AND `".$field."` NOT LIKE '%#".$o.",%'";
              } else {
                $sql[] = "`".$field."` != '' ";
              }
            }
              //многие связи
            elseif(isset($this->fields[$field]["relation"]) && ($this->fields[$field]["size"][1] > 1) && $sign == "=") {
              if(!empty($o)) {
                $sql[] = "`".$field."` LIKE '%#".$o."' OR `".$field."` LIKE '%#".$o.",%'";
              } else {
                $sql[] = "`".$field."` != ''";
              }
            }
            elseif(isset($this->fields[$field]["relation"]) && ($this->fields[$field]["size"][1] > 1) && $sign == "!=") {

              if(!empty($o)) {
                //Log::debug("`".$field."` NOT LIKE '%#".$o."' AND `".$field."` NOT LIKE '%#".$o.",%'");
                $sql[] = "`".$field."` NOT LIKE '%#".$o."' AND `".$field."` NOT LIKE '%#".$o.",%'";
              } else {
                $sql[] = "`".$field."` != '' ";
              }
            }

            //обычное неравно
            elseif($sign == '!=') {
              if($o === null) {
                $sql[] = "`".$field."` IS NOT NULL";
              } else {
                $sql[] = "`".$field."` ".$sign." '".$o."' OR `".$field."` IS NULL";
              }
            }


              //обычное поле
            else {
              $sql[] = "`".$field."` ".$sign." '".$o."'";
            }

          }
        }
      }

    }
    if(!empty($sql)) {
      $where[] = $sql;
    }


    return $this;


  }


  public function _find($a) {

      //получаем все нужные значения в виде массива
    if(!is_array($a[2])) { $a[2] = [$a[2]]; }
    if(isset($a[3])) {
      for($y=3;$y<sizeof($a);$y++) {
        if(is_array($a[$y])) {
          $a[2] = array_merge($a[2], $a[$y]);
        } else {
          $a[2][] = $a[$y];
        }
      }
    }

      //если такого field нет в массиве
    if(!isset($where[$a[0]])) { $where[$a[0]] = [];}

      //добавляем условие

    $this->_find_where[$a[0]][$a[1]] = $a[2];
  }

  public $_find_where = [];

  //легкие условия
  public function find() {

    $this -> _find_where = [];
    $i = 0;
    $args = func_get_args();

    foreach($args as $a) {

        //если передаются массивы
      if(is_array($a) && $i==0) {  $this->_find($a); }
      else {$i++;}

    }
      //если передано одной строкой
    if($i != 0) {
      if(!isset($args[2])) { $args[2] = $args[1]; $args[1] = '='; }
      $this -> _find($args);
    }

    $this -> where($this->_find_where);
    return $this;
  }

  public function is() {
    $args = func_get_args();
    $field = $args[0];
    unset($args[0]);
    $aa = [];
    foreach($args as $a) {
      if(is_array($a)) { $aa = array_merge($aa, $a); }
      else { $aa[] = $a; }
    }
    $this -> find([$field, '=', $aa]);
    return $this;
  }

  public function id() {
    $x = func_get_args();

    if(sizeof($x) == 1) { $this->sql["id"] = $x[0]; }

    $this -> where(["id" => $x]);
    return $this;
  }

  public function not() {
    $x = func_get_args();
    $field = $x[0];
    array_shift($x);
    $this -> where([$field => ["!=" => $x]]);
    return $this;
  }

  public function like($x,$y) {
    $x = func_get_args();
    $field = $x[0];
    array_shift($x);
    $this -> where([$field => ["LIKE" => $x]]);
    return $this;
  }

  public function notlike($x,$y) {
    $x = func_get_args();
    $field = $x[0];
    array_shift($x);
    $this -> where([$field => ["NOT LIKE" => $x]]);
    return $this;
  }

  public function between($field, $d1, $d2) {
    $this -> where([$field => ["between" => [$d1,$d2] ] ]);
    return $this;
  }




  /**************************************
  * МЕТОДЫ ДЛЯ ОСТАЛЬНОЙ СБОРКИ ЗАПРОСА *
  **************************************/


  //указывает индекс при составлении массива
  public function index($index) {
    $this -> sql["index"] = $index;
    return $this;
  }

  //делает лимит запроса
  public function limit($x,$y=null) {

    if(!isset($y)) { $y = $x; $x = 0; }
    $x = (int) $x;
    $y = (int) $y;

    $this -> sql["limit"] = [$x,$y];
    return $this;
  }

  //выполняет группировку запроса
  public function group(...$args) {

    foreach($args as $arg) {
      if(!in_array($arg, $this->sql['group'])) $this->sql["group"][] = $arg;
    }
    return $this;
  }

  //выполняет соритровку запроса
  public function order(...$args) {

    foreach($args as $one) {

      //в какую сторону сортировать
      if(strpos(strtoupper($one), "DESC") !== false) {
        $type = "DESC";
        $one = str_ireplace("desc", "", $one);
      } else {
        $type = "ASC";
        $one = str_ireplace("asc", "", $one);
      }
      $one = trim($one);
      $one = explode(".", $one);

      //если нет точки в названии
      if(sizeof($one) == 1) {
        $one = "`".$this->table."`.`".$one[0]."`";
      }

      //если есть точка в названии
      else {
        $one = strtoupper($one[1])."(`".$this->table."`.`".$one[0]."`)";
      }

      //канкатинируем поле и тип сортировки
      $one = $one.' '.$type;

      //сохраняем
      $this->sql["order"][] = $one;

    }
    return $this;
  }


  //добавляет тип поля, для того чтобы потом его корректно распарсить
  protected function _addFieldType($field, $type) {
    if(!in_array($field, $this->sql["types"][$type])) {
      $this->sql["types"][$type][] = $field;
    }
  }

  //собирает поля запроса
  public function fields() {

    $array = func_get_args();

    foreach($array as $one) {

      if(!is_array($one)) { $one = [$one]; }
      foreach($one as $field) {
        if(empty($field)) {continue;}

        if(strpos($field, '|')) {
          $field = explode('|', $field);
          $filter = trim($field[1]);
          $field = trim($field[0]);
          $this->sql['filters'][$field] = $filter;
        }

        $field = explode(".", $field);

        //если поле не найдено
        if($field[0] == '*') continue;

        if(!isset($this->fields[$field[0]])) {
          $this->_error("Field '".$field[0]."' not found");
        }

        //если поле содержит связи
        elseif(isset($this->fields[$field[0]]["relation"])) {

          $f = $field[0];

          //если есть поля из связанной таблицы
          if(isset($field[1])) {

            // добавляем поле в список связей
            if(!in_array($f, $this->sql["relations"])) $this->sql["relations"][] = $f;

            // убираем название поля в этой таблице
            array_shift($field);

            $field = implode(".", $field);
            if(!isset($this->sql["fields"][$f])) {
              $this->sql["fields"][$f] = [];
            }
            if(!empty($field)) {
              $this->sql["fields"][$f][$field] = $field;
            }

          }

          //если не указаны поля из связанной таблицы
          else {

            //echo "{$field[0]} {$this->fields[$field[0]]["size"][1]}\n";

            //распарсим поля как тип array (если связь ко многим)
            if($this->fields[$field[0]]["size"][1] != 1) {
              $this->_addFieldType($field[0], 'array');
            }
            $this->sql["fields"][$f] = $f;
          }

        }

        //если не удается найти связи
        elseif(isset($field[1])) {

          if($field[1] == "sum") {
            $this->sql["fields"][$field[0].".".$field[1]] = [];
          }

          elseif($field[1] == "max") {
            $this->sql["fields"][$field[0].".".$field[1]] = [];
          }

          elseif($field[1] == "min") {
            $this->sql["fields"][$field[0].".".$field[1]] = [];
          }

          else {
            $this->_error("Field '".$field[0]."' don't have relation");
          }
        }

        //если это файл
        elseif($this->fields[$field[0]]["type"] == "files") {
          $this->_addFieldType($field[0], 'files');
          $this->sql["fields"][$field[0]] = [];
        }

        //если это теги
        elseif($this -> fields[$field[0]]["type"] == "array") {
          $this->_addFieldType($field[0], 'array');
          $this->sql["fields"][$field[0]] = [];
        }

        //если это число
        elseif($this->fields[$field[0]]["type"] == "int") {
          $this->_addFieldType($field[0], 'int');
          $this->sql["fields"][$field[0]] = [];
        }

        //если это bool
        elseif($this->fields[$field[0]]["type"] == "bool") {
          $this->_addFieldType($field[0], 'bool');
          $this->sql["fields"][$field[0]] = [];
        }

        //если это json
        elseif($this->fields[$field[0]]["type"] == "json") {
          $this->_addFieldType($field[0], 'json');
          $this->sql["fields"][$field[0]] = [];
        }

        //если это password
        elseif($this->fields[$field[0]]["type"] == "password") {
          $this->_addFieldType($field[0], 'password');
          $this->sql["fields"][$field[0]] = [];
        }

        //если это обычное поле
        else {
          $this->sql["fields"][$field[0]] = [];
        }

      }
    }

    return $this;
  }




  /******************************
  * МЕТОДЫ ДЛЯ ПОЛУЧЕНИЕ ДАННЫХ *
  ******************************/


  //получить результат
  public function get(...$args) {


    //если что-то передано
    if(sizeof($args) != 0) {

      //если передан лимит
      if(is_numeric($args[0])) {
        if(isset($args[1])) {
          $this -> limit($args[0],$args[1]);
        } else {
          $this -> limit($args[0]);
        }
      }

      //если переданы поля
      else {
        foreach($args as $row) {
          $this -> fields($row);
        }
      }

    }

    //очищаем связи
    $this->relations = [];

    return $this -> ifcache();
  }

  //получить результат с лимитом 1
  public function one(...$args) {

    //сохраняем то что нам нужно получить только один результат
    $this -> sql["one"] = true;

    //если передан 1 аргумент(поле) - значит в конце нам нужно будет получит только это поле
    if(sizeof($args) === 1)  {
      $field = $args[0];
      $field = str_replace([".sum", ".max", ".min"], "", $field);
      $this->sql["oneField"] = $field;
    }


    $this -> limit(1);
    $this -> fields($args);
    return $this -> ifcache();
  }

  //проверка на существование
  public function check() {

    $this -> limit(1);
    $temp = $this -> ifcache();

    if(empty($temp)) {
      return false;
    } else {
      return true;
    }
  }

  //получить в виде ассоциативного массива
  public function arr($id,$val=null) {

      //группируем по id
    //$this->group($id);

      //если ассоциативный массив
    if(isset($val)) {
      $arrval = $val;
      $arrid  = $id;
      if(strpos($arrval, ".")) { $arrval = explode(".", $arrval); $arrval = $arrval[0]; }
      if(strpos($arrid,  ".")) { $arrid  = explode(".", $arrid ); $arrid  = $arrid[0];  }
      $this->sql["array"] = [$arrid,$arrval];
      return $this -> fields($id,$val) -> get();
    }

    //если обычный массив
    $this->sql["array"] = $id;

    return $this -> fields($id) -> get();
  }






  //вывести строку запроса в консоль
  public function log($error=false) {
    $msg = $this->class.":\n".$this->query;
    if($error) Log::err($msg);
    else Log::debug($msg);
    return $this;
  }








  //  СИСТЕМНЫЕ

  //собирает запрос
  protected function build() {


    $q = &$this->query;

    $q = ["SELECT"];


    //получаем поля
    if(empty($this->sql["fields"])) $this -> getFields();
    $fields = $this->sql["fields"];


    $f = [];
    foreach($fields as $field => $arr) {

      //если это шифрованное поле
      if(isset($this->fields[$field]) &&$this->fields[$field]["encrypt"] === true) {
        $f[] = "\n\tAES_DECRYPT(`".$this->table."`.`".$field."`, '".$this->getSecret()."') as `".$field."`";
      }

      //если это обычное поле
      else {

        $fi = explode(".", $field);

        //если нужно совершить какие-то действия с полем
        if(sizeof($fi) == 1) $f[] = "\n\t`".$this->table."`.`".$field."`";
        else $f[] = "\n\t".strtoupper($fi[1])."(`".$this->table."`.`".$fi[0]."`) as `".$fi[0]."`";

      }
    }

    //добавляем все поля
    $q[] = implode(",", $f);
    unset($f);

    //индекс
    if(isset($this->sql["index"])) {
      $q[] = ",`".$this->table."`.`".$this->sql["index"]."` as `index_for_request`";
    }

    //указываем таблицу
    $q[] = "\nFROM `".$this->table."`";


    //собираем where
    $where = $this->sql["where"];
    if($this->deleted) $q[] = "\nWHERE TRUE ";
    else $q[] = "\nWHERE\n\t`".$this->table."`.`_delete` IS NULL ";

    if(sizeof($where) != 0) { $q[] = "\nAND ("; }
    $w = [];

    foreach($where as $one) {
      $w[] = "\n\t(".implode(" OR ", $one).")";
    }

    $q[] = implode(" AND ", $w);

    if(sizeof($where) != 0) { $q[] = "\n)"; }



    //собираем группировку
    if(sizeof($this->sql["group"]) != 0) {
      $q[] = "\nGROUP BY"; $g = [];
      foreach($this->sql["group"] as $one) {
        $g[] = "\n\t`".$this->table."`.`".$one."`";
      }
      $this->sql["group"];
      $q[] = implode(",",$g);
      unset($g);
    }

    //собираем сортировку
    if(sizeof($this->sql["order"]) != 0) {

      $q[] = "\nORDER BY"; $o = [];

      foreach($this->sql["order"] as $one) {
        $o[] = "\n\t".$one;
      }
      $q[] = implode(",",$o);
      unset($o);
    }



    //собираем лимит
    if(!empty($this->sql["limit"])) {
      $q[] = "\nLIMIT ".$this->sql["limit"][0].", ".$this->sql["limit"][1];
    }

    //преобразем запрос в строку
    $q = implode(" ", $q);

    //сохраняем запрос




    try {
      return $this->tryQuery($q);
    } catch (Exception $e) {
      $this->log(true);
    }


  }

  //пробует выполнить запрос
  protected function tryQuery($q) {

    $rs = Db::query($q);

    $array = [];

    if(empty($rs)) {
      $this->log(true);
    } else {
      return $this -> parse($rs);
    }

  }

  //обрабатывает результат запроса
  protected function parse($rs) {



    if(empty($rs->num_rows)) {
      return [];
    }

    //индекс
    $index = $this->sql["index"];

    //массив с результатом
    $array = [];


    //если нужно что-то расшифровать
    $encrypt = [];
    foreach($this->fields as $field => $param) {
      if(isset($param["encrypt"]) == true) {
        $encrypt[] = $field;
      }
    }


    //получем массив
    if(empty($index)) {
      while($row = mysqli_fetch_assoc($rs)) {
        $array[] = $row;
      }
    } else {
      while($row = mysqli_fetch_assoc($rs)) {
        $i = $row["index_for_request"];
        unset($row["index_for_request"]);
        $array[$i] = $row;
      }
    }



    foreach($array as $id => $val) {

      //если есть интовые поля
      if(isset($this->sql["types"]['int'][0])) {
        foreach($this->sql["types"]['int'] as $field) {
          $array[$id][$field] = (int) $val[$field];
        }
      }

      //если есть булевы
      if(isset($this->sql["types"]['bool'][0])) {
        foreach($this->sql["types"]['bool'] as $field) {
          $array[$id][$field] = (bool) $val[$field];
        }
      }

      //если есть json-поля
      if(isset($this->sql["types"]['json'][0])) {
        foreach($this->sql["types"]['json'] as $field) {
          $array[$id][$field] = json_decode($val[$field], true);
        }
      }

      //если есть password-поля
      if(isset($this->sql["types"]['password'][0])) {
        foreach($this->sql["types"]['password'] as $field) {
          $array[$id][$field] = '';
        }
      }

      //если есть связи
      if(isset($this->sql["relations"][0])) {
        foreach($this->sql["relations"] as $relation) {
          $array[$id][$relation] = $this->relation($relation, $val[$relation]);

          if(!isset($array[$id]['_relation'])) {
            $array[$id]['_relation'] = [];
          }
          $array[$id]['_relation'][$relation] = $val[$relation];
        }
      }

      //если есть поля-массивы
      if(isset($this->sql["types"]['array'][0])) {
        foreach($this->sql["types"]['array'] as $arr) {

          $temp = explode(",", $val[$arr]);
          foreach($temp as $tempid => $tempval) {
            $temp[$tempid] = substr($tempval, 1);
            if(empty($tempval)) { unset($temp[$tempid]);}
          }
          $array[$id][$arr] = $temp;

        }
      }

      //если есть поля-файлы
      if(isset($this->sql["types"]['files'][0])) {


        $dir = $this->filesDir;


        //если файлы
        foreach($this->sql["types"]['files'] as $file) {

          //для каждого поля с файлами
          $val[$file] = explode("\n", $val[$file]);
          $array[$id][$file] = [];
          foreach($val[$file] as $v) {
            if(!empty($v)) {
              $array[$id][$file][] = $dir."/".$v;
            }
          }
          if($this->fields[$file]["size"][1] == 1) {
            if(empty($array[$id][$file])) {
              $array[$id][$file] = '';
            } else {
              $array[$id][$file] = $array[$id][$file][0];
            }
          }

          //
          // //если есть фильтры для файла
          // if(isset($this->sql['filters'][$file])) {
          //   $filter = $this->sql['filters'][$file];
          //   //Log::debug($filter);
          //   if(is_array($array[$id][$file])) {
          //
          //     foreach ($array[$id][$file] as $id => $val) {
          //       $array[$id][$file][$id] = Assets::img($val, $filter);
          //     }
          //   } else {
          //     $array[$id][$file] = Assets::img($array[$id][$file], $filter);
          //   }
          // }




        }

      }

    }


    // если есть фильтры
    if(!empty($this->sql['filters'])) {
      foreach ($this->sql['filters'] as $field => $filter) {

        // если есть ограничения
        $filter = explode("!", $filter);
        if(isset($filter[1])) {
          $max = $filter[1];
        } else {
          $max = null;
        }

        $filter = $filter[0];

        $field = explode('.', $field);

        foreach ($array as $id => $val) {

          if(sizeof($field) == 1) { $ff = &$array[$id][$field[0]]; }
          elseif(sizeof($field) == 2) { $ff = &$array[$id][$field[0]][$field[1]]; }
          elseif(sizeof($field) == 3) { $ff = &$array[$id][$field[0]][$field[1]][$field[2]]; }

          // если файлов нет
          if(empty($ff)) {
            continue;
          }

          // если несколько фоток массивом
          if(is_array($ff)) {
            $x = 0;
            foreach ($ff as $keyff => $valueff) {
              // если есть ограничения
              $x++;
              if(isset($max) && $x > $max) {
                unset($ff[$keyff]);
              } else {
                $ff[$keyff] = Assets::img($valueff, $filter);
              }
            }
          } else {
            $ff = Assets::img($ff, $filter);
          }

        }
      }
    }



    //если лимит = 1

    //если нужно получить всего 1 значение
    if(isset($this->sql["one"])) {

      $array = $array[0];

      //если нужно получить всего одно поле
      if(isset($this->sql["oneField"])) {
        $array = $array[$this->sql["oneField"]];
      }

    }


    //если нужно поулчить массив (key => value)
    if(isset($this->sql["array"])) {

      $arr2 = [];

      if(is_array($this -> sql["array"])) {
        foreach($array as $a) {
          $x = $a[$this->sql["array"][0]];
          $y = $a[$this->sql["array"][1]];
          $arr2[$x] = $y;
        }
      } else {
        foreach($array as $a) {
          $arr2[] = $a[$this->sql["array"]];
        }
      }

      return $arr2;
    }

    return $array;
  }



  //создает связи
  public function relation($field, $val) {
    if(empty($val)) { return [];}
    if(!isset($this->relations[$field])) {

      //получаем значение из fields
      $model = $this->fields[$field]["relation"]["table"];

      //получаем объект
      $model = F::table($model, explode('\\', $this->class)[1]);

      $result = $model
        -> index($this->fields[$field]["relation"]["index"])
        -> fields($this->sql["fields"][$field])
        -> get();

      $this->relations[$field] = $result;
    }


    $max = 1;
    if(isset($this->fields[$field]["size"])) {
      if(!is_array($this->fields[$field]["size"])) {
        $max = $this->fields[$field]["size"];
      } else {
        $max = $this->fields[$field]["size"][1];
      }
    }



    if($max == 1) {
      if(isset($this->relations[$field][$val])) {
        return $this->relations[$field][$val];
      } else {
        return [];
      }
    } else {
      $val = explode(",", $val);
      $arr = [];
      foreach($val as $v) {
        $v = substr($v, 1);
        $v = (int) $v;
        if(isset($this->relations[$field][$v])) {
          $arr[] = $this->relations[$field][$v];
        }
      }
      return $arr;
    }


  }

  //получаем все поля таблицы
  public function getFields() {
    $fields = [];
    foreach($this->fields as $field => $param) {  $fields[] = $field; }
    $this -> fields($fields);
  }

  //проверка на кеширование
  public function ifcache() {

    if($this->sql["cache"]["time"] == 0) {
      return $this->build();
    } else {

      $table = &$this;

      return Cache::get(function() use(&$table) {
        return $table->build();
      },[
        'time' => $this->sql["cache"]["time"],
        'tables' => $this->sql["cache"]["tables"],
      ]);

    }




  }

  //триггер, срабатывающий при изменениях
  public function trigger($type=null) {

    if($this -> trigger === false) { return false; }

    //удаляем кеш
    Cache::changeTable($this->class);
    /*

    //сохраняем историю
    if(isset($type)) {
      $fields = [];
      foreach($this->_history as $history) {
        $fields[$history["field"]] = [
          'before' => $history["before"],
          'after' => $history["after"],
        ];
      }
      $time = date('Y-m-d'),
      $file = DIR.'/'.MOD.'/files/history/';
      //File::write()
    }

*/
  }

  //задает время кеширование запроса
  public function cache() {

    $args = func_get_args();
    foreach($args as $arg) {
      if(is_numeric($arg)) {
        $this -> sql["cache"]["time"] = $arg;
      } else {
        $this -> sql["cache"]["models"] = $arg;
      }
    }
    return $this;
  }

  //получает секретный ключ
  protected function getSecret() {
    if(empty($this->secretKey)) {
      if(file_exists(DIR."/config/kеy.php")) {
        $key = require(DIR."/config/kеy.php");
      } else {
        $key = "Base mrKey 005";
      }
      $this->secretKey = md5($key);
    }
    return $this->secretKey;
  }
  protected $secretKey;

    //регулярные выражения для валидации
  public $validators = [

    "email"   => [  //электронная почта
      'message' => "Email введен некорректно",
      'pattern' => "^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$",
    ],
    "engone"   => [  //английские буквы и цифры (одно слово)
      'message' => "Допустимы только английские буквы и цифры (без пробелов)",
      'pattern' => "^[a-zA-Z0-9]+$",
    ],
    "ruone"   => [  //русские буквы и цифры (одно слово)
      'message' => "Допустимы только русские буквы и цифры (без пробелов)",
      'pattern' => "^[а-яА-ЯёЁ0-9]+$",
    ],
    "engruone" => [//русские и английские буквы (одно слово)
      'message' => "Допустимы только русские, английские буквы и цифры (без пробелов)",
      'pattern' => "^[а-яА-ЯёЁa-zA-Z0-9]+$",
    ],
    "engru"  => [  //русские и английские символы
      'message' => "Допустимы только русские, английские буквы и цифры (без пробелов)",
      'pattern' => "^[а-яА-ЯёЁa-zA-Z0-9]+$",
    ],
    "pass"  => [  //пароль
      'message' => "Нужна хотя бы 1 латинская буква, хотя бы 1 цифра, минимум 7 символов",
      //'pattern' => "^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$",
      'pattern' => "^(?=.*\d)(?=.*[a-zA-Z])(?!.*\s)(?=^.{7,}$).*$",
    ],
    "date2"  => [  //дата в формате DD/MM/YYYY
      'message' => "Допустима только дата в формате DD/MM/YYYY",
      'pattern' => "[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])",
    ],
    "date"  => [  //lата в формате YYYY-MM-DD
      'message' => "Допустима только дата в формате YYYY-MM-DD",
      'pattern' => "(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d",
    ],
    "int"  => [  //целые числа
      'message' => "Допустимы только целые числа",
      'pattern' => "^\-?[0-9]+$",
    ],
    "float"  => [  //целые и дробные числа
      'message' => "Допустимы только целые и дробные",
      'pattern' => "\-?\d+(\.\d{0,})?",
    ]

  ];



  //валидация (если не прошла - возвращает массив с ошибками) (если прошла - возвращает value)
  public function validate($row, $val, $dupUpdate=false) {

    $error = [];

    $min = $this -> fields[$row]["size"][0];
    $max = $this -> fields[$row]["size"][1];


      //ВАЛИДАЦИЯ на КОЛИЧЕСТВО


    //если есть связи
    if(isset($this->fields[$row]["relation"]) || $this->fields[$row]["type"] == "tags") {
      if(!is_array($val)) { $val = [$val]; }
      if(sizeof($val) > $max || sizeof($val) < $min) {
        if($min == $max) {
          $this -> error[] = ["field" => $row, "message" => "Нужно выбрать ".$min, "value" => $val];
        } else {
          $this -> error[] = ["field" => $row, "message" => "Нужно выбрать от ".$min." до ".$max, "value" => $val];
        }
      }

      if(isset($this->fields[$row]["relation"]) && $this->fields[$row]["size"][1] == 1) {
        $val = $val[0];
        if($val === '0' || $val === 0) $val = null;
      }

    }

    elseif($this->fields[$row]['type'] == 'json') {
      return "'".json_encode($val, JSON_UNESCAPED_UNICODE)."'";
    }


    //если это файлы
    elseif($this->fields[$row]["type"] == "files") {

      if(is_array($val)) {
        $files = $val;
      } else {
        $files = explode(",", $val);
      }

      if(sizeof($files) > $max || sizeof($files) < $min) {
        if($min == $max) {
          $this -> error[] = ["field" => $row, "message" => "Необходимо выбрать только ".$min." файл", "value" => $val];
        } else {
          $this -> error[] = ["field" => $row, "message" => "Необходимо от ".$min." до ".$max." файлов", "value" => $val];
        }

      }

      //разрешенные расширения файлов
      if(isset($this->fields[$row]["ext"])) {
        $exts = $this->fields[$row]["ext"];
        if(!is_array($exts)) { $exts = [$exts]; }
      } else {
        $exts = [];
      }

      $_i = 0;

      $files = array_diff($files, [""]);

      foreach($files as $file) {
        $_i++;

        //проверяем на расширения
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        if(in_array($ext, $exts) || empty($exts)) {}
        else {
          $this -> error[] = ["field" => $row, "message" => "Недопустимый тип файла №".$_i];
        }

        $dir = $this->filesDir;



        if(!file_exists($dir."/".$file)) {
          $this -> error[] = ["field" => $row, "message" => "Что-то пошло не так..."];
        }

      }

      $val = implode("\n", $files);

    }

    //валидация на минимум/максимум
    elseif($max != 0) {

      if(strlen($val) > $max || strlen($val) < $min) {
        if($min == $max) {
          $this -> error[] = ["field" => $row, "message" => "Необходимо ".$min." символ(ов)", "value" => $val];
        } else {
          $this -> error[] = ["field" => $row, "message" => "Необходимо от ".$min." до ".$max." символов", "value" => $val];
        }

      }
    }

    //в float заменяем запятую на точку
    if($this -> fields[$row]["type"] == "float") {
      $val = str_replace(",", ".", $val);
    }




    //валидация на регулярные выражения
    if(!empty($val)) {

      $field = $this->fields[$row];

      foreach($field["regexp"] as $reg) {
        $pattern = '/'.$this->validators[$reg]["pattern"].'/';
        if(is_array($val)) {
          foreach($val as $v) {
            if(!preg_match($pattern, $v)) {
              $this -> error[] = ["field" => $row, "message" => $this->validators[$reg]["message"], "value" => $val];
            }
          }
        } else {
          if(!preg_match($pattern, $val)) {
            $this -> error[] = ["field" => $row, "message" => $this->validators[$reg]["message"], "value" => $val];
          }
        }

      }
    } elseif($min != 0 && $max != 0) {
      $this -> error[] = ["field" => $row, "message" => "Поле не может быть пустым"];
    }


    //проверка на уникальность
    if($dupUpdate === false) {

      if($this->fields[$row]["unique"] === true) {
        if(!empty($val)) {
          $model = $this -> class;
          if((new $model()) -> find($row, $val) -> check()) {
            $this -> error[] = ["field" => $row, "message" => "Такое значение уже существует"];
          }
        }
      }

    }


    //если есть связи (ко многим) или это массив
    if( isset($this->fields[$row]["relation"])  || $this->fields[$row]["type"] == "array") {

      if(is_array($val)) {

        foreach($val as $ID => $VAL) {
          if(empty($VAL)) { unset($val[$ID]); }
          else {$val[$ID] = "#".$VAL; }
        }
        $val = implode(",", array_values(array_unique($val)));

      }

    }

    //если это bool
    if($this->fields[$row]["type"] == "bool") {

      if($val === "on" || $val === 1  || $val === "1" || $val === true) { $val = 1; }
      else { $val = 0; }

    }


    //если нужно зашифровать
    if($this->fields[$row]["encrypt"] == true && !empty($val)) {
      return "AES_ENCRYPT('".$val."','".$this->getSecret()."')";
    }

    //если это пароль
    if($this->fields[$row]["type"] == "password") {
      if(empty($val)) {
        $this -> error[] = ["field" => $row, "message" => "Поле не может быть пустым"];
      }
      return "PASSWORD('".md5($val).$this->getSecret()."')";
    }

    //если это html
    if($this->fields[$row]["type"] == "html") {
      $val = str_replace("'", "\'", $val);
    }


    //если допустим NULL и это NULL
    if($this -> fields[$row]["null"] === true && $val === null) {
      return "NULL";
    }

    $val = "'".$val."'";
    return $val;

  }






  // ОПРЕРАЦИИ ПО ИЗМЕНЕНИЮ ДАННЫХ


  //очищает все значения
  public function clear() {
    $this -> sql = $this -> clear;
    $this -> error = [];
    return $this;
  }

  //псевдо-удаляет из таблицы все что поподает под условие where
  public function delete($from=null) {

    //обнуляем ошибки


      //получаем старые значения
    if(!empty($this->sql["where"])) {

      $where = $this -> sql["where"];
      $this -> clear();
      $this -> sql["where"] = $where;

      $olds = $this -> get("id");
      if(empty($olds)) { $olds = false; }
    } else {
      $this -> error[] = ["message" => "Нечего удалять"];
      return false;
    }
    if(empty($olds)) {
      return false;
    }
      //делаем перебор старых значений
    foreach($olds as $old) {

      if($from === null) { $from = 'NULL'; }
      else { $from = "'$from'"; }

      $query = "UPDATE `".$this-> table."` SET `_delete` = '".time()."', `_change` = '".time()."', `_from` = ".$from." WHERE `id` = '".$old["id"]."' ";

      $this -> query = $query;
      Db::query($query);

    }
    $this -> trigger('delete');
    return true;

  }



  protected function history($field, $before, $after) {
    $_history = [
      'field' => $field,
      'before' => $before,
      'after' => $after,
    ];
  }
  protected $_history = [];


  //удаляет из таблицы насовсем
  public function remove() {

    //обнуляем ошибки


    //получаем старые значения
    if(!empty($this->sql["where"])) {

      $where = $this -> sql["where"];
      $this -> clear();
      $this -> sql["where"] = $where;



      $olds = $this -> get("id");
      if(empty($olds)) { $olds = false; }
    } else {
      $this -> error[] = ["message" => "Нечего удалять"];
      return false;
    }

    if(empty($olds)) {
      return false;
    }

    //делаем перебор старых значений
    foreach($olds as $old) {

      $query = "DELETE FROM `".$this-> table."` WHERE `id` = '".$old["id"]."' ";

      $this -> query = $query;
      Db::query($query);

    }
    $this -> trigger('delete');
    return true;
  }

  //Обновляет данные
  public function update() {

    //обнуляем ошибки
    $sql = $this -> sql;

    $values = [];    //значения
    $insert = false;  //можно ли создавать - если нечего изменять
    $error = [];    //ошибки
    $arg = func_get_args();


    //если переданы значения как массив
    if(is_array($arg[0])) {
      $values = $arg[0];
      if(isset($arg[1])) { $insert = $arg[1]; }
    }

    //если переданы значения как строки
    elseif(is_string($arg[0]) && isset($arg[1])) {
      $values[$arg[0]] = $arg[1];
      if(isset($arg[2])) { $insert = $arg[2]; }
    }



    //если не указан _from - ставим null
    if(!isset($values['_from'])) { $values["_from"] = null; }





    //получаем старые значения
    if(!empty($this->sql["where"])) {

      $where = $this -> sql["where"];
      $this -> clear();
      $this -> sql["where"] = $where;

      $olds = $this -> get();
      if(empty($olds)) { $olds = false; }
    } else {
      $olds = false;
    }

      //если старых значений нет и можно создавать
    if(!is_array($olds) && $olds == false && $insert == true) {
      if($insert == true) {
        return $this -> insert($values);
      } else {
        $this -> error[] = ["message" => "Не найдено поле, которое нужно изменить"];
        return false;
      }
    }

    $querys = [];


      //если нет старых значений
    if(!is_array($olds)) {
      $this -> error[] = ["message" => "Не найдено полей, которые нужно изменить"];
      return false;
    }

    //иначе делаем перебор старых значений
    foreach($olds as $old) {



      $update = [];

      $vals = $values;



        //смотрим какие поля действительно изменяются
      foreach($vals as $field => $val) {


          //пустой массив
        //if(is_array($val) && sizeof($val) == 1 && empty($val[0])) { $val = []; }


          //если это пароль и передано пустое значение - пропускаем
        if(empty($val) && $this->fields[$field]["type"] == "password") {
          unset($vals[$field]);
        }

        //если старое и новое значения совпадают
        elseif($old[$field] == $val) {
          unset($vals[$field]);
        }
        elseif($old[$field] === 0 && $val === "") {
          unset($vals[$field]);
        }


          //если нельзя менять это значение
        elseif($this -> fields[$field]["change"] == false) {
          unset($vals[$field]);
        }



        //если значение действительно изменилось
        else {

          //проверяем на валидность
          $val = $this -> validate($field,$val);

          //если нужно сохранить в истории
          if($this->fields[$field]["history"]) {
            $this->history($field, $old[$field], $val);
          }


          //иначе добавляем поле => значение
          if($val === null) {
            $update[] = "`".$field."` = 'NULL'";
          } else {
            $update[] = "`".$field."` = ".$val;
          }

        }

      }



      //если нечего изменять - то пропускаем
      if(empty($update)) { continue; }

      $update[] = " `_update` = '".time()."', `_change` = '".time()."' ";

      $update = implode(", ", $update);

      $query = "UPDATE `".$this-> table."` SET ".$update." WHERE `id` = '".$old["id"]."' ";

      $querys[] = $query;


    }
    if(empty($this -> error)) {
      foreach($querys as $query) {
        Db::query($query);
      }
      $this -> trigger('update');
      $this -> sql = $sql;
      return true;
    } else {

      return false;
    }



  }

  //дабавляет данные
  public function insert($val, $dupUpdate=false) {

    //обнуляем ошибки
    $this -> error = [];

    //должен быть передан массив
    if(!is_array($val)) { $this->_error('В insert передан не массив'); return false; }

    $u = $f = $v = [];

    foreach($val as $field => $param) {

      //нельзя указывать ID если он increment
      if($field === 'id' && isset($this->fields['id']["sql"]["increment"])) continue;

      //если поле начинается c _
      if($field{0} == "_" && $field !== '_from') continue;

      //если такого поля нет в таблице
      if(!isset($this->fields[$field])) continue;

      //прогоняем через валидатор
      $val[$field] = $this->validate($field,$val[$field], $dupUpdate);

      //если нужно сохранить в истории
      if($this->fields[$field]["history"]) {
        $this->history($field, null, $val[$field]);
      }

      //сохраняем значения для вставки
      $f[] = "`".$field."`";
      $v[] = $val[$field];

      //сохраняем значения для обновления
      if($dupUpdate) {
        $u[] = "`$field` = ".$val[$field];
      }


    }

    //если есть ошибки
    if(!empty($this->error)) return false;


    //добавляем время создания
    $f[] = "`_create`";
    $v[] = "'".time()."'";

    //добавляем время изменения
    $f[] = "`_change`";
    $v[] = "'".time()."'";

    //преобразовываем поля и значения в строки
    $f = implode(",", $f);
    $v = implode(",", $v);

    //формируем  sql-строку
    $query = "INSERT INTO `".$this->table."` (".$f.") VALUES (".$v.")";

    //если можно обновлять при совпадении ключей
    if($dupUpdate === true) {

      //добавляем дату обновления
      $u[] = "`_update` = '".time()."'";
      $u[] = "`_change` = '".time()."'";
      $u = implode(', ', $u);

      $query .= " ON DUPLICATE KEY UPDATE $u";

    }

    //сохраняем строку запроса
    $this->query = $query;

    //выполняем запроса в БД
    Db::query($query);

    //получаем INSERT_ID и возвращаем его
    $id = Db::$db->insert_id;
    $this -> trigger('insert');
    return $id;

  }




  // СОЗДАНИЕ / ИЗМЕНЕНИЕ СТРУКТУРЫ ТАБЛИЦ В БД



  //добавляет характеристики к полям
  protected function _addFieldsChar() {

    $fields = &$this->fields;



    //добавляем поля со временем
    $fields['_create'] = [
      'title' => 'Время создания',
      'type' => 'int',
      'null' => 'true',
      'default' => null,
    ];
    $fields['_update'] = [
      'title' => 'Время обновления',
      'type' => 'int',
      'null' => 'true',
      'default' => null,
    ];
    $fields['_delete'] = [
      'title' => 'Время удаления',
      'type' => 'int',
      'null' => 'true',
      'default' => null,
    ];
    $fields['_change'] = [
      'title' => 'Время изменения',
      'type' => 'int',
      'null' => 'true',
      'default' => null,
    ];

    $fields['_from'] = [
      'title' => 'Кто сделал изменения',
    ];




    //проходимся по всем полям

    foreach($fields as $name => $field) {


      //заголовок по умолчанию
      if(!isset($field["title"])) $field["title"] = $name;

      //тип по умолчанию
      if(!isset($field["type"])) $field["type"] = 'varchar';

      //участвует ли в форме по умолчанию
      if(!isset($field["form"])) $field["form"] = true;

      //если размер указан не в виде массива
      if(isset($field["size"]) && !is_array($field["size"])) $field["size"] = [0, $field["size"]];
      elseif(!isset($field["size"])) $field["size"] = null;

      //регулярные выражения по умолчанию
      if(!isset($field["regexp"])) $field["regexp"] = [];
      elseif(!is_array($field["regexp"])) $field["regexp"] = [$field["regexp"]];

      //можно ли изменять поле по умолчанию
      if(!isset($field["change"])) $field["change"] = true;

      //нужно ли шифровать по умолчанию
      if(!isset($field["encrypt"])) $field["encrypt"] = false;

      //дефолтное значение по умолчанию
      if(!isset($field["default"])) $field["default"] = null;

      //уникальность по умолчанию
      if(!isset($field["unique"])) $field["unique"] = false;

      //null не null по умолчанию
      if(!isset($field["null"])) $field["null"] = true;

      //history
      if(!isset($field['history'])) $field['history'] = false;

      /*************
      * Типы полей *
      *************/


      // Relation
      if(isset($field["relation"])) {

        $field["type"] = 'relation';

        //размер по умолчанию
        if(!isset($field["size"])) $field["size"] = [0, 1];

        //характеристики поля в БД
        $field["sql"] = [
          'type'     => 'varchar',
          'size'     => 100,
          'default'   => $field["default"],
          'null'     => $field["null"],
          'index'   => null,
        ];


        //если связи - к многим
        if($field["size"][1] > 1) {
          $field["sql"]["type"] = 'text';
          $field["sql"]["size"] = null;
        }


      }

      // Int и Float
      elseif($field["type"] == 'int' || $field["type"] == 'float') {

        //если не указан null
        if(!isset($field["null"])) $field["null"] = true;

        //если не указано значение по умолчанию
        if(!isset($field["default"])) {
          if($field["null"]) $field["default"] = null;
          else $field["default"] = 0;
        }

        //если не указан размер
        if(!isset($field["size"])) $field["size"] = [0, 11];

        //регулярное выражение для чисел
        if(!in_array($field["type"], $field["regexp"])) $field["regexp"][] = $field["type"];

        //характеристики поля в БД
        $field["sql"] = [
          'type'     => $field["type"],
          'size'     => $field["size"][1],
          'default'   => $field["default"],
          'null'     => $field["null"],
          'index'   => null,
        ];


      }

      // Password
      elseif($field["type"] == 'pass' || $field["type"] == 'password') {

        $field["type"] = 'password';


        //регулярное выражение для пароля
        if(!in_array("pass", $field["regexp"])) $field["regexp"][] = "pass";


        $field["sql"] = [
          'type'     => 'varchar',
          'size'     => 50,
          'default'   => null,
          'null'     => true,
          'index'   => null,
        ];

      }

      // Varchar
      elseif($field["type"] == 'varchar') {

        //размер по умолчанию
        if(!isset($field["size"])) $field["size"] = [0, 255];


        $field["sql"] = [
          'type'     => 'varchar',
          'size'     => $field["size"][1],
          'default'   => $field["default"],
          'null'     => $field["null"],
          'index'   => null,
        ];

      }

      // Булевый
      elseif($field["type"] == 'bool') {

        //определяем корректное значение по умолчанию
        if($field["default"]) $field["default"] = true;
        else $field["default"] = false;

        //этому полю нельзя присвоить null
        $field["null"] = false;

        //размер определит mysql автоматически
        $field["size"] = null;


        $field["sql"] = [
          'type'     => 'bool',
          'size'     => null,
          'default'   => 0,
          'null'     => false,
          'index'   => null,
        ];

        if($field["default"]) $field["sql"]["default"] = 1;

      }

      // Text
      elseif($field["type"] == 'text') {

        $field["sql"] = [
          'type'     => 'text',
          'size'     => null,
          'default'   => $field["default"],
          'null'     => $field["null"],
          'index'   => null,
        ];


      }

      // Html
      elseif($field["type"] == 'html') {

        $field["sql"] = [
          'type'     => 'longtext',
          'size'     => null,
          'default'   => $field["default"],
          'null'     => $field["null"],
          'index'   => null,
        ];


      }

      // Json
      elseif($field["type"] == 'json') {

        $field["sql"] = [
          'type'     => 'text',
          'size'     => null,
          'default'   => $field["default"],
          'null'     => $field["null"],
          'index'   => null,
        ];

      }

      // Массив
      elseif($field["type"] == 'array') {

        $field["sql"] = [
          'type'     => 'text',
          'size'     => null,
          'default'   => $field["default"],
          'null'     => $field["null"],
          'index'   => null,
        ];

      }

      // Файлы
      elseif($field["type"] == 'files' || $field["type"] == 'file') {

        if(!isset($field["size"])) $field["size"] = [0, 1];


        $field["sql"] = [
          'type'     => 'text',
          'size'     => null,
          'default'   => $field["default"],
          'null'     => $field["null"],
          'index'   => null,
        ];

      }

      // Цвет
      elseif($field["type"] == 'color') {

        $field["sql"] = [
          'type'     => 'varchar',
          'size'     => 50,
          'default'   => $field["default"],
          'null'     => $field["null"],
          'index'   => null,
        ];

      }

      // Иконка
      elseif($field["type"] == 'icon') {

        $field["sql"] = [
          'type'     => 'varchar',
          'size'     => 50,
          'default'   => $field["default"],
          'null'     => $field["null"],
          'index'   => null,
        ];

      }

      // Дата, Время
      elseif($field["type"] == 'date' || $field["type"] == 'time' || $field["type"] == 'dt') {

        $field["sql"] = [
          'type'     => ($field["type"] == 'dt') ? 'datetime' : $field["type"],
          'size'     => null,
          'default'   => $field["default"],
          'null'     => $field["null"],
          'index'   => null,
        ];

      }



      // если нужно шифровать
      if($field["encrypt"] === true) {
        $field["sql"]["type"] = 'blob';
        $field["sql"]["size"] = null;
      }


      //если поле уникальное
      if($field["unique"] === true) {
        $field["sql"]["index"] = 'UNIQUE KEY';
        $field["sql"]["null"] = false;
      }

      //сохраняем
      $fields[$name] = $field;
    }



    //если нет поля ID - создаем его
    if(!isset($fields['id'])) {

      $fields["id"] = [
        'title' => 'Идентификатор',
        'type'  => 'int',
        'null'  => false,
        'size'  => [0, 11],
        'encrypt' => false,
        'unique' => true,
        'sql' => [
          'type'     => 'int',
          'size'     => 11,
          'default'   => null,
          'null'     => false,
          'index'   => 'PRIMARY KEY',
          'increment' => true,
        ],
      ];

    } else {
      $fields['id']["unique"] = true;
      $fields['id']["sql"]["index"] = 'PRIMARY KEY';
    }



  }


  // СТАТИЧНЫЕ МЕТОДЫ


  //получает форму
  public function form($rule='cru', $reload=false, $def=null, $js=null) {


    if(isset($this->sql["id"])) {
      $form = Form::init($this,$this->sql["id"]);
    } else {
      $form = Form::init($this);
    }

    if($reload === false) {}
    elseif($reload===true) { $form->reload();}
    else { $form-> reload($reload); }

    if(isset($def)) {
      $form -> def($def);
    }

    if(isset($js)) {
      $form->config["js"] = $js;
    }


    if(!empty($this->sql["fields"])) {
      $ff = [];
      foreach($this->sql["fields"] as $f => $i) { $ff[] = $f; }
      $form->fields($ff);
    }

    if(!isset($_SERVER["HTTP_X_FORM_AJAX"])) {
      return $form->get($rule);
    }




  }


  //получает angular-массив для ws-сервера
  public function angular($rule='r') {

    //если переданы поля - но нет ID
    if(!empty($this->sql["fields"])) {
      if(!isset($this->sql["fields"]["id"])) {
        $this->fields('id');
      }
    }

    return Angular::init($this, $rule);
  }



  //выводит лог оь ошибке
  protected function _error($str) {
    Log::err("Таблица ".$this->class." - ".$str);
  }

}
