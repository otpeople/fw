<?php

namespace fw\core;

class File {


    //получить название файла, первые символы которого - деректории
  public static function subdirs($file, $dirs=2) {
    if($dirs == 1) {
      $file = substr($file, 0, 2).'/'.substr($file, 2);
    } elseif($dirs == 2) {
      $file = substr($file, 0, 2).'/'.substr($file, 2, 2).'/'.substr($file, 4);
    } elseif($dirs == 3) {
      $file = substr($file, 0, 2).'/'.substr($file, 2, 2).'/'.substr($file, 4, 2).'/'.substr($file, 6);
    } elseif($dirs == 4) {
      $file = substr($file, 0, 2).'/'.substr($file, 2, 2).'/'.substr($file, 4, 2).'/'.substr($file, 6, 2).'/'.substr($file, 8);
    } elseif($dirs == 5) {
      $file = substr($file, 0, 2).'/'.substr($file, 2, 2).'/'.substr($file, 4, 2).'/'.substr($file, 6, 2).'/'.substr($file, 8, 2).'/'.substr($file, 10);
    }
    return $file;
  }


  //создает директорию вместе со всеми поддиректориями
  public static function mkdir($dir) {

    $array = explode("/", $dir);

    //если передан файл -> то цель = его директория
    if(strpos($array[sizeof($array)-1], ".")) {
      $dir = dirname($dir);
    }


    //если директория не существует
    if(!file_exists($dir)) {

      //если не существует директория родителя -> создаем ее
      if(!file_exists(dirname($dir))) {
        self::mkdir(dirname($dir));
      }

      //создаем текущую директорию
      mkdir($dir);
    }
  }



  //записывает содержимое в файл
  public static function write($file, $content) {

    if(!file_exists($file)) { self::mkdir($file); }

    $fp = fopen($file, "w");
    fwrite($fp, $content);
    fclose($fp);
  }

  //добавляет содержимое в файл
  public static function append($file, $content) {

    if(!file_exists($file)) { self::mkdir($file); }

    if($content{0} != "\n") { $content = "\n".$content; }

    $fp = fopen($file, "a");
    fwrite($fp, $content);
    fclose($fp);

  }


  //устанавливает кеш
  public static function setCache($content, $time, $token=null) {

    if(!isset($token)) {
      $token = sha1(uniqid());
    }

    $name = $token . '.json';

    $file = DIR .'/files/tmp/'.self::subdirs($name);

    $content = json_encode(['content' => $content, 'time' => time() + $time], JSON_UNESCAPED_UNICODE);

    self::write($file, $content);

    return $token;

  }

  //получает кеш
  public static function getCache($token, $del=false, $cont=true) {



    $file = DIR.'/files/tmp/'.self::subdirs($token.'.json');

    if(!file_exists($file)) { return  false; }



    $content = json_decode(file_get_contents($file), true);



    if($content["time"] < time()) { return false; }


    //если нужно удальть файл кеша
    if($del) { unlink($file); }


    //если нужно вернуть контент
    if($cont === true) {
      return $content['content'];
    } else {
      return true;
    }


  }

  //удалить кеш
  public static function rmCache($token) {
    $file = DIR.'/files/tmp/'.self::subdirs($token.'.json');
    if(file_exists($file)) {
      unlink($file);
    }
  }


}
