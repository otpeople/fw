<?php

namespace fw\core;


class Log {

  const EMERG   = 0;  // Emergency: system is unusable
  const ALERT   = 1;  // Alert: action must be taken immediately
  const CRIT    = 2;  // Critical: critical conditions
  const ERR     = 3;  // Error: error conditions
  const WARN    = 4;  // Warning: warning conditions
  const NOTICE  = 5;  // Notice: normal but significant condition
  const INFO    = 6;  // Informational: informational messages
  const DEBUG   = 7;  // Debug: debug messages


  //ЧП: система непригодна для использования
  public static function emerg($msg, $file=null, $line=null) {
    self::add(0, $msg, $file, $line);
  }

  //Предупреждение: действия должны быть предприняты немедленно
  public static function alert($msg, $file=null, $line=null) {
    self::add(1, $msg, $file, $line);
  }

  //Критическая ситуация: критические состояния
  public static function crit($msg, $file=null, $line=null) {
    self::add(2, $msg, $file, $line);
  }

  //Ошибка: ошибка условия
  public static function err($msg, $file=null, $line=null) {
    self::add(3, $msg, $file, $line);
  }

  //Предупреждение: предупреждение условий
  public static function warn($msg, $file=null, $line=null) {
    self::add(4, $msg, $file, $line);
  }

  //Уведомление: нормальный, но существенное условие
  public static function notice($msg, $file=null, $line=null) {
    self::add(5, $msg, $file, $line);
  }

  //Информационные: информационные сообщения
  public static function info($msg, $file=null, $line=null) {
    self::add(6, $msg, $file, $line);
  }

  //Отладка: отладка Сообщений
  public static function debug($msg, $file=null, $line=null) {
    self::add(7, $msg, $file, $line);
  }

  



  private static function add($type, $msg, $file, $line) {

    if(!isset(self::$logger)) {

      $writer = new \Zend\Log\Writer\Stream(DIR."/logger.log");
      self::$logger = new \Zend\Log\Logger();
      self::$logger->addWriter($writer);
    }


    if(is_string($msg)) {

      //если указан файл
      if(isset($file)) {
        $msg .= ' in '.$file;
      }

      //если указана строка
      if(isset($line)) {
        $msg .= ' on line '.$line;
      }
    }


    self::$logger -> log($type, $msg);
  }

  private static $logger;



}
