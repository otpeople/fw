<?php

namespace fw\core;

use fw\tables\Caches;

use fw\core\App;
use fw\core\Log;
use fw\core\File;

class Cache {


  //директория для хранения кеша
  public static $dir = DIR.'/files/cache';




  //получить значение
  public static function get($key, $value=null, $param=[]) {

    //если вместо параметров передано число - значит это время
    if(is_numeric($param)) $param = ['time' => $param];


    //удалять/не удлаять по умолчанию
    if(!isset($param["remove"])) $param["remove"] = false;


    //получаем название файла
    $file = self::_getFileName($key);


    //если файл не найден или через filemtime мы понимаем что он просрочен
    if( !file_exists($file) || (isset($param["time"]) && filemtime($file)+$param["time"] < time()) ) {

      //если не передано значение по умолчанию - возвращаем null
      if(!isset($value)) { return null; }

      //если есть значение по умолчанию - то делаем кеш из этого значения
      else { return self::set($key, $value, $param); }
    }



    return self::_get($key, $value, $param, $file);




  }

  //получает содержимое файла (с учетом что файл может записываться в данный момент)
  private static function _get($key, $value, $param, $file, $try=0) {


    //получаем содержимое файла
    $cont = file_get_contents($file);


    //если сейчас этот файл записывается
    if($cont === 'writing...') {
      if($try == 30) {
        return null;
      } else {
        sleep(1);
        return self::_get($key, $value, $param, $file, $try++);
      }
    }


    $cont = json_decode($cont, true);

    //если время вышло - удаляем кеш и возвращаем null (или создам кеш)
    if($cont["time"] < time()) {

      //если не передано значение по умолчанию - возвращаем null
      if(!isset($value)) { return null; }

      //если есть значение по умолчанию - то делаем кеш из этого значения
      else { return self::set($key, $value, $param); }

    } else {

      //если при получении куша нужно удалить его - удаляем
      if($cont["remove"] || $param["remove"]) {
        unlink($file);
      }


      return $cont["value"];
    }


  }




  //установить значение
  public static function set($key, $value, $param=[]) {


    //если не передан $key - генерируем его
    if(!isset($key)) {
      $returnKey = true; // флаг что мы должны вернуть ключ
      $key = md5(uniqid());
    } else {
      $returnKey = false;
    }

    //если вместо параметров передано число - значит это время
    if(is_numeric($param)) $param = ['time' => $param];

    //параметры по умолчанию
    if(!isset($param["time"])) $param["time"] = 60;  //время
    if(!isset($param["remove"])) $param["remove"] = false;


    //имя файло кеша
    $file = self::_getFileName($key);

    //объявляем о записи в файл
    File::write($file, 'writing...');




    //если value - это замыкание
    if(is_callable($value)) $value = $value();

    //время, когда кеш будет просроченным
    $param["time"] = time() + $param["time"];




    //сохраняем файл
    File::write($file, json_encode([
      'time'    => $param["time"],
      'value'  => $value,
      'remove' => $param["remove"]
    ], JSON_UNESCAPED_UNICODE));


    //если указаны таблицы при изменении которых нужно очищать кеш
    if(isset($param["tables"])) {
      if(!is_array($param["tables"])) $param["tables"] = [$param["tables"]];

      foreach($param["tables"] as $table) {


        //генерируем название файла для таблицы
        $txt = self::tableTxt($table);
        if(!$txt) continue;

        //записываем в файл
        File::append($txt, $file);

      }

    }



    return ($returnKey) ? $key : $value;

  }



  //генерирует имя файла для таблицы
  public static function tableTxt($table) {

    //если таблица передана без неймспейсов - добавляем их
    if(strpos($table, '\\') === false) {
      $table = '\\'.MOD.'\\tables\\'.$table;
    }

    //если класса такой таблицы нет - выводим ошибку в лог
    if(!class_exists($table)) {
      Log::warn("При кешировании не найдена таблица $table");
      return false;
    }

    //заменяем неймспейсы
    if($table{0} === '\\') $table = substr($table, 1, 999);
    $table = str_replace('\\', '_', $table);

    //убирает _table_
    $table = str_replace('_tables_', '_', $table);

    return self::$dir.'/tables/'.$table.'.txt';
  }



  //удалить кеш
  public static function remove($key) {

    $file = self::_getFileName($key);
    if(file_exists($file)) unlink($file);

  }



  //получить имя файла кеша по ключу
  private static function _getFileName($key) {
    return self::$dir.'/'.File::subdirs(md5($key)).'.json';
  }



  //при изменении таблицы
  public static function changeTable($table) {


    //получаем название файл
    $txt = self::tableTxt($table);
    if(!$txt || !file_exists($txt)) return false;

    //получаем содержимое файла и удаляем его
    $caches = array_unique(file($txt)); unlink($txt);

    //удаляем все закешированные файлы
    $array = [];
    foreach($caches as $one) {
      if(!isset($array[$one])) {
        if(file_exists($one)) { unlink($one); }
        $array[$one] = true;
      }
    }


  }


}





?>
