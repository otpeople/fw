<?php
namespace fw\core;

use fw\core\File;

class Assets {

  public static function clear() {
    self::$files = [
      'script' => [],
      'style'  => [],
      'other'  => [],
      'js' => [],
      'css' => [],
    ];
    self::$tmp = [
      'js' => [],
      'css' => [],
    ];
  }

  public static $files = [
    'script' => [],
    'style'  => [],
    'other'  => [],
    'js' => [],
    'css' => [],
  ];




  //временные файлы
  public static $tmp = [
    'js' => [],
    'css' => [],
  ];


  public static $filesC = [
    'script' => [],
    'style'  => [],
    'other'  => []
  ];

  public static $meta = ['<meta charset="utf-8">'];

  public static $types = [
    'script' => ['js',"coffee","cjsx"],
    'style'  => ['css',"scss","less","styl"],
    'other'  => ['woff','woff2','ttf','svg','map',"otf","eot"],
    'image'  => ['jpg','png',"gif"]
  ];

  public static $libs = [];

  public static function get_libs() {

    //получаем системные библиотеки
    if(file_exists(FW.'/assets.json')) {
      $libs = file_get_contents(FW.'/assets.json');
      $libs = json_decode($libs,true);

      if(is_array($libs)) {
        foreach($libs as $lib => $cont) {
          if(isset($cont["dir"])) {
            $dir = FW.'/'.$cont["dir"].'/';
          } else {
            $dir = FW.'/';
          }
          if(isset($cont["files"])) {
            foreach ($cont["files"] as $key => $value) {
              $cont["files"][$key] = $dir.$value;
            }
          }
          self::$libs[$lib] = $cont;
          if(isset($cont["meta"])) {
            foreach ($cont["meta"] as $key => $value) {
              self::addMeta($value);
            }
          }
        }
      }
    }

    //получаем бибилиотеки приложения
    if(file_exists(DIR.'/assets.json')) {
      $libs = file_get_contents(DIR.'/assets.json');
      $libs = json_decode($libs,true);
      if(is_array($libs)) {
        foreach($libs as $lib => $cont) {
          if(isset($cont["files"])) {
            foreach ($cont["files"] as $key => $value) {
              $cont["files"][$key] = DIR."/".$value;
            }
          }
          self::$libs[$lib] = $cont;
          if(isset($cont["meta"])) {
            foreach ($cont["meta"] as $key => $value) {
              self::addMeta($value);
            }
          }
        }
      }

    }



  }


  //добавляет ресурсы, которые нужно будет подключить
  public static function addOne($arg) {

    //если это файл,
    if(file_exists($arg)) {
      self::addFile($arg);
    }

    //если это библиотека
    else {

      //если библиотеки не загружены - загружаем
      if(empty(self::$libs)) { self::get_libs(); }

      //если такая библиотека существует
      if(isset(self::$libs[$arg])) {

        //если есть зависимоси
        if(isset(self::$libs[$arg]["depends"])) {

          //подключаем зависимости
          foreach(self::$libs[$arg]["depends"] as $dep) {
            self::addOne($dep);
          }
        }

        //добавляем файлы
        if(isset(self::$libs[$arg]["files"])) {
          foreach(self::$libs[$arg]["files"] as $file) {
            if(isset(self::$libs[$arg]["dir"])) {
              $dir = self::$libs[$arg]["dir"];
            } else {
              $dir = null;
            }
            self::addFile($file, $dir);
          }
        }

      }

    }
  }



  public static function add(...$args) {


    if(LIVERELOAD) Livereload::addAssets(...$args);


    //Log::debug($args);
    self::$tmp = ['js'=>[], 'css'=>[]];


    foreach($args as $arg) {
      self::addOne($arg);
    }


    //обработка js
    if(!empty(self::$tmp["js"])) {

      //если всего один файл
      if(sizeof(self::$tmp["js"]) == 1) {
        $distFile = self::$tmp["js"][0];
      }

      //если более 1 js файла - сжимаем в 1
      elseif(sizeof(self::$tmp["js"]) > 1) {

        $distDir = WEB."/assets/".File::subdirs(md5(implode('_', self::$tmp["js"])), 3);
        $distFile = $distDir.'/script.min.js';

        //создаем файл
        if(!file_exists($distFile)) {
          File::mkdir($distDir);
          $node = App::getConfig('local', 'node');
          $uglify = FW.'/node_modules/.bin/uglifyjs';
          $files = implode(' ', self::$tmp["js"]);
          $map = $distFile.'.map';
          $command = "$node $uglify $files -o $distFile --source-map $map";
          //Log::debug($command);

          //выполняем команду
          exec($command);

          //исправляем пути в sourse.map и js файле
          file_put_contents($map, str_replace(DIR.'/public', '', file_get_contents($map)));
          file_put_contents($distFile, str_replace(DIR.'/public', '', file_get_contents($distFile)));

        }


      }

      self::$files["js"][] = $distFile;

    }

    //обработка css
    if(!empty(self::$tmp["css"])) {

      if(sizeof(self::$tmp["css"]) == 1) {
        $file = self::$tmp["css"][0];
      } else {

        $files = implode(' ', self::$tmp["css"]);
        $dir = WEB."/assets/".File::subdirs(md5($files), 3);
        $file = $dir.'/style.min.css';

        if(!file_exists($file)) {
          File::mkdir($dir);
          $node = App::getConfig('local', 'node');
          $uglify = FW.'/node_modules/.bin/uglifycss';
          $clean = FW.'/node_modules/.bin/cleancss';
          $command = "$node $clean -o $file $files";
          exec($command);
          //Log::debug($file);
          //Log::debug($command);
        }

      }
      self::$files["css"][] = $file;
    }


    //преобразем в URL
    //str_replace(DIR.'/public/', '/', $distFile)









    //Log::debug(self::$tmp["js"]);

  }



  public static function addMeta($meta) {


    if(is_array($meta)) {
      $temp=[];
      foreach($meta as $name => $val) {
        $temp[] = $name.'="'.$val.'"';
      }
      $meta = '<meta '.implode(" ", $temp).'>';
    }

    if(!in_array($meta, self::$meta)) {
      self::$meta[] = $meta;
    }
  }




  //добавляет файлы
  public static function addFile($file, $dir=null) {

    //проверяем чтобы файлы не загружались более одного раза
    static $saved = [];

    if(in_array($file, $saved)) { return false; }
    else { $saved[] = $file; }
    if(LIVERELOAD) Livereload::addFile($file);



    //если это директория
    if(is_dir($file)) {
      $dir = scandir($file);
      //сканируем и добавляем каждый файл
      foreach($dir as $one) {
        if($one != '.' && $one != '..') {
          self::addFile($file.'/'.$one);
        }
      }
      return false;
    }

    //расширение файла
    $info = pathinfo($file, PATHINFO_EXTENSION);

    //если это скрипт
    if(in_array($info, self::$types['script'])) {
        //если файл не добавляелся ранее
      if( !in_array($file, self::$files['script']) && !in_array($file, self::$filesC['script']) ) {
        self::$tmp['js'][] = self::md($file, $dir);
      }
    }

    //если это стили
    else if(in_array($info, self::$types['style'])) {
      if( !in_array($file, self::$files['style']) && !in_array($file, self::$filesC['style']) ) {
        self::$tmp['css'][] = self::md($file, $dir);
      }
    }

  }







  public static function compileCoffee($file) {
    $coffee = FW.'/node_modules/coffee-script/bin/coffee';
    $node = App::getConfig('local', 'node');
    $command = "$node $coffee -cm $file";
    exec($command, $ret);
    if(!empty($ret)) {
      Log::debug("Ошибка компиляции:".implode("\n", $ret));
    }


  }


  public static function compileCjsx($file) {
    $bin = FW.'/node_modules/.bin/cjsx';
    $node = App::getConfig('local', 'node') ?? 'node';
    $command = "$node $bin -cm $file";
    exec($command, $ret);
    if(!empty($ret)) {
      Log::debug("Ошибка компиляции:".implode("\n", $ret));
    }

  }

  public static function compileStyl($file) {

    $dir = dirname($file);
    $styl = FW.'/node_modules/.bin/stylus';
    $node = App::getConfig('local', 'node');
    $command = "cd $dir; $node $styl -u nib $file -m";
    //Log::debug($command);
    exec($command, $ret);
    //Log::debug($ret);
    //if(!empty($ret)) {
    //  Log::debug("Ошибка компиляции:".implode("\n", $ret));
    //}

  }




  //вывод
  public static function styles() {
    $styles = [];
    foreach(self::$files['style'] as $file) {
      $styles[] = self::md($file,'css');
    }
    foreach(self::$files['other'] as $file) {
      self::md($file);
    }

      //если есть сжатые стили
    if(!empty(self::$filesC["style"])) {
      $styles[] = self::md(self::$filesC["style"],'css');
    }

    return $styles;
  }

  public static function scripts() {

    $scripts = [];
    foreach(self::$files['script'] as $file) {
      $scripts[] = self::md($file,'js');
    }

    return $scripts;
  }

  public static function metas() {
    $metas = "";
    foreach(self::$meta as $meta) {
      $metas .= $meta;
    }
    return $metas;
  }



  //генерация файлов
  public static function md($file, $dir=null) {

    //определяем расширение файла
    $info = strtolower(pathinfo($file, PATHINFO_EXTENSION));

    //определяем директорию выходного файла
    if(isset($dir)) {
      $dir = FW.'/'.$dir;
      $distDir = WEB."/assets/".File::subdirs(md5($dir.filemtime($dir)), 3);
      exec("cp -Rf $dir $distDir");
      $fileName = str_replace(dirname($dir), '', $file);
      $distFile = $distDir.$fileName;
    } else {
      $distDir = WEB."/assets/".File::subdirs(md5($file.filemtime($file)), 3);
      $fileName = pathinfo($file, PATHINFO_BASENAME);
      $distFile = $distDir.'/'.$fileName;
    }




    //если такой файл не найден - создаем
    if(!file_exists($distFile)) {



      //создаем нужную папку, если ее нет
      File::mkdir($distFile);

      //скрипты - стили
      if($info == "js" || $info == 'css') {
        copy($file, $distFile);
      }
      elseif($info == 'styl') {
        copy($file, $distFile);
        self::compileStyl($distFile);
      }
      elseif($info == 'coffee') {
        copy($file, $distFile);
        self::compileCoffee($distFile);
      }
      elseif($info == 'cjsx') {
        copy($file, $distFile);
        self::compileCjsx($distFile);
      }


    }

    if($info == 'styl') {
      $distFile = str_replace('.styl', '.css', $distFile);
    }
    elseif($info == 'coffee') {
      $distFile = str_replace('.coffee', '.js', $distFile);
    }
    elseif($info == 'cjsx') {
      $distFile = str_replace('.cjsx', '.js', $distFile);
    }



    if(file_exists($distFile)) {
      return $distFile;
    } else {
      Log::err("Ошибка компиляции ".str_replace(DIR, '', $file));
    }



  }


    //находит изображение и сохдает его шифрованную копию
  public static function img($file,$size=0,$nophoto="base") {



    //если передан массив файлов - берем только первый файл
    if(is_array($file) && !empty($file)) { $file = $file[0];}

    $img = $file;

    //если файл не существует
    if(empty($file) || !file_exists($file)) {
      $file = FW.'/assets/img/noPhoto/'.$nophoto.'.jpg';
      if(!file_exists($file)) {
        $file = FW.'/assets/img/noPhoto/base.jpg';
      }
    }

    $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));

    if($ext == "jpg" || $ext == "png" || $ext =="jpeg" || $ext =='gif') {

      // размер изображения
      if(empty($size)) {
        $size = [];
      } elseif(is_string($size)) {
        $size = explode('x', $size);
        $size = [$size[0], $size[1]];
      } elseif(is_numeric($size)) {
        $size = [$size, $size];
      }

      //копируем в публичную папку

      //Log::debug($file);

      // определяем путь изображения
      $distFile = WEB."/images/".File::subdirs(md5($file.filemtime($file).implode('',$size)), 3).'.'.$ext;

      // если такого файла еще нет - создаем
      if(!file_exists($distFile)) {
        File::mkdir($distFile);
        copy($file, $distFile);

          //если задан размер - обрезаем
        if(!empty($size)) {
          self::resize($distFile,$size[0],$size[1]);
        }
      }

      // возвращаем путь относительно public
      return str_replace(DIR.'/public', '', $distFile);
    }
    else {

      //если есть картинка для этого расширения

      $dir = FW."/assets/img/filetype/";
      $img = $dir.$ext.".jpg";
      if(file_exists($img)) {
        $file = $img;
      }

      //если нет картинки для этого расширения
      else {
        $file = FW.'/assets/img/filetype/file.jpg';
      }

      return self::img($img, $size);
    }


  }




  //$x_o и $y_o - координаты левого верхнего угла выходного изображения на исходном
  //$w_o и h_o - ширина и высота выходного изображения
  public static function crop($image, $x_o, $y_o, $w_o, $h_o) {



    if (($x_o < 0) || ($y_o < 0) || ($w_o < 0) || ($h_o < 0)) {
      //Некорректные входные параметры
      return false;
    }
    list($w_i, $h_i, $type) = getimagesize($image); // Получаем размеры и тип изображения (число)
      $types = array("", "gif", "jpeg", "png"); // Массив с типами изображений
      $ext = $types[$type]; // Зная "числовой" тип изображения, узнаём название типа
    if ($ext) {
      $func = 'imagecreatefrom'.$ext; // Получаем название функции, соответствующую типу, для создания изображения
      $img_i = $func($image); // Создаём дескриптор для работы с исходным изображением
    } else {
      //echo 'Некорректное изображение'; // Выводим ошибку, если формат изображения недопустимый
      return false;
    }
    if ($x_o + $w_o > $w_i) $w_o = $w_i - $x_o; // Если ширина выходного изображения больше исходного (с учётом x_o), то уменьшаем её
    if ($y_o + $h_o > $h_i) $h_o = $h_i - $y_o; // Если высота выходного изображения больше исходного (с учётом y_o), то уменьшаем её
    $img_o = imagecreatetruecolor($w_o, $h_o); // Создаём дескриптор для выходного изображения

    imagealphablending($img_o, false);
    imagesavealpha($img_o, true);


    imagecopy($img_o, $img_i, 0, 0, $x_o, $y_o, $w_o, $h_o); // Переносим часть изображения из исходного в выходное
    $func = 'image'.$ext; // Получаем функция для сохранения результата
    return $func($img_o, $image); // Сохраняем изображение в тот же файл, что и исходное, возвращая результат этой операции
  }


  //$w_o и h_o - ширина и высота выходного изображения
  public static function resize($image, $w_o = false, $h_o = false) {

    if (($w_o < 0) || ($h_o < 0)) {
      //echo "Некорректные входные параметры";
      return false;
    }
    list($w_i, $h_i, $type) = getimagesize($image); // Получаем размеры и тип изображения (число)
    $types = array("", "gif", "jpeg", "png"); // Массив с типами изображений
    $ext = $types[$type]; // Зная "числовой" тип изображения, узнаём название типа
    if ($ext) {
      $func = 'imagecreatefrom'.$ext; // Пwc олучаем название функции, соответствующую типу, для создания изображения
      $img_i = $func($image); // Создаём дескриптор для работы с исходным изображением
    } else {
      echo 'Некорректное изображение'; // Выводим ошибку, если формат изображения недопустимый
      return false;
    }
    /* Если указать только 1 параметр, то второй подстроится пропорционально */
    if (!$h_o) $h_o = $w_o / ($w_i / $h_i);
    if (!$w_o) $w_o = $h_o / ($h_i / $w_i);
    $img_o = imagecreatetruecolor($w_o, $h_o); // Создаём дескриптор для выходного изображения

    imagealphablending($img_o, false);
    imagesavealpha($img_o, true);

    imagecopyresampled($img_o, $img_i, 0, 0, 0, 0, $w_o, $h_o, $w_i, $h_i); // Переносим изображение из исходного в выходное, масштабируя его
    $func = 'image'.$ext; // Получаем функция для сохранения результата
      return $func($img_o, $image); // Сохраняем изображение в тот же файл, что и исходное, возвращая результат этой операции
  }




  public static function mkdir($dir) {


    $array = explode("/", $dir);

      //если передан файл -> то цель = его директория
    if(strpos($array[sizeof($array)-1], ".")) {
      $dir = dirname($dir);
    }


      //если директория не существует
    if(!file_exists($dir)) {

        //если не существует директория родителя -> создаем ее
      if(!file_exists(dirname($dir))) {
        self::mkdir(dirname($dir));
      }
        //создаем текущую директорию
      mkdir($dir);
    }

  }




}
