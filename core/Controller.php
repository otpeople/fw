<?php
namespace fw\core;

use fw\core\View;
use fw\core\App;
use fw\core\Cache;
use fw\core\Access;
use fw\core\Log;
use fw\core\Livereload;
use fw\help\Func as F;

class Controller {

  /**
   * Имя контроллера
   * @example IndexController -> index
   */
  protected $__name;

  /**
   * Директория для представлений этого контроллера
   * @example IndexController -> /views/index
   */
  protected $__views;  //директория для представлений


  /**
   * Название экшена (метода), который будет вызван
   */
  protected $__action;


  public $request;
  public $response;

  /**
   * Конструктор контроллера
   */
  public function __construct() {

    //сохраняем директорию
    $dir = (new \ReflectionClass(get_called_class())) -> getFileName();

    //Если включен livereload - добалвяем этот класс в livereload
    if(LIVERELOAD) Livereload::addClass(get_called_class());

    //определяем имя контроллера
    $this -> __name = strtolower(str_replace('Controller.php', '', substr($dir, strrpos($dir, '/')+1)));

    //определяем диреткорию вида приложения
    $this -> __views = dirname(dirname($dir)) . "/views/" . $this -> __name;


  }


  /**
   * Отрисовывет представление
   * @method render
   * @param  string $action Имя представления
   * @param  array  $vars   Переменные, которые будут переданы в представления
   * @return @void
   */
  public function render(string $action, array $vars=[]) {

    //если в экшене есть слеш -> значит указана папка другого контроллера
    if(strstr($action, '/')) {
      $action = explode('/', $action);

      //если указан модуль
      if(isset($action[2])) {

        //если модуль - фреймворк
        if($action[0] == 'fw') {
          $action = FW . '/views/' . $action[1] . '/' . $action[2];
        } else {
          $action = dirname(dirname($this->__views)) . '/' . $action[0] . '/'. $action[1]. '/' . $action[2];
        }
      } else {
        $action = dirname($this->__views) . '/' . $action[0] . '/' . $action[1];
      }
    } else {
      $action = $this->__views . '/'. $action;
    }

    //передаем действие классу View
    (new View($action, $vars)) -> render();
  }


  /**
   * Находим экшен (метод контроллера) и вызывает его
   * @method __action
   * @param  string   $method Имя экшена (метода класса контроллера)
   * @param  array    $args   Переменные, которые передадутся в параметры метода
   * @return @void
   */
  public function __action(string $method, array $args = []) {

    //сохраняем название экшена
    $this->__action = $method;

    //ищем метод
    //если не найдет переданный метод
    if(!method_exists($this, $method)) {
      //находим метода с префиксом action
      //index -> indexAction
      $method = "action".ucfirst($method);
      if(!method_exists($this, $method)) {
        //вызываем метод _error(404)
        $this->_error(404);
      }
    }

    //проверяем на доступ
    if(php_sapi_name() !== 'cli' && isset($this->access)) {
      if(!Access::to($this->access, $this->__action)) {
        $this->_error(403);
      }
    }


    $_this = &$this;

    //функиця, которая возвращает содержимое view
    $callback = function() use (&$_this, $method, $args) {

      //это замыкание будет вызвано если кеша нет
      ob_start();
        $_this -> $method(...$args);
        $content = ob_get_contents();
      ob_end_clean();

      //возвращаем контент
      return $content;
    };


    //если не нужно кешировать
    if(!isset($this->cache) || !isset($this->cache[$this->__action])) {
      $content = $callback();

    //если нужно кешировать
    } else {

      //получаем параметры кеша
      $cache = &$this->cache[$this->__action];

      //преобразуем в массив
      if(!is_array($cache)) $cache = ['time' => $cache];
      if(!isset($cache["tables"])) $cache["tables"] = null;

      //токен для идентификаии запроса
      $token    = get_called_class().$this->__action.implode(",", $args).implode("", $_POST).implode("", $_GET);
      $token  .= (isset($_SERVER["HTTP_X_PJAX"])) ? " (pjax)" : "";

      //выводим закешированные данные
      //замыкание вернет закешированный контент, и создаст его если нужно
      $content = Cache::get($token, $callback ,[
        'time' => $cache["time"],
        'tables' => $cache["tables"],
      ]);

    }

    //возвращаем контент
    return $content;

  }


  /**
   * Выводит массив данные в формате application/json
   * @method json
   * @param  array $array  массив, который будет преобразован в json
   * @return @void
   */
  public function json(array $array) {
    $this->response->headers->set('Content-Type', 'application/json');
    $this->response->setContent(json_encode($array));
  }


  /**
   * Получить переменные из php://input (например, POST-запрос в angularjs)
   * @method ngPost
   * @param  string|null     $var переменная (не обязательно)
   * @return array|string|number|null|bool  нужн[ую|ые] переменные [ую|ые]
   */
  public function ngPost($var=null) {

    //сдесь будут храниться переменные
    static $vars;

    //получаем переменные
    if(!isset($vars)) {
      $vars = json_decode($this->request->getContent(), true);
      if(empty($vars)) $vars = [];
    }

    //если нужно получть все переменные
    if(!isset($var)) return $vars;

    //если нужно получить конкретную переменную
    return (isset($vars[$var])) ? $vars[$var] : null;

  }


  /**
   * Создает WebSocket-сервер и возвращает порт
   * @method websocket
   * @param  string|object   $ws    Модель, унаследованная от \fw\core\Ws
   * @param  array            $vars  Переменныее, которые будут переданы в объект модели
   * @return [type] @void
   */
  protected function websocket($ws, array $vars=[]) {


    //если передано название модели без неймспейсов - добавляем
    if(!strpos($ws, '\\')) {
      $ws = '\\'.MOD.'\\models\\'.$this->__name.'\\'.$ws;
    }

    //проверяем чтобы класс модели существовал
    if(!class_exists($ws)) {
      Log::err("Контроллер {$this->__name}: не найдена WS-модель $ws");
      return false;
    }

    $path = MOD.'/'.$this->__name.'/'.$this->__action;

    //стартуем сервер
    $ws = new $ws($path, $vars);
    $ws -> run();

  }


  /**
   * Страница ошибки
   * @method _error
   * @param  integer $code Код ошибки
   * @param  string  $msg  Текст ошибки
   * @return @void
   */
  protected function _error($code=404, $msg=null) {
    App::err($code, $msg);
  }


}
