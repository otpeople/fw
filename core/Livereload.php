<?php

namespace fw\core;

// TODO исправить ошибку, когда пользователь еще не авторизовался
class Livereload {

  public static function addClass($class) {
    if(!LIVERELOAD) return false;
    if(class_exists($class)) {
      self::$files[] = (new \ReflectionClass($class)) -> getFileName();
    }
  }


  public static function addFile($file) {
    if(!LIVERELOAD) return false;
    if(file_exists($file) && !is_dir($file)) {
      self::$files[] = $file;
    }
  }


  public static function addAssets(...$assets) {
    if(!LIVERELOAD) return false;
    self::$assets[] = $assets;
  }


  public static $files = [

  ];

  public static $assets = [

  ];



}
