<?php

namespace fw\core;

class Angular {

  public static function init(...$args) {
    return new self(...$args);
  }





  protected $rule;
  public $table;
  protected $mainTable;
  protected $data;
  public $ws;
  public $name;
  public $conn;
  public $time;

  public function __construct($table, $rule) {


    //доступ к данным
    $this->rule = $rule;

    //главная таблица для получения данных
    $this->mainTable = $table;
    $this->mainTable -> index('id');


    //таблица для выполнения одноразовых опереция
    $this->table = clone $table;
    $this->table -> clear();




  }



  //получить данные из таблицы
  public function getFromTable() {
    $this->data = $this->mainTable -> get();
  }


  //получить все данные
  public function onGetData($conn, $data=null) {
    $this->send($conn, 'getData', array_values($this->data));
  }


  //сохранить один элемент
  public function onSaveOne($conn, $data) {


    //если обновлять запрешено
    if(strpos($this->rule, 'u') === false) {
      return $this->ws->error("Попытка изменить {$this->name}, хотя это запрещено");
    }

    //Log::debug($data);
    $_from = $this->ws->path;
    if(isset($this->conn)) {
      $_from = $_from.=':'.$this->conn->resourceId;
    }


    $id   = $data['id'];
    $data = $data['data'];
    $data['_from'] = $_from;

    //сохраняем в БД
    if($this->table->clear()->id('id', $id) -> update($data)) {

      $this->time = time();


      //если успешно - сохраняем в $this->data
      foreach($data as $prop => $val) {
        $this->data[$id][$prop] = $val;
      }

      //и отправляем всем другим соединениям
      foreach($this->ws->clients as $client) {
        if($client != $conn) {
          $this->send($client, 'updateOne', ['id' => $id, 'data' => $data]);
        }
      }


    } else {
      //если ну удалось сохранить
      $this->send($conn, "errorOne", ['id' => $id, 'data' => $this->table->error]);
    }


  }


  //удалить один элемент
  public function onDeleteOne($conn, $id) {

    //если удалять запрешено
    if(strpos($this->rule, 'd') === false) {
      return $this->ws->error("Попытка удалить {$this->name}, хотя это запрещено");
    }


    //проверям, чтобы он был в $this->data
    if(!isset($this->data[$id])) {
      return false;
    }

    //удаляем из БД
    $this->table->clear()->is('id', $id) -> delete($this->ws->path);

    $this -> updateAllData();

    return false;


    //удаляем из $this->data
    unset($this->data[$id]);

    //говрим всем пользователям что элемент удален
    foreach($this->ws->clients as $client) {
      $this->send($client, 'deleted', $id);
    }

  }



  //добавить новый элемент
  public function onAddOne($conn, $data, $callbackId) {

    //если обновлять запрешено
    if(strpos($this->rule, 'c') === false) {
      return $this->ws->error("Попытка добавить {$this->name}, хотя это запрещено");
    }

    $_from = $this->ws->path;
    if(isset($this->conn)) {
      $_from = $_from.=':'.$this->conn->resourceId;
    }

    $data['_from'] = $_from;

    //пробуем сохранить
    $id = $this->table->clear()->insert($data);

    //если сохранение прошло успешно
    if($id) {

      //обновляем данные
      $this->updateAllData();

      //выполняем нужный колбек на клиенте
      $this->send($conn, 'callback', $callbackId, $id);

    //если не удалось сохранить
    } else {

      $this->send($conn, 'addError', $this->table->error);

    }



  }







  //обновляет все данные на сервере и на клиентах
  public function updateAllData() {

    $this->getFromTable();

    if(isset($this->conn)) {
     $this->onGetData($this->conn);
    } else {
      foreach($this->ws->clients as $client) {
        $this->onGetData($client);
      }
    }

  }








  protected function send($conn, $type, ...$data) {


    $this->ws->send($conn, 'angular', $this->name, $type, $data);
  }


}
