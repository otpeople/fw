<?php

namespace fw\core;

use Jenssegers\Date\Date as JDate;

class Date extends JDate {

  //дата начала любой пятинедельки
  const FIVE = '2015-10-22';

  //дата начала недели (по умолчанию - четверг)
  const WEEK = 4;

  //переводит в начало пятинедельки
  public function fiveStart() {

    //создаем переменную для расчетов
    $date = new Date($this->format('Y-m-d'));

    //чтобы дата была больше константы
    while($date->format('Y-m-d') < self::FIVE) {
      $date -> add('35 days');
    }

    //чтобы дата была меньше чем константа+35
    $plus35 = new Date(self::FIVE);
    $plus35 = $plus35 -> add('35 days') -> format('Y-m-d');
    while($date->format('Y-m-d') >= $plus35) {
      $date -> sub('35 days');
    }

    //считаем, сколько дней нужно отнять, чтобы получить начало
    $x = 0;
    while($date->format('Y-m-d') != self::FIVE) {
      $date->sub('1 day'); $x++;
    }

    //отнимаем нужное кол-во дней
    if($x != 0) {
      $this -> sub("$x days");
    }

    return $this;
  }

  //переводит в конце пятинедельки
  public function fiveEnd() {
    $this -> fiveStart();
    $this -> add('34 days');
    return $this;
  }

  //переводит в начало недели
  public function weekStart() {
    $day = $this -> format('w');
      if($day == 0) { $this -> sub('3 days'); }
    elseif($day == 1) { $this -> sub('4 days'); }
    elseif($day == 2) { $this -> sub('5 days'); }
    elseif($day == 3) { $this -> sub('6 days'); }
    elseif($day == 5) { $this -> sub('1 days'); }
    elseif($day == 6) { $this -> sub('2 days'); }
    return $this;
  }

  //переводит в конец недели
  public function weekEnd() {
    $this -> weekStart();
    $this -> add('6 days');
    return $this;
  }


  //переводит в начало месяца
  public function monthStart() {
    $this -> day = 1;
    return $this;
  }


  //переводит в конец месяца
  public function monthEnd() {

    $m = $this->format('m');
    if(in_array($m, [1,3,5,7,8,10,12])) {
      $this -> day = 31;
    } elseif(in_array($m, [4,6,9,11])) {
      $this -> day = 30;
    } else {
      $this -> day = 28;
    }
    return $this;
  }


}
