<?php

namespace fw\core;


use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

use fw\help\Websocket;
use fw\help\Func;
use fw\core\Log;
use fw\core\App;

class Ws {

  public $angular = [];
  protected $vars;
  public $personal = false; // персональный под каждого клиента

  public function __construct($path, $vars=[]) {

    //парсим $path
    $path = explode('/', $path);
    if(sizeof($path) == 2) {
      if(defined('MOD')) {
        array_unshift($path, MOD);
      } else {
        $path = implode('/', $path);
        Log::err("При запуске WS-сервера не найден модуль ($path)"); exit;
      }
    } elseif(sizeof($path) != 3) {
      $path = implode('/', $path);
      Log::err("При запуске WS-сервера неверно указан путь ($path)");
    }
    $this->path = implode('/', $path);


    //получаем порт
    $this->_port = $this->_getPort();

    foreach($vars as $var => $val) {
      $this->$var = $val;
    }


    }



  //вызывает метод работающего класса вебсокета
  public function push($method) {

    $port = &$this->_port;

    $client = new \WebSocket\Client("ws://0.0.0.0:$port/$method");

    try {
      $response = $client->receive();
    } catch (\Throwable $t) {
      $response = false;
    }

    return $response;

  }


  //запускает ws-сервер
  public function run($hard=false) {

    //если php запущен не из консоли - запускаем WS в бекграунде и даем ответ
    if(php_sapi_name() !== 'cli') {

      //составляем команду
      $dir = DIR;
      $path = $this->path;
      $php = App::getConfig('local', 'php');
      if(!$php) $php = 'php';
      $cmd = "$php $dir/fwc.php websocket $path -n";
      //Log::debug($cmd);
      exec($cmd);

      //отдаем ответ
      App::$response->setContent(json_encode(['port' => $this->_port]));
      App::$response->headers->set('Content-Type', 'application/json');
      App::$response->send();
      exit;

    }


    //если нужно в любом случае перезапустить сервер
    if($hard) {
      $this->hardReset();
    }


    //проверяем, может соединения уже запущено
    if($this->push('ping') === 'OK') {
      // Log::debug("Соединение уже запущено");
      exit;
    }



    //получае порт
    $port = &$this->_port;

    //закрываем старое соединение
    $this -> push("stop");

    //получаем объект контроллера (для проверки доступа)
    $path = explode('/', $this->path);
    $module = $path[0];
    $ctrl   = ucfirst($path[1]).'Controller';
    $method = $path[2];


    $ctrlName = "\\$module\\controllers\\$ctrl";
    if(!class_exists($ctrlName)) {
      $this->error("Не существует контроллера $ctrlName"); exit;
    }
    $ctrl = new $ctrlName();

    if(!method_exists($ctrl, $method)) {
      $this->error("У контроллера $ctrlName нет метода $method"); exit;
    }

    if(isset($ctrl->access)) {
      $access = $ctrl->access;
    } else {
      $access = null;
    }

    //удаляем лишние переменные, оставляем $access и $method
    unset($path, $module, $ctrl, $ctrlName);





    //вызываем метод start()
    if(method_exists($this, 'start')) {
      $this->start();
    }




    //стартуем сервер
    $server = IoServer::factory(
      new HttpServer(
        new WsServer(
          new Websocket($this, $access, $method)
        )
      ),
      $port
    );


    $path = $this->path;
    if($this->personal) $path = $path.":".Access::getUserId();


    //сообщение о старте сервера
    Log::info("Websocket server $path on port $port start!");

    //сообщение о прекращении сервера
    register_shutdown_function(function() use ($path, $port) {
      Log::info("Websocket server $path on port $port stop!");
    });



    //активируем вотчеры
    $server->loop->addPeriodicTimer(2, function () {


      foreach($this->_watchers as $id => $watcher) {

        //смотрим время последнего изменения таблицы
        $time = $watcher["table"]->one('_change');
        if(empty($time)) { continue; }

        //если время изменилось
        if($time > $watcher["time"]) {

          //сохраянем новое время
          $this->_watchers[$id]["time"] = $time;

          //выполняем все методы
          foreach($watcher['methods'] as $method) {


            // если строка - значит метод
            if(is_string($method)) {
              //если это метод для ангулара
              if(strpos($method, 'angular=') === 0) {
                $var = str_replace('angular=', '', $method);
                if(isset($this->angular[$var])) {
                  $this->angular[$var]->updateAllData();
                }

              //если это обычный метод
              } elseif(method_exists($this, $method)) {
                $this->$method();
              }
            }

            elseif(is_callable($method)) {
              $method();
            }

          }

        }

      }

    });


    //каждую секунду
    $server->loop->addPeriodicTimer(1, function () {

      //увеличиваем счетчик на 1
      $this->_everyTime++;

      //проходимся по всем запланированным действиям
      foreach($this->_every as $one) {
        if($this->_everyTime % $one["time"] == 0 || $this->_everyTime == 1) {
          $one['callback']();
        }
      }


    });



    $server->run();

  }



  protected $_everyTime = 0;
  protected $_every = [];
  public function interval($callback, $time) {
    $this->_every[] = [
      'time' => $time,
      'callback' => $callback,
    ];
  }



  //получить время изменения файла модели (для перезапуска сервера)
  public function _getTime() {
    return 123;
    $file = get_called_class();
    $file = DIR.'/'.str_replace('\\', '/', $file).'.php';
    if(!file_exists($file)) {
      Log::emerg("Не найден файл модели ($file)", __FILE__, __LINE__); exit;
    }

    return filemtime($file);
  }



  //получает порт
  private function _getPort() {


    $path = $this->path;

    //если персональный
    if($this->personal) {
      $path = $path.":".Access::getUserId();
    }

    //файл, где будут храниться порты
    $file = DIR . '/config/ports.json';


    if(file_exists($file)) {
      $content = json_decode(file_get_contents($file), true);
    } else {
      exec("touch $file");
      $content = [];
    }


    //если есть старый номер порта
    if(isset($content[$path])) {
      $port = $content[$path];
    } else {
      $port = $this -> _generatePort();
    }


    $content[$path] = $port;


    //сохраняем файл
    file_put_contents($file, json_encode($content, JSON_PRETTY_PRINT));


    return $port;

  }

  //возвращает случайный свободный tcp-порт
  private function _generatePort() {
    $success = false;
    $host = '0.0.0.0';
    while(!$success) {
      $port = rand(2000, 50000);
      $connection = @fsockopen($host, $port);
      if (!is_resource($connection)) {
        return $port;
      }
    }
  }



  //отправляет данные клиенту
  public function send($conn, $type, ...$data) {

    //смотрим, кому нужно отправить
    $conn -> send(json_encode([$type, $data]));
  }



  //выводит в logger сообщения об ошибки с имененм текущего сокета
  public function error($msg) {
    Log::err("WS-сервер {$this->path}: $msg");
  }



  public $_watchers = [];


  //выключает сервер, убивая порт
  public function hardReset() {


    //смотрим процессы, запущенные на этом порту
    exec('lsof -i :'.$this->_port, $pids);
    unset($pids[0]);
    foreach($pids as $pid) {

      $pid = array_values(
        array_diff(
          explode(" ", $pid),
        [''])
      );

      //останавливаем процесс
      if($pid[0] == 'php' || $pid[0] == 'php-cgi' || $pid[0] == 'httpd') {
        exec("kill ".$pid[1]. " > /dev/null 2>&1");
        //Log::debug("kill ".$pid[1]. " > /dev/null 2>&1");
      }
    }


  }






  //добавляем таблицу в отслеживаемые
  protected function watch($table, $method, $conn=null) {

    $_from = $this->path;
    if(isset($conn)) {
      $_from = $_from.':'.$conn->resourceId;
    }

    $table = clone $table;

    //определяем хеш таблицы
    $tableHash = md5(json_encode($table->sql).$table->class);
    if(isset($conn)) $tableHash.= $conn->resourceId;


    //если не существует вотчера для этой таблицы - создаем
    if(!isset($this->_watchers[$tableHash])) {

      //получаем время изменения таблицы
      $table -> order('_change DESC');
      $table -> not('_from', $_from);
      $table -> deleted = true;
      $time = $table->one('_change');
      if(empty($time)) $time = 0;
      //$time = 0;

      $array = [
        'table'   => $table,
        'methods'   => [],
        'time'     => $time,
      ];

      if(isset($conn)) $array['conn'] = &$conn;

      //добавляем в вотчеры
      $this->_watchers[$tableHash] = $array;


    }

    //добавляем метод в вотчер
    $this->_watchers[$tableHash]['methods'][] = $method;

  }


  protected function sync($var, $angular, $conn=null) {

    //генерируем переменную
    $var1 = $var;
    if(isset($conn)) $var1 .= $conn->resourceId;
    $var1 = md5($var1);


    //добавляем в массив переменных ангуляра
    if(isset($conn)) $angular->conn = $conn;

    $this->angular[$var1] = $angular;
    $this->angular[$var1] -> getFromTable();
    $this->angular[$var1] -> ws = &$this;
    $this->angular[$var1] -> name = $var;

    //делаем первое получение/обновление данных
    if(isset($conn)) {
      $this->angular[$var1] -> updateAllData();
    }

    //добавляем вотчер
    $this->watch($angular->table, 'angular='.$var1, $conn);

  }


}
