<?php

namespace fw\core;

class Db {

    //тут хранится подключение к базе данных
  public static $db;
  private static $dbname;

  public static $tables = [];


  public $affected_rows;    //кол-во затронутых строк


  //список всех таблиц со временем их изменения
  public static function getTables() {
    self::connect();

    $tables = [];
    $query = Db::query("SHOW TABLE STATUS FROM `".self::$dbname."`");
    while($row = mysqli_fetch_assoc($query)) {
      $tables[$row["Name"]] = $row["Comment"];
    }

    return $tables;
  }


    //подключается к базе данных, если подключение не создано
  public static function connect() {

    if(!isset(self::$db)) {

       /*  Устанавливаем соединения с базой данных  */
      $db = require(DIR."/config/db.php");

      try {
        self::$db = mysqli_connect($db["host"],$db["user"],$db["pass"],$db["name"]);
        self::$db -> query("SET CHARACTER SET utf8");
        self::$dbname = $db["name"];
      } catch (\Exception $e) {
        Log::emerg('Ошибка соединения с БД'); App::err(500);
      }




    }
  }

  public static function query($sql) {
    self::connect();

    //$sql = str_replace(["\n", "\t"], '', $sql);

    //print_r($sql);

    try {
      return self::$db -> query($sql);
    } catch (\Exception $e) {
      Log::emerg('Ошибка соединения с БД'); App::err(500);
    }


  }

}
