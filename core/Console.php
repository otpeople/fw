<?php
namespace fw\core;

use fw\core\Log;

class Console {

  public $dir;

  public function __construct() {

    //директория приложения
    $this -> dir = DIR;

    //директория фреймворка
    if(!defined("FW")) {
      define("FW", dirname(__DIR__));
    }



  }

  protected function progress($done, $total, $size=30) {

      static $start_time;

      // if we go over our bound, just ignore it
      if($done > $total) return;

      if(empty($start_time)) $start_time=time();
      $now = time();

      $perc=(double)($done/$total);

      $bar=floor($perc*$size);

      $status_bar="\r[";
      $status_bar.=str_repeat("=", $bar);
      if($bar<$size){
          $status_bar.=">";
          $status_bar.=str_repeat(" ", $size-$bar);
      } else {
          $status_bar.="=";
      }

      $disp=number_format($perc*100, 0);

      $status_bar.="] $disp%  $done/$total";

      if($done == 0) {
        $rate = 0;
      } else {
        $rate = ($now-$start_time)/$done;
      }
      $left = $total - $done;
      $eta = round($rate * $left, 2);

      $elapsed = $now - $start_time;

      $status_bar.= " remaining: ".number_format($eta)." sec.  elapsed: ".number_format($elapsed)." sec.";

      echo "$status_bar  ";

      flush();

      // when done, send a newline
      if($done == $total) {
        $start_time = null;
        echo "\n";
      }

  }



  //получить строку от пользователя
  public function stdin($str, $def=null, $many=false) {

    //если не указано значение по умолчанию
    if(!isset($def)) {
      echo $str.': ';
      $var = trim(fgets(STDIN));
      if(empty($var)) {
        return $this->stdin($str);
      } else {
        return $var;
      }
    }

    //если заданы возможные значения
    elseif(is_array($def)) {

      //если можно выбирать несколько - то добавляем all (выбрать всех)
      if($many) {
        array_unshift($def, 'all');
      }

      echo $str.":";
      foreach($def as $id => $val) {
        echo "\n\t\t$id -> $val";
      }


      $success = false;
      while(!$success) {
        echo "\nYour choose: ";
        $vars = explode(' ', trim(fgets(STDIN)));

        //если нельзя брать несколько
        if($many == false) {
          //если всего одни - то все нормально
          if(sizeof($vars) == 1) {
            $var = $vars[0];
            if(isset($def[$var])) {
              return $def[$var];
            } else {
              echo "\nTry again";
            }
          } else {
            echo "You can pick only 1";
            $success = false;
          }
        } else {
          //если можно выбирать несколько
          if(!empty($vars)) {

            //если выбраны все
            if(in_array(0, $vars)) {
              array_shift($def);
              return $def;
            }
            $arr = [];
            foreach($vars as $v) {
              if(isset($def[$v])) {
                $arr[] = $def[$v];
              }
            }
            if(!empty($arr)) {
              return $arr;
            } else {

            }

          }
        }

        $success = false;

      }

      return $def[$var];
    }


    //если указано значение по умолчанию
    else {
      echo $str.' ['.$def.']: ';
      $var = trim(fgets(STDIN));
      if(empty($var)) {
        return $def;
      } else {
        return $var;
      }
    }
  }


  //выполнить системную команду
  public function exec($command, $print=true) {

    if($print == false) { $command .= ' > /dev/null 2>&1'; }

    \exec($command, $return, $status);

    return ($status == 0) ? $return : false;
  }


  //получить нужный модуль приложения
  public function getModule($name=null) {

    //считываем composer.json
    $file = $this->dir.'/composer.json';
    $array = json_decode(file_get_contents($file), true);
    $apps = array_keys($array['autoload']['psr-4']);
    $apps = array_map(function($n) {
      return str_replace("\\", '', $n);
    }, $apps);

    $apps = array_values(array_diff($apps, ['console','fw']));

    //если модулей вообще нет
    if(empty($apps)) {
      exit("You don't have any modules\n");
    }

    //если указанный модуль существует - сразу возвращаем его
    if(in_array($name, $apps)) {
      return $name;
    }

    //иначе спрашиваем какой модуль нужен
    return $this->stdin('Choose module', $apps);

  }


  //записать файл из шаблона
  protected function template($file, $template, $vars=[]) {

    $fp = fopen($file, 'w');


    $template = dirname(__DIR__).'/console/templates/'.$template;


    if(!file_exists($template)) { return false; }

    $content = file_get_contents($template);
    $keys = [];
    $vals = [];
    foreach($vars as $id => $val) {
      $keys[] = "{{$id}}";
      $vals[] = $val;
    }

    $content = str_replace($keys, $vals, $content);

    fwrite($fp, $content);

    fclose($fp);

  }


  public static function getFwDir() {
    return dirname(__DIR__);
  }



  //выводит информацию цветом
  public function color($str, $color=null, $bgColor=null) {

    $colored_string = "";

    // Check if given foreground color found
    if(isset($this->colors[$color])) {
      $colored_string .= "\033[" . $this->colors[$color] . "m";
    }
    // Check if given background color found
    if(isset($this->bgColors[$bgColor])) {
      $colored_string .= "\033[" . $this->bgColors[$bgColor] . "m";
    }

    // Add string and end coloring
    return $colored_string .=  $str . "\033[0m";

  }


  //главная функция, запускающая консольную утилиту
  public static function __init($argv, $dir) {


    //если переданы аргументы - пытаемся найти класс, который обработает команду
    $class = explode('/', $argv[1]);
    $action = (isset($class[1])) ? $class[1] : 'index';
    $class = ucfirst($class[0]);

    //нужные константы
    define("DIR", $dir);
    define("WEB", $dir.'/public');
    define("DEV", true);
    define("LIVERELOAD", false);
    define("FW", dirname(__DIR__));

    $classApp = "\\console\\$class";
    $classFw  = "\\fw\\console\\$class";

    if(class_exists($classApp)) {
      $class = new $classApp($dir);
    } elseif(class_exists($classFw)) {
      $class = new $classFw($dir);
    } else {
      echo "command ".$argv[1]." not found\n"; exit;
    }

    //определяет флаги
    foreach($argv as $id => $arg) {
      if($arg{0} === '-') {
        for($i=1;$i<strlen($arg);$i++) {
          $class->__flags[] = $arg[$i];
        }
        unset($argv[$id]);
      }
    }

    //если есть флаг 'n' - значит нужно запустить скрипт в фоновом режиме
    if($class->flag('n')) {

      $flags = [];
      foreach($class->__flags as $flag) {
        if($flag != 'n') $flags[] = $flag;
      }
      if(!empty($flags)) {
        $flags = ' -'.implode('', $flags);
      } else {
        $flags = '';
      }

      //получаем php
      $php = App::getConfig('local', 'php');
      $php = ($php) ? $php : 'php';


      //собираем условия
      $dir = DIR;
      $cli = "$dir/fwc.php ".implode(' ', array_slice($argv, 1)).$flags;
      $cli = "nohup $php ".$cli.' > /dev/null 2>&1 &';
      //$cli = "$php -q ".$cli.' < /dev/null > /dev/null &';
      //Log::debug($cli);
      exec($cli);
      return false;

    }



    if(method_exists($class, $action)) {
      $class -> $action(...array_slice($argv, 2));
    } else {
      echo "command ".$argv[1]." not found\n";
    }

  }


  private $colors = [
    'black' => '0;30',
    'dark_gray' => '1;30',
    'blue' => '0;34',
    'light_blue' => '1;34',
    'green' => '0;32',
    'light_green' => '1;32',
    'cyan' => '0;36',
    'light_cyan' => '1;36',
    'red' => '0;31',
    'light_red' => '1;31',
    'purple' => '0;35',
    'light_purple' => '1;35',
    'brown' => '0;33',
    'yellow' => '1;33',
    'light_gray' => '0;37',
    'white' => '1;37',
  ];

  private $bgColors = [
    'black' => '40',
    'red' => '41',
    'green' => '42',
    'yellow' => '43',
    'blue' => '44',
    'magenta' => '45',
    'cyan' => '46',
    'light_gray' => '47',
  ];





  //возвращает true если есть такой флаг
  public function flag($flag) {
    return (in_array($flag, $this->__flags)) ? true : false;
  }
  public $__flags = [];


}
