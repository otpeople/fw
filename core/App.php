<?php
namespace fw\core;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use fw\helpers\Translit;
use fw\core\Access;
/**
 *   Инициализатор приложения
 *
 *   Определяет модуль/контроллер/экшен и запускает приложение
 */

class App {

  /**
   * Время старта скрипта в microtime
   */
  private static $_time;

  /**
   * HttpFoundateion Request
   * @var object
   */
  public static $request;

  /**
   * HttpFoundateion Response
   * @var object
   */
  public static $response;

  /**
   * Получить время работы скрипта
   *
   * @return float время работы скрипта
   */
  public static function time() {
    return microtime(true) -  self::$_time;
  }


  /**
   * Запускает приложение
   * @param  array $doms Массив в формате "Домен/путь" => "модуль/контроллер/экшен"
   * @return @void
   */
  public static function init($doms) {

    //отмечаем время старта скрипта
    self::$_time = microtime(true);

    //проверка на браузеры
    self::_oldBrowser();

    //инициализируем HttpFoundation
    self::$request = Request::createFromGlobals();
    self::$response = new Response();

    //получаем из URL
    //0-модуль 1-контроллер 2-экшен 3-переменные
    $app = self::urlParser(
      $doms,
      self::$request->server->get('HTTP_HOST'),
      self::$request->server->get('REQUEST_URI')
    );

    //если не обнаружен путь
    if($app === false) self::err(404);

    //определяем нужные константы
    define("MOD", $app[0]);                              //название модуля
    define("APP", $app[0]);                              //название приложения(модуля)
    define("DIR", dirname(self::$request->server->get('DOCUMENT_ROOT')));   //директория приложения
    define("WEB", self::$request->server->get('DOCUMENT_ROOT'));            //web-директория
    define("FW", dirname(__DIR__));                      //директория фреймворка


    //liverealod
    if(self::getConfig('local', 'livereload')) {
      define("LIVERELOAD", true);
    } else {
      define("LIVERELOAD", false);
    }


    // если приложение отключено
    if(file_exists(DIR.'/off.json')) {
      // получаем содержимое файла
      $content = json_decode(file_get_contents(DIR.'/off.json'), true);
      // если приложение действительно выключено
      if(isset($content['off']) && $content['off'] === true) {
        App::run('fw', 'fw', 'off', [$content['title'], $content['message']]);
        exit;
      }


    }




    //проверяем доступность модуля

    //если файл access.php существует - проверям есть ли доступ
    $accessFile = DIR."/".MOD."/access.php";
    if(file_exists($accessFile)) {

      //если нет доступа - запускаем авторизацию
      $data = require($accessFile);
      if(!Access::module($data)) {
        APP::run('fw','login','index',[$data["table"], [$data['name'], $data['pass']]]); exit;
      }

    }

    //если файла access.php нет - значит весь модуль доступен



    //если обращение к контроллеру фреймворка
    if($app[1] == 'fw') {
      self::run('fw', 'fw', $app[2], $app[3]);
    } else {
      self::run($app[0], $app[1], $app[2], $app[3]);
    }

  }


  /**
   * Запускает нужный контроллер/экшен
   * @param  string $from       Модуль
   * @param  string $controller Контроллер
   * @param  string $action     Экшен
   * @param  array  $vars       Переменные
   * @return @void
   */
  public static function run($from, $controller, $action="index", $vars=[]) {

    $controller = "\\".$from."\\controllers\\".ucfirst($controller).'Controller';

    //если класс контроллера найден
    if(class_exists($controller)) {
      $controller = new $controller();
      $controller->request = &self::$request;
      $controller->response = &self::$response;
      $content = $controller->__action($action, $vars);
      if(!empty($content)) {
        self::$response->setContent($content);
      }
      self::$response->send();
    } else {
      self::err(404);
    }

  }


  /**
   * Распарсивает URL
   * @param  array  $router Массив в формате "домен/путь" => "модуль/контроллер/экшен"
   * @param  string $host   Доменное имя
   * @param  string $path   URI
   * @return array          массив [модуль, контроллер, экшен]
   */
  public static function urlParser($router, $host, $path) {

    //замена доменных зон для локального сервера
    $pos = strpos($host, '.loc');
    if($pos) { $host = substr($host, 0, $pos); }
    unset($pos);

    //убираем хеши и переменные из path
    $pos = strpos($path, "?"); if($pos) { $path = substr($path, 0, $pos); }
    $pos = strpos($path, "#"); if($pos) { $path = substr($path, 0, $pos); }
    if($pos !== false) { $url = substr($path, 0, $pos); }
    unset($pos);

    //переводим $path в массив
    if(is_string($path)) {
      if($path == '/') {
        $path = [];
      } else {
        $path = explode('/', substr($path, 1));
      }
    }

    //определяем правило для текущего url из router
    $p = $path;
    $path = [];
    while(sizeof($p) != 0) {
      $index = $host.'/'.implode('/', $p);
      if(isset($router[$index])) {
        $dist = $router[$index];
        break;
      }
      unset($index);
      array_unshift($path, array_pop($p));
    }
    if(!isset($dist)) {
      if(isset($router[$host])) {
        $dist = $router[$host];
      } else {
        return false;
      }
    }


    //определяем модуль/контроллер/экшен
    $dist = explode('/', $dist);
    for($i=1; $i<3; $i++) {


      if(!isset($dist[$i])) {
        $dist[$i] = (isset($path[0])) ? array_shift($path) : 'index';
      }
    }

    //добавляем переменные
    $dist[] = $path;

    return $dist;
  }


  /**
   * Получить нужные настройки приложения
   * @param  string $name       Имя файла конфига (config/$name.php)
   * @param  string $val        Свойство, которое нужно получить
   * @return string|array|null  Значение нужного свойства
   */
  public static function getConfig($name, $val=null) {

    static $config = [];

    //получаем конфиг из файла
    if(!isset($config[$name])) {
      $file = DIR.'/config/'.$name.'.php';
      if(file_exists($file)) {
        $config[$name] = require($file);
      } else {
        $config[$name] = [];
      }
    }

    if(!isset($val)) {
      return $config[$name];
    } else {
      if(isset($config[$name][$val])) {
        return $config[$name][$val];
      } else {
        return null;
      }
    }


  }


  /**
   * Выводит страницу с ошибкой
   * @param  integer $code Код ошибки
   * @param  string  $msg  Сообщение ошибки
   * @return @void
   *
   */
  public static function err($code=404, $msg=null) {
    //App::run("Error",$code);
    //self::$response->setContent("");
    self::$response->setStatusCode($code, $msg);
    self::$response->send();
    exit;
  }


  /**
   * Перенаправление для старых браузеров
   * @return @void
   *
   */
  private static function _oldBrowser() {
    if(preg_match('/(?i)msie [1-8]/',$_SERVER['HTTP_USER_AGENT'])) {
      header("Location: http://browser-update.org/ru/update.html");
      exit;
    }
  }


}
