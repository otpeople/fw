<?php

namespace fw\models\pult;

use fw\core\Access;
use fw\core\Log;

class TabsModel {

  //получить вкладки для пульта
  public static function get() {

    $tabs = [];
    //return [];
    //определяем директорию контроллеров
    $dir = DIR.'/'.MOD.'/controllers';
    foreach(scandir($dir) as $file) {
      if(strpos($file, 'Controller.php')) {
        $name = str_replace('.php', '', $file);
        $class = "\\pult\\controllers\\".$name;
        $ctrl = new $class();

        if(isset($ctrl->tabs)) {

          //проходимся по каждой вкладке
          foreach($ctrl->tabs as $action => $tab) {

            //ссылка
            $tab['href'] = '/'.lcfirst(str_replace('Controller','',$name)).'/'.$action;

            //можно ли закрывать по умолчанию
            if(!isset($tab["close"])) {
              $tab['close'] = true;
            }

            //индекс по умолчанию
            if(!isset($tab['index'])) {
              $tab['index'] = 0;
            }


            //тип по умолчанию
            if(!isset($tab["type"])) {
              $tab['type'] = 'tab';
            }

            // проверка доступа
            if(isset($ctrl->access)) {
              if(Access::to($ctrl->access, $tab["href"])) {
                $tabs[] = $tab;
              }
            } else {
              $tabs[] = $tab;
            }

          }
        }
      }
    }

    //сортируем табы по индексу/наименованию
    uasort($tabs, function($a, $b) {
      $k = $b['index'] <=> $a['index'];
      if($k === 0) {
        $k = $a['title'] <=> $b['title'];
      }
      return $k;
    });

    return array_values($tabs);

  }


}
