<?php

namespace fw\models\pult;

use fw\core\Ws;
use fw\core\Log;
use fw\core\Assets;

class PultWs extends Ws {

	//при старте ws-сервера
	public function start() {

		//получаем всех возможных участников чата
		$this -> getAllChatUsers();

		//таблицы
		$users 	= $this->chatUsers;
		$groups = $this->chatGroups;
		$rooms 	= $this->chatRooms;

		//создаем вотчеры
		$this->watch($users::init()->not('_from','not-chat'), 'updateAllChatUsers');
		$this->watch($groups::init(), 'updateAllChatUsers');
		$this->watch($rooms::init(), 'updateAllChatUsers');

	}

	//открытие соединения
	public function open($conn) {

		$messages = $this->chatMessages;

		$conn->chat	= new $messages();

		//отправляем пользователю инфо о всех остальных пользователях
		$this->onGetChatUsers($conn);

		//обновляем у всех статус этого пользователя
		$this->updateUserStatus($conn->userId);

	}

	//закрытие соединения
	public function close($conn) {

		//обновляем у всех статус этого пользователя
		$this->updateUserStatus($conn->userId);
	}



	/**************
	ЭКШЕНЫ ДЛЯ ЧАТА
	***************/


	//отправляет пользователю информацию о всех других пользователях чата
	public function onGetChatUsers($conn, $data=null) {

		$all = $this->all;

		$rooms = [];

		//удаляем все комнаты, в которых пользователь не состоит
		foreach($all["rooms"] as $id => $room) {
			if(!in_array($conn->userId, $room["users"])) {
				unset($all["rooms"][$id]);
			} else {
				$rooms[] = 'room_'.$room["id"];
			}
		}
		$all["rooms"] = array_values($all["rooms"]);


		//получаем все непрочитанные сообщения от пользователей
		$messages = $conn->chat
			-> clear()
			-> not('read', $conn->userId)
			-> is('to', $conn->userId)
			-> group('from')
			-> arr('from', 'amount.sum');

		//получаем все непрочитанные сообщения от комнат
		$messagesRoom = $conn->chat
			-> clear()
			-> not('read', $conn->userId)
			-> not('from', $conn->userId)
			-> is('chat_id', $rooms)
			-> group('chat_id')
			-> arr('chat_id', 'amount.sum');


		//$conn->chat -> log();


		//распределяем непрочитанные сообщения от комнат
		foreach($all["rooms"] as $rId => $room) {
			$chatId = 'room_'.$room["id"];
			if(isset($messagesRoom[$chatId])) {
				$all["rooms"][$rId]["new"] = $messagesRoom[$chatId];
			}
		}


		//распределяем непрочитанные сообщения по пользователям
		foreach($all["groups"] as $gid => $group) {
			foreach($group["users"] as $uid => $user) {

				//смотрим непрочитанные сообщения от этого пользователя
				if(isset($messages[$user["id"]])) {
					$all["groups"][$gid]["users"][$uid]["new"] = $messages[$user["id"]];
				}
				//смотрим статус для этого пользователя
				if(in_array($user["id"], $this->users)) {
					$all["groups"][$gid]["users"][$uid]['status'] = 1;
				} else {
					$all["groups"][$gid]["users"][$uid]['status'] = 0;
				}
			}
		}



		//отправляем клиенту
		$this -> send($conn, 'allChatUsers', $all);
		unset($all);
	}


	//когда пользователь прочитал сообщения
	public function onReadMessages($conn, $type, $id) {

			//список пользоватлеей (id), которым нужно будет разослать информацию о прочтении
			$users = [];

			//если прочитаны сообщения пользователя
			if($type == 'user') {

				//отмечаем что пользователь прочитал
				$conn->chat->clear()
					->is('chat_id', $conn->chat->chatIdUsers($id, $conn->userId))
					->not('read', $conn->userId)
					->is('to', $conn->userId)
					->update(['read' => [$conn->userId]]);

				//добавляем собеседника в список
				$users[] = $id;


			//если прочитано сообщение в комнате
			} elseif($type == 'room') {

				//получаем все сообщения
				$messages = $conn->chat->clear()
					->is('chat_id', $conn->chat->chatIdRoom($id))
					->not('read', $conn->userId)
					->not('from', $conn->userId)
					->get('id', 'read');

					//$conn->chat->log();


				//добавляем текещего пользователя к прочитавшим
				foreach($messages as $msg) {
					$msg["read"][] = $conn->userId;
					//Log::debug($msg["read"]);
					$conn->chat->clear()->is('id', $msg["id"]) -> update(['read' => $msg["read"]]);
				}

				//получаем всех пользователей комнаты
				foreach($this->all["rooms"] as $room) {
					if($room["id"] == $id) {
						$users = $room["users"];
					}
				}


			}


			//отправляем собеседнику что мы прочитали сообщение
			foreach($this->clients as $client) {
				if(in_array($client->userId, $users)) {
					$this -> send($client, 'readMessages', $type, $id, $conn->userId);
				}
			}

			//обновляем весь чат текущему пользователю
			$this->onGetChatUsers($conn);

			/*

			//отправляем другому пользователю, что все прочитано от этого пользователя
			foreach($this->clients as $client) {
				if($client->userId == $user) {
					$this -> send($client, 'readMessages', $conn->userId);
				}
			}

*/

	}


	//когда пользователь присылает новое сообщение
	public function onNewMessage($conn, $type, $id, $text) {


		//формируем массив для сохранения в БД
		$d = [
			'from' => $conn->userId,
			//'to'   => $toUser,
			'text' => $text,
			'date' => date('Y-m-d'),
			'time' => date('H:i:s'),
			'read' => 0,
			//'chat_id' => 'users_'.$this->getChatId($conn->userId, $toUser),
			'amount' => 1,
		];


		if($type == 'user') {
			$d["to"] = $id;
			$d["chat_id"] = $conn->chat->chatIdUsers($id, $conn->userId);
		} elseif($type == 'room') {
			$d["chat_id"] = $conn->chat->chatIdRoom($id);
		}

		//сохраняем в БД
		$success = $conn->chat
			-> clear()
			-> insert($d);

		//если успешно сохранили сообщение в базе
		if($success) {

			//отправляем себе же
			//$this -> send($conn, 'newMessage', $d);

			//формируем список подключенных клиентов, которым будем отправлять
			$users = [];

			$users[] = $conn->userId;

			//если оправляется одному пользователю
			if($type == 'user') {
				$users[] = $id;

			//если отправляется в комнате
			} elseif($type == 'room') {

				//смотрим всех участников этой группы
				$groupUsers = [];
				foreach($this->all["rooms"] as $room) {
					if ($room["id"] == $id) {
						foreach($room["users"] as $user) {
							$users[] = $user;
						}
					}
				}

			}



			//отправляем тому, кому и собирались отправлять
			foreach($this->clients as $client) {
				if(in_array($client->userId, $users)) {
					$this -> send($client, 'newMessage', $type, $id, $d); // ДОБАВИТЬ group/users - сделать чтобы переменные передавались как аргументы функции в js
				}

			}

		//если не удалось сохранить - пишем ему об этом
		} else {
			$this->send($conn, 'newMessageError', $d);
		}


	}


	//когда нужно прислать пользователю все сообщения
	public function onGetAllMessages($conn, $type, $id, $start=null) {

		//пользователь, сообщения с которым нам нужны
		$id = (int) $id;

		$conn->chat->clear()->fields('from','to','read','text','date','time','id');

		if($type == 'user') {
			$conn->chat->is('chat_id', $conn->chat->chatIdUsers($id, $conn->userId));
		} elseif($type == 'room') {
			$conn->chat->is('chat_id', $conn->chat->chatIdRoom($id));
		} else {
			return false;
		}


		//лимит сообщени
		$conn->chat->order('id DESC')->limit(50);

		//если передано сообщение
		if(isset($start)) $conn->chat->find('id', '<', $start);

		$this -> send($conn, 'allMessages', $type, $id, $conn->chat->get(), $start);

	}


	//когда пользователь набирает сообщение
	public function onTyping($conn, $type, $id, $no=false) {



		if($type == 'user') {
			foreach($this->clients as $client) {
				if($client->userId == $id) {
					$this->send($client, 'typing', $type, $conn->userId, $id, $no);
				}
			}
		} elseif($type == 'room') {

			//формируем список пользователей
			$users = [];
			foreach($this->all["rooms"] as $room) {
				if($room["id"] == $id) {
					foreach($room["users"] as $user) {
						if($user != $conn->userId) {
							$users[] = $user;
						}
					}
				}
			}

			//Log::debug($users);

			//рассылаем пользователям
			foreach($this->clients as $client) {
				if(in_array($client->userId, $users)) {
					$this->send($client, 'typing', $type, $conn->userId, $id, $no);
				}
			}


		}


	}









	/**********************
	БАЗОВЫЕ МЕТОДЫ ДЛЯ ЧАТА
	**********************/


	//обновляет статус пользователя и рассылает его всем остальным
	private function updateUserStatus($id) {
		if(in_array($id, $this->users)) {
			$status = 1;
		} else {
			$status = 0;
		}
		//отправляем изменение статуса всем кто в сети
		foreach($this->clients as $client) {
			$this -> send($client, 'updateUserStatus', ['user'=>$id, 'status'=>$status]);
		}

	}


	//формирует всех возможных участников
	private function getAllChatUsers() {

		$users  = $this->chatUsers;
		$groups = $this->chatGroups;
		$rooms  = $this->chatRooms;

		//получаем всех пользователей/группы/комнаты
		$users  = $users::init() -> get('id','title','chatGroup', 'chatRooms', 'photo');
		$groups = $groups::init() -> index('id') -> get('id','title', 'icon');
		$rooms  = $rooms::init() -> index('id') -> get('id', 'title', 'icon');

		//создаем в группах массив под пользователей
		foreach($groups as $id => $group) {
			$groups[$id]["users"] = [];
		}

		//создаем в комнатах массив под пользователей
		foreach($rooms as $id => $room) {
			$rooms[$id]["users"] = [];
		}

		//создаем группу "Не распределенные"
		$groups[0] = [
			'title' => 'Не распределенные',
			'id' => '0',
			'icon' => 'fa fa-exclamation-circle',
			'users' => [],
		];


		//по каждому пользователю
		foreach($users as $user) {

			//добавляем в группу
			$group = (int) $user["chatGroup"];
			if(isset($groups[$group])) {
				$groups[$group]["users"][] = [
					'id'	=> $user["id"],
					'title' => $user["title"],
					'photo' => Assets::img($user["photo"], [100,0]),
				];
			}

			//добавляем в комнаты
			foreach($user["chatRooms"] as $room) {
				if(isset($rooms[$room])) {
					$rooms[$room]["users"][] = $user["id"];
				}
			}

		}

		//удаляем пустые группы
		foreach($groups as $gId => $group) {
			if(empty($group["users"])) unset($groups[$gId]);
		}

		//удаляем пустые комнаты
		foreach($rooms as $id => $room) {
			if(empty($room["users"])) unset($rooms[$id]);
		}


		//если нет нераспределенных - удаляем эту группу
		if(empty($groups[0]["users"])) {
			unset($groups[0]);
		}

		//сохраняем группы и комнаты для отсылки пользователям
		$this->all = [
			'groups' => array_values($groups),
			'rooms'  => array_values($rooms),
		];


		unset($groups);
		unset($users);

	}


	//обновляет порядок пользователей и групп и отправляет их всем пользователям
	public function updateAllChatUsers() {


		$this -> getAllChatUsers();
		foreach($this->clients as $client) {
			$this -> onGetChatUsers($client, true);
		}

	}



	private $all; //тут будут храниться все возможные участники
	private $status = []; // статусы пользователей

	//получить id чата для двух пользователей
	private function getChatId($id1, $id2) {
		$data = [$id1, $id2];
		sort($data);
		return substr(md5(implode('_', $data)), 0, 20);
	}







	public function onSearch($conn, $data) {
		$data = (string) $data;

		//ищем товар
		$conn->goods
		->clear()
		->getReal()
		->where([
			'id' 	=> [ 'like' => '%'.$data.'%' ],
			'title' => [ 'like' => '%'.$data.'%' ],
			'artikul' => [ 'like' => '%'.$data.'%' ],
		])
		-> limit(5)
		-> fields('title', 'artikul', 'id');

		//ищем клиентов
		$conn->clients
		->clear()
		->where([
			'title' => [ 'like' => '%'.$data.'%' ],
			'id' 	=> [ 'like' => '%'.$data.'%' ],
			'phone' => [ 'like' => '%'.$data.'%' ],
			'tags' => [ 'like' => '%'.$data.'%' ],
		])
		//-> not('name','')
		-> limit(5)
		-> fields('title','phone','id', 'tags');

		//ищем контакты клиентов
		$conn->persons
		->clear()
		->where([
			'title' => ['like' => '%'.$data.'%'],
			'phone' => ['like' => '%'.$data.'%'],
			'email' => ['like' => '%'.$data.'%'],
		])
		->limit(5)
		-> fields('title', 'phone', 'post', 'email', 'client.title');


		$result = [
			'goods' 	=> $conn->goods	->get(),
			'clients' 	=> $conn->clients->get(),
			'persons'	=> $conn->persons->get(),
		];


		$this -> send($conn, 'search', $result);
	}



}
