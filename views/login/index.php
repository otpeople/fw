<?php
$this -> title = "Авторизация";
$this -> assets("bootstrap");

?>
<style>body {background-color: rgba(0, 0, 0, 0.8);}</style>
<br><br><br>
<div class="container">
  <div class="row">
    <div class="col-sm-6 col-sm-offset-3">

      <div class="panel panel-primary wow bounce">
        <div class="panel-heading">
          Пожалуйста, авторизуйтесь
        </div>
        <div class="panel-body" style="padding: 15px 20px">

          <? if(isset($error)): ?>
            <p class="text-danger"><?= $error ?></p>
          <? endif; ?>


          <form action="" method="post">

            <input type="hidden" name="token" value="<?= $token ?>">

            <? foreach($fields as $field): ?>
              <div class="form-group">
                <label class="control-label"><?= $field["title"] ?></label>
                <input type="<?= ($field["type"] == 'password') ? 'password' : 'text'  ?>" class="form-control" name="<?= $field["name"] ?>" required value="<?= (isset($def[$field["name"]])) ? $def[$field['name']] : '' ?>">
              </div>
            <? endforeach; ?>

            <div class="form-group" style="text-align: right">
              <button class="btn btn-primary" type="submit">Войти</button>
            </div>
          </form>

        </div>

      </div>

    </div>
  </div>
</div>
