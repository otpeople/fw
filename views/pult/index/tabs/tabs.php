<div ng-controller="tabsCtrl" id="tabs-ctrl">

  <!-- <div id="new-tab" ng-click="openMobileTab()">
    <i class="fa fa-bars"></i>
  </div> -->

  <div id="tabs">
  </div>

  <div ng-click="openTab(tab)"
    class="tab animated"
    ng-class="{open:tab.open, select:tab.select, show:showTabs}"
    ng-repeat="tab in tabs"
    style="top:{{tab.top}}px;left:{{tab.left}}px;width:{{tab.width}}px">
    <span class="icon">
      <i class="fa fa-{{tab.icon}}"></i>
      <i class="fa fa-refresh" ng-click="refreshTab(tab)"></i>
    </span>
    <span class="title">{{tab.title}}</span>
    <span class="pull-right" ng-click="closeTab(tab, $e)">
      <i class="fa fa-times"></i>
    </span>
    <div class="trans" ng-if="!tab.open" tooltip-right="tab.title"></div>
  </div>

  <div class="window"
    style="top: {{window.y}}px; left: {{window.x}}px;"
    ng-class="{opend: window.open, transition: window.transition}"
    ng-repeat="window in windows">
    <header ng-mousedown="startMoveWindow(window, $event)">
      <span class="icon">
        <i class="fa fa-{{window.icon}}"></i>
        <i class="fa fa-refresh" ng-click="refreshWindow(window)"></i>
      </span>
      <span class="title">{{window.title}}</span>
      <span class="pull-right" ng-click="closeWindow(window, $e)">
        <i class="fa fa-times"></i>
      </span>
      <div
        class="trans"
        ng-if="!window.open"
        ng-click="openWindow(window)"
        tooltip-right="window.title"></div>
    </header>
    <div class="frame"
      style="height:{{window.height}}px">
      <div class="under-frame" ng-show="underFrame"></div>
      <iframe
        width="{{window.width}}"
        height="{{window.height}}"
        style="opacity: {{ (window.load) ? '1' : '0' }} "
        for-window="{{window.href}}">
      </iframe>
    </div>
  </div>

  <div id="top-panel">

  </div>


  <div class="no-tabs" style="width:{{noTabsWidth}}px"></div>

  <div id="frames" fw-loading="loadFrame">
    <div class="under-frame" ng-show="underFrame"></div>
    <iframe width="100%" height="100%"
      ng-repeat="tab in tabs"
      data-src="{{tab.href}}"
      ng-show="tab.load"
      border="0"
      ng-class="{fadeInLeft:tab.select, fadeOutRight:!tab.select, animated:tab.load}"
    ></iframe>
  </div>


</div>
