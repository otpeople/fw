app.controller 'tabsCtrl',
($scope, $http, $timeout, $rootScope, $localStorage) ->
  $scope.tabs = []
  #получить все вкладки
  do $scope.getTabs = ->
    $http.get('/index/gettabs').success (data) ->
      $scope.tabs = []
      $scope.windows = []
      top = 41
      left = 0
      width = 40
      for tab in data
        if $(window).width() <= 768
          $scope.tabs.push tab
        else if tab.type == 'tab'
          $scope.tabs.push tab
        else if tab.type == 'window'
          tab.load = no
          $scope.windows.push tab
      $scope.refreshTabs()
      $scope.refreshWindows()
      $timeout ->
        $scope.openTab(data[0])
      , 300
  $scope.openTab = (tab) ->
    # если на мобильном устройстве
    if $(window).width() <= 768
      # закрываем все вкладки
      $scope.tabs.each (tab) ->
        tab.load = no
        tab.open = no
        tab.select = no
        $('#frames iframe[src="'+tab.href+'"]').removeAttr('src')

    #помечаем все прочие вкладки как не выбранные
    for t in $scope.tabs then t.select = no
    #помечаем эту вкладку как выбранную
    tab.select = yes

    #если вкладка ранее не была открыта - открываем
    unless tab.open
      tab.open = yes
      #если вкладка(фрейм) не загружен
      #unless tab.load
      $scope.loadFrame = yes
      $timeout ->
        $('#frames iframe[data-src="'+tab.href+'"]').attr('src',tab.href)
        .unbind('load').bind 'load', ->
          $timeout =>
            $scope.loadFrame = no
            el = document.querySelector('#frames iframe[data-src="'+
              $(@).attr('data-src')+'"]')
            scope = angular.element(el).scope()
            scope.$apply ->
              scope.tab.load = yes
          , 500
      , 500
      #обновляем вкладки
      do $scope.refreshTabs


  $scope.closeTab = (tab, $e) ->
    $timeout ->
      tab.open = no
      tab.load = no
      $('#frames iframe[src="'+tab.href+'"]').removeAttr('src')
      #обновляем вкладки
      do $scope.refreshTabs
      #выбираем активной первую попавшуюся открытую вкладку
      if tab.select
        tab.select = no
        for tab in $scope.tabs
          if tab.open
            tab.select = yes
            return false
    ,50

  #рефрешит нужную вкладку
  $scope.refreshTab = (tab) ->
    unless tab.open then return false
    $scope.loadFrame = yes
    tab.load = no
    $('#frames iframe[data-src="'+tab.href+'"]').attr('src',tab.href)
    .unbind('load').bind 'load', ->
      $scope.loadFrame = no
      el = document.querySelector('#frames iframe[data-src="'+
        $(this).attr('data-src')+'"]')
      scope = angular.element(el).scope()
      scope.$apply ->
        scope.tab.load = yes
    return

  $(window).resize ->
    $scope.refreshTabs()
    $scope.refreshWindows()

  #обновляет размеры и расположение вкладок
  $scope.refreshTabs = ->
    # десктопная версия
    if $(window).width() >= 768
      allSize = $(window).width() - 202
      if $(window).width() <= 600
        allSize += 122
      #определяем кол-во открытых вкладок
      opened = 0
      for tab in $scope.tabs
        if tab.open then opened++
      # определяем размер одной открытой вкладки
      oneSize = Math.floor( allSize / opened ) - opened + 1
      left = 41
      top = 0
      x = 0
      for tab in $scope.tabs
        if tab.open
          x++
          tab.width = oneSize
          tab.top = 0
          tab.left = left
          left += oneSize
          if x == opened
            tab.width = allSize
          else
            allSize = allSize - oneSize - 1
        else
          tab.top = top
          tab.width = 40
          tab.left = 0
          top += 41
          if $(window).width() <= 600
            tab.width = 300

    # мобильная версия
    else
      # ширина окна без правой иконки пользователя
      allSize = $(window).width() - 40
      # ширина одной иконки (вкладки)
      oneSize = (allSize / $scope.tabs.length).floor()
      left = 0
      for tab in $scope.tabs
        tab.top = 0
        tab.width = oneSize
        tab.left = left
        left += oneSize
      $scope.tabs.last().width += (allSize - left)
  $scope.openMobileTab = ->
    if $(window).width() <= 600
      $scope.showTabs = !$scope.showTabs


  $scope.refreshWindows = ->
    amount = 0
    for win in $scope.windows
      unless win.open then amount++

    top = $(window).height() - 40 * amount

    for win in $scope.windows
      win.x = 0
      win.y = top
      top++


  # раз в секунду проверяем window
  $scope.tickWindow = (win) ->
    # если окно открыто
    if win.open
      # получаем его iframe и высоту содержимого
      iframe = $('iframe[for-window="'+win.href+'"]')
      height = iframe.contents().find('body').height()
      # делаем такую же высоту у window
      win.height = height - 20
      # смотрим чтобы window не вылезал за свои границы
      $scope.winBoundary(win)
      # через секунду опять проверяем
      $timeout ->
        $scope.tickWindow(win)
      , 1000
    # если окно закрыто - значит не нужно снова повторять
    else
      return no


  # открываем окно
  $scope.openWindow = (win) ->
    unless win.open
      win.transition = yes
      # получаем iframe
      iframe = $('iframe[for-window="'+win.href+'"]')
      # выставялем ему src
      iframe.attr('src', win.href)

      # пытаемся взять x и y в localStorage
      winStorage = $localStorage.pultWindows ? null
      if winStorage?
        if winStorage[win.href]
          win.x = winStorage[win.href].x
          win.y = winStorage[win.href].y
          win.height = winStorage[win.href].h
          win.width = winStorage[win.href].w

      # обновляем положение всех неокрытых окон
      # $scope.refreshWindows()

      # отмечаем что окно открыто
      win.open = yes


      # вешаем обработчик когда iframe загрузится
      iframe.unbind('load').bind 'load', ->

        if iframe.attr('src')
          # получаем ширину содержимого
          win.width  = iframe.contents().find('body').width()

          iframe.contents().find('head').append '<style>html,body{
            overflow:hidden !important;width:100% !important}</style>'

          # запоминаем показатели window (глобальный объект js)
          win.window =
            width: $(window).width()
            height: $(window).height()
          $timeout ->
            win.load = yes
            $scope.tickWindow(win)
          , 1000




    return


  $scope.refreshWindow = (win) ->
    iframe = $('iframe[for-window="'+win.href+'"]')
    iframe.attr 'src', iframe.attr 'src'
    return

  $scope.closeWindow = (win) ->

    win.transition = yes
    iframe = $('iframe[for-window="'+win.href+'"]')
    iframe.removeAttr 'src'
    $timeout ->
      win.load = no
      win.open = no
      $scope.refreshWindows()
    , 100
    return

  # при нажатии на окно - делаем его перетаскиваемым
  $scope.startMoveWindow = (win, e)->
    win.transition = no
    $('html').addClass('no-select')
    if win.open
      $scope.movingWindow = win
      win.window =
        width: $(window).width()
        height: $(window).height()
      win.offset =
        x: e.offsetX
        y: e.offsetY
      # показываем div над iframe
      # чтобы iframe не попадал в фокус при перетаскивании
      $scope.underFrame = yes

  # сохраянем параметры окна в localStorage
  $scope.saveWinStorage = ((win) ->
    if win.open and win.load
      winStorage = $localStorage.pultWindows ? null
      unless winStorage?
        winStorage = {}
      winStorage[win.href] =
        x: win.x
        y: win.y
        w: win.width
        h: win.height
      $localStorage.pultWindows = winStorage
  ).throttle(2000)

  # не дает окну выйти за границу приложения
  $scope.winBoundary = (win) ->
    if win.open
      max =
        x: win.window.width - win.width - 43
        y: win.window.height - win.height - 43
      min =
        x: 41
        y: 41
      if win.x < min.x then win.x = min.x
      else if win.x > max.x then win.x = max.x
      if win.y < min.y then win.y = min.y
      else if win.y > max.y then win.y = max.y
      $scope.saveWinStorage(win)






  # при покидании окна или отжатии прекращаем пеертаскивание
  # $rootScope.mouseleave = (e) ->
  #   $scope.movingWindow = null
  #   $('html').removeClass('no-select')
  #   return
  $rootScope.mouseup = (e) ->
    if $scope.movingWindow?
      $scope.underFrame = no
      $scope.movingWindow = null
      $('html').removeClass('no-select')
    return


  # перетаскивание
  $rootScope.mousemove = ((e)->
    if $scope.movingWindow?
      win = $scope.movingWindow
      win.y = e.pageY - win.offset.y
      win.x = e.pageX - win.offset.x
      $scope.winBoundary(win)
  )
