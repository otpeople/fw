<?php
use fw\core\Assets;

$this->assets('pult');
$this->assets('./main.coffee','./main.styl');
$this->assets('./chat');
$this->assets('./tabs');
$this->assets('./clock');
$this->assets('./mobile');

$this->icon  = "icon.png";
$this->title = $title;

$this->meta([
  'name' => 'viewport',
  'content' => 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
]);

$this->html['ng-app']        = 'app';
$this->body['ng-controller'] = 'mainCtrl';
$this->body['ng-cloak']      = "";
$this->body['ng-init']       = "USER='$user'";
$this->body['ng-mouseup']    = 'mouseup($event)';
$this->body['ng-mousemove']  = 'mousemove($event)';
$this->body['ng-mouseleave'] = 'mouseleave($event)';


?>

<!-- Часы -->
<? $this->require('clock/clock'); ?>

<!-- Вкладки -->
<? $this->require('tabs/tabs'); ?>

<!-- Чат -->
<? $this->require('chat/chat'); ?>


<audio id="new-message-song" src="/songs/new-message.wav"></audio>
