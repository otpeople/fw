#настройка уведослений
app.config (NotificationProvider) ->
  NotificationProvider.setOptions
    delay: 4000



#контроллер чата
window.app.controller 'chatCtrl', ($scope, $http, $timeout, $interval, $sce, Notification) ->
  $scope.open = no
  $scope.emoji =
    selected: null
    openGroup: (title) ->
      for g in @groups
        if g.title == title
          g.selected = yes
          @selected = g
        else
          g.selected = no
    add: (i, group) ->
      for g in @groups
        if g.title == group
          unless g.smiles then g.smiles = []
          unicode = '&#x'+i.toString(16)+';'
          g.smiles.push
            smile: $sce.trustAsHtml unicode
            html: unicode
    groups: [
      {title: 'people',  icon: 'smile-o'}
      {title: 'flora',   icon: 'tree'}
      {title: 'transport',icon: 'truck'}
      {title: 'work',   icon: 'feed'}
    ]

  $scope.emoji.add(i, 'people') for i in [0x1F601..0x1F64F]
  $scope.emoji.add(i, 'people') for i in [0x1F446..0x1F450]
  $scope.emoji.add(i, 'people') for i in [0x1F466..0x1F4AA]
  $scope.emoji.add(i, 'flora') for i in [0x1F331..0x1F384]
  $scope.emoji.add(i, 'flora') for i in [0x1F40C..0x1F43E]
  $scope.emoji.add(i, 'transport') for i in [0x1F680..0x1F6C0]
  $scope.emoji.add(i, 'work') for i in [0x1F4B0..0x1F517]
  $scope.emoji.add(i, 'work') for i in [0x1F3A3..0x1F3F0]
  $scope.emoji.openGroup('people')




  $scope.class = 'off' #класс для панели чата

  $scope.data = {}

  $scope.amount = null #кол-во всех непрочитанных сообщений

  #открыть чат при нажатии на уведомление (передается title пользователя)
  $scope.fromNotification = (title) ->
    for group in $scope.data
      for user in group.users
        if user.title == title
          $scope.class = 'on'
          $scope.dialog.open(user.id, title)

  #показывает/скрывает чат
  $scope.width = 40

  $scope.show = ->
    if $scope.width == 40
      $scope.width = 250
    else
      $scope.width = 40

  #принимает всех участников чата
  $scope.ws.onAllChatUsers = (data) ->
    $scope.data = data
    $scope.updateAmountMessages()
    for group in $scope.data.groups
      for user in group.users
        $scope.users[user.id] =
          title: user.title
          photo: user.photo
          id: user.id
        if user.id == $scope.ws.userId
          $scope.user =
            id: user.id
            photo: user.photo
            title: user.title

  #принимает одно сообщение с сервера
  $scope.ws.onNewMessage = (type, id, data) ->

    #если это сообщение в открытом чате
    if $scope.dialog.type == type and ( Number($scope.dialog.id) == Number(id) or Number($scope.dialog.id) == Number(data.from) )

      #список пользователей, которые прочитали
      unless data.read then data.read = []


      data.text = $sce.trustAsHtml data.text
      data.new = yes
      $timeout ->
        data.new = no
      , 3000

      $scope.dialog.mbScroll()
      $scope.dialog.messages.push data

      if $scope.dialog.sending
        $scope.dialog.sending = false
        $scope.dialog.text = ''

      #говорим что мы прочитали
      if data.from != $scope.ws.userId
        $scope.dialog.read()

      #удаляем пользователей из печатающих
      $scope.dialog.removeTyping(data.from)
      $scope.dialog.updateBody()

    else
      #получаем данные для всей панели чата
      $scope.ws.send('getChatUsers', 1)

      #включаем звук нового сообщения
      $scope.newMessageSong.play()

      #определяем для от кого
      for group in $scope.data.groups
        for user in group.users
          if Number(user.id) == Number(data.from)
            title = user.title
      text = data.text.substr(0, 50)
      if text.length > text.length
        text += '...'


      #если сообщение из комнаты
      if type == 'room'
        for room in $scope.data.rooms
          if room.id == id
            title = room.title + " (#{title})"
      else
        id = data.from

      #выводим уведомление
      Notification.primary({title:title, message:text, type:type, id:id})




  #когда приходят все сообщения с нужным контактом
  $scope.ws.onAllMessages = (type, id, messages, start) ->
    if Number($scope.dialog.id) == Number(id) && $scope.dialog.type == type

      #если сообщений нет - скрываем кнопку "Предыдущие сообщения"
      if !messages[0] then $scope.dialog.mbNewMessages = off

      #если меньше 50 сообщений - тоже убираем "Предыдущие сообщения"
      else if messages.length < 50 then $scope.dialog.mbNewMessages = off


      for mes in messages
        unless mes.read then mes.read = []
        mes.time = mes.time.substr(0, 5)
        mes.text = $sce.trustAsHtml mes.text

      #если это предыдущие сообщения
      if start then messages = _.union($scope.dialog.messages, messages)

      #сортируем по id
      messages = _.sortBy messages, (item) -> item.id

      #передаем в диалог
      $scope.dialog.messages = messages

      #прокручиваем диалог
      $scope.dialog.scroll()

  #когда собеседник прочитал наши сообщения
  $scope.ws.onReadMessages = (type, id, user) ->

    if $scope.dialog.type == type and ( Number($scope.dialog.id) == Number(id) or Number($scope.dialog.id) == Number(user) )
      for mes in $scope.dialog.messages
        if mes.from == $scope.ws.userId
          mes.read.push(user)

  #когда нужно обновить статус участника чата
  $scope.ws.onUpdateUserStatus = (data) ->
    data.user = Number(data.user)
    for group in $scope.data.groups
      for user in group.users
        if Number(user.id) == data.user
          user.status = data.status

  #когда нам печатают
  $scope.ws.onTyping = (type, user, id, noTyping) ->
    #console.log 'typing'
    #console.log 'type ' + type
    #console.log 'user ' + user
    #console.log 'id ' + id

    #если диалогове окно не открыто
    unless $scope.dialog.id then return false

    #если это в открытом диалоговом окне
    if (type == 'user' && Number($scope.dialog.id) == Number(user) && $scope.dialog.type == type) || ( Number($scope.dialog.id) == Number(id) && $scope.dialog.type == type && type == 'room')


      #если пользователь сообщает что больше не печатает
      if noTyping
        $scope.dialog.removeTyping(user)

      #если пользователь печатает
      else
        user = $scope.users[user]
        user.time = 7

        if user in $scope.dialog.typingUsers
          for u in $scope.dialog.typingUsers
            if u.title = user.title
              u.time = 7
        else
          $scope.dialog.typingUsers.push user
          $scope.dialog.mbScroll()


  $scope.hideNotification = (n) ->
    for one, id in $scope.notifications
      if one == n
        $scope.notifications.splice(id, 1)


  #обновляет кол-во новых сообщений
  $scope.updateAmountMessages = ->
    amount = 0
    for data in $scope.data.groups
      for user in data.users
        if user.new
          amount +=Number(user.new)
    for data in $scope.data.rooms
      for user in data.users
        if user.new
          amount +=Number(user.new)
    $scope.amount = amount

  #рассылка
  $scope.newMany = ->

    $scope.dialog.open('many', 'Рассылка')

  $scope.users = {}


  #диалог с одним участником и все что с этим связано
  $scope.dialog =
    title: ""    #заголовок чата
    id: 0      #id собеседника
    messages:[]    #сообщения
    disabled: true  #окно чата скрыто
    footer: 30
    header: 30
    sending: off
    height: no
    mbNewMessages: on
    smiles: off
    range: null
    full: yes
    #открывает окно чата с нужным собеседником
    open: (@type, @id, @title, @icon) ->
      $scope.open = no
      @mbNewMessages = on
      if @id == 'many'
        @messages = []
      else
        @getMessages()
        $timeout =>
          $('#dialog .panel-footer .text').get(0).focus()
          @updateBody()
        , 100

      #говорим, что мы прочитали все сообщения
      @read()

    #прокручивает сообщения в чате
    scroll: ->
      $timeout =>
        #если нужно прокрутить до определенного
        if @height
          @body.scrollTop = @body.scrollHeight - @height
          @height = no
        #иначе прокручиваем до конца
        else
          @body.scrollTop = @body.scrollHeight
      , 1

    #когда мы прочитали сообщение
    read: ->

      #отправляем информацию об этом на сервер
      $scope.ws.send('readMessages', @type, @id)

      #обновляем в панели чата кол-во сообщений
      for group in $scope.data
        for user in group.users
          if user.id == id and user.new?
            user.new = null
      $scope.updateAmountMessages()

    #проверяет, нужно ли прокрутить сообщения в чате
    mbScroll: ->
      if @body.scrollHeight - @body.scrollTop - $(@body).height() < 150
        @scroll()

    #когда в процессе загрузки чего-нибудь
    inProcess: no

    #получает все сообщения для этого чата
    getMessages: ->
      $scope.ws.send('getAllMessages', @type, @id)

    #получить все предыдущие сообщения этого чата
    getLastMessages: ->
      #находим самое последнее сообщение
      mesDiv = $('#dialog .message').first()
      mesId = mesDiv.attr('data-id')

      #запоминаем ширину окна
      @height = @body.scrollHeight

      #отправляем запрос серверу
      $scope.ws.send('getAllMessages', @type, @id, mesId)

    #при нажатии на enter - отправляет сообщение
    keydown: (e) ->
      if !e.ctrlKey and !e.shiftKey and e.keyCode == 13
        @send()
      else
        $timeout =>
          @sel = window.getSelection()
          @range = window.getSelection().getRangeAt(0)
          @typing()
        , 10
      @updateBody()

    #отсылаем команду, что мы печатаем сообщение (срабатывает не чаще чем раз в 5 сек)
    typing: _.throttle ->
      if $('#text').html() # если использовать @text - то работает через раз
        $scope.ws.send('typing', @type, @id)
      else
        $scope.ws.send('typing', @type, @id, true)
    , 3000


    #массив пользователей, которые сейчас печатают сообщения
    typingUsers: []

    #удаляет пользователя из печатающих
    removeTyping: (id) ->
      users = []
      for user in @typingUsers
        if Number(user.id) != Number(id) then users.push user
      @typingUsers = users


    #обновляет высоту body в диалоговом окне
    updateBody: ->
      $timeout =>
        #вычисляем высоту футера и хедера
        @footer = $(@element).find('.panel-footer').outerHeight()
        @header = $(@element).find('.panel-heading').outerHeight()
        @mbScroll()
      , 50

    #добавить смайлик
    addSmile: (smile) ->

      #добавляем смайлик
      @text = $sce.trustAsHtml @text+smile

      #переводим курсор в конец
      $timeout ->
        el = document.getElementById("text")
        #el.focus();
        if typeof window.getSelection? && typeof document.createRange?
          range = document.createRange()
          range.selectNodeContents(el)
          range.collapse(false)
          sel = window.getSelection();
          sel.removeAllRanges();
          sel.addRange(range);
        else if typeof document.body.createTextRange?
          textRange = document.body.createTextRange()
          textRange.moveToElementText(el)
          textRange.collapse(false)
          textRange.select()
      , 50

    #отправляет сообщение собеседнику
    send: ->

      #убираем пустые строки
      text = []
      all = $("#text").html().split('<br>')
      for one, i in all
        if one.length
          text.push one
        else if all[i+1]? and all[i+1].length
          text.push one


      text = text.join('<br>')

      #если сообщение пустое - не отправляем
      unless text.length then return no

      #если сообщение для пользователя
      if @type == 'user'
        @sending = yes
        $scope.ws.send('newMessage', @type, @id, text)
        @scroll()

      #если сообщение для группы
      else if @type == 'room'
        @sending = yes
        $scope.ws.send('newMessage', @type, @id, text)
        @scroll()

  $timeout ->
    $scope.dialog.element = $('#dialog').get(0)
    $scope.dialog.body = $('#dialog').find('.panel-body').get(0)
    $scope.newMessageSong = $('#new-message-song').get(0)
  , 500



  #раз в 500 м/с обращаем значение $scope.ticker
  $scope.ticker = off
  $interval ->
    $scope.ticker = !$scope.ticker

    #проходимя по пользователям, которые нам печатают
    for user in $scope.dialog.typingUsers
      user.time--
      #удаляем тех, от которых давно не было инфы
      if user.time == 0 then $scope.dialog.removeTyping user.id
  , 500

  #выход из пользователя
  $scope.logout = ->
    window.location = '/index/logout'

window.openDialog = (title, type) ->
  title = title.split(' (')[0]
  setTimeout ->
    $('#chats .room').add('#chats .user').each ->
      if $(this).find('.title').text() == title
        $(this).trigger('click')
  , 200
