function surroundSelection(textBefore, textAfter) {
	
	//добаваляем фокус
	$('#text').focus();
	
	
    if (window.getSelection) {
        var sel = window.getSelection();
        if (sel.rangeCount > 0) {
            var range = sel.getRangeAt(0);
            var startNode = range.startContainer, startOffset = range.startOffset;
            var boundaryRange = range.cloneRange();
            //var startTextNode = document.createTextNode(textBefore);
            var el = document.createElement('span');
			el.innerHTML = textBefore;
			var startTextNode = el;
			
            //var endTextNode = document.createTextNode(textAfter);
            boundaryRange.collapse(false);
            //boundaryRange.insertNode(endTextNode);
            boundaryRange.setStart(startNode, startOffset);
            boundaryRange.collapse(true);
            boundaryRange.insertNode(startTextNode);
            
            // Reselect the original text
            range.setStartAfter(startTextNode);
            //range.setEndBefore(endTextNode);
            sel.removeAllRanges();
            sel.addRange(range);
        }
    }
}