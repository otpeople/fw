<div ng-controller="chatCtrl" ng-clock id="chat-ctrl">

  <!-- Кнопка для открытия чата -->
  <div id="btn-chat" ng-click="open=!open">

    <img ng-src="{{user.photo}}">
    <div class="new" ng-if="amount">{{amount}}</div>

  </div>
  <!-- Кнопка для открытия чата -->


  <!-- Панель чата -->
  <div id="chats" ng-class="{open:open}">

    <div class="about">
      <span title="Рассылка" data-placement="left" class="icon"><i class="fa fa-paper-plane"></i></span>
      <button class="btn btn-danger" id="logout" ng-click="logout()">Выйти</button>
    </div>


    <!-- Перебор комнат -->
    <div ng-repeat="room in data.rooms">
      <div class="room" ng-click="dialog.open('room', room.id, room.title, room.icon)">
        <span class="icon" title="{{room.title}}" data-placement="left">
          <i class="{{room.icon}}"></i>
          <span class="new" ng-show="room.new">{{room.new}}</span>
        </span>
        <span class="title">{{room.title}}</span>
      </div>
    </div>

    <!-- Перебор групп -->
    <div ng-repeat="group in data.groups">
      <div class="group">
<!--
        <span class="icon">
          <i class="{{group.icon}}"></i>
        </span>
-->
        <span class="title">{{group.title}}</span>
      </div>
      <div class="user user{{user.id}}" ng-repeat="user in group.users" ng-click="dialog.open('user', user.id, user.title, user.photo)" ng-if="user.id != USER">
        <div style="position: relative">

          <span class="icon" title="{{user.title}}" data-placement="left">
            <img ng-src="{{user.photo}}">
            <span class="new" ng-show="user.new">{{user.new}}</span>
            <span class="online" ng-show="user.status"></span>
          </span>

          <span class="title">{{user.title}}</span>
        </div>
      </div>
    </div>
  </div>
  <!-- Панель чата -->




  <!-- Диалог -->
  <div id="dialog" class="panel panel-primary animated" ng-class="{fadeInRight: dialog.id, fadeOutRight: !dialog.id, full: dialog.full}" style="right: {{width + 10}}px">

    <!-- Шапка -->
    <div class="panel-heading">
      <span class="icon">
        <i class="{{dialog.icon}}" ng-if="dialog.type == 'room'"></i>
        <img ng-src="{{dialog.icon}}" ng-if="dialog.type == 'user'">
      </span>
      <span class="title">{{dialog.title}}</span>
      <div class="pull-right close" ng-click="dialog.id=null"><i class="fa fa-times"></i></div>
      <div class="pull-right full" ng-click="dialog.full=true" ng-show="!dialog.full"><i class="fa fa-expand"></i></div>
      <div class="pull-right no-full" ng-click="dialog.full=false" ng-show="dialog.full"><i class="fa fa-compress"></i></div>
    </div>

    <!-- Тело -->
    <div class="panel-body" style="height: calc(100% - {{dialog.footer + dialog.header}}px)" fw-loading="!dialog.messages">


      <br>
      <br ng-if="!dialog.mbNewMessages">

      <a class="last-messages" ng-click="dialog.getLastMessages()" ng-if="dialog.mbNewMessages">
        Предыдущие сообщения
      </a>

      <div class="message animated fadeInLeft" ng-repeat="message in dialog.messages" data-id="{{message.id}}">

        <div style="margin-top: -6px" ng-if="dialog.messages[$index-1].from == message.from"></div>


        <div class="date" ng-if="!dialog.messages[$index-1] || dialog.messages[$index-1].date != message.date">
          <hr>
          <span>{{message.date | moment:'DD MMMM YYYY'}}</span>
        </div>

        <!-- Мои сообщения -->
        <div ng-if="message.from == USER" class="my cloud">
          <span ng-bind-html="message.text"></span>
          <i class="read fa fa-check" ng-if="message.read[0]"></i>
          <span class="time">{{message.time.substr(0,5)}}</span>
        </div>

        <!-- Сообщения собеседника -->
        <div ng-if="message.from != USER && dialog.messages[$index-1].from != message.from" class="title"><i>{{users[message.from].title}}</i></div>
        <div ng-if="message.from != USER" class="other cloud">
          <div ng-if="dialog.messages[$index-1].from != message.from" class="photo"><img ng-src="{{users[message.from].photo}}"></div>
          <span ng-bind-html="message.text"></span>
          <span class="time">{{message.time.substr(0,5)}}</span>
        </div>
      </div>

      <div class="typing" ng-repeat="user in dialog.typingUsers">
        <img ng-src="{{user.photo}}">
        <span>Набирает сообщение...</span>
      </div>





    </div>

    <!-- Смайлики -->
    <div class="smile-dialog" ng-show="dialog.smiles" ng-mouseleave="dialog.smiles=false">
      <div class="smiles">
        <span class="smile" ng-repeat="smile in emoji.selected.smiles" ng-bind-html="smile.smile" ng-click="dialog.addSmile(smile.html)"></span>
      </div>
      <div class="groups">
         <span ng-repeat="group in emoji.groups" ng-click="emoji.openGroup(group.title)" ng-class="{selected:group.selected}">
           <i class="fa fa-{{group.icon}}"></i>
         </span>
      </div>
    </div>

    <!-- Подвал -->
    <div class="panel-footer">

      <!-- Ввод -->
      <div class="text" id="text"
        contenteditable class="form-control" id="dialog-text" placeholder="Сообщение"
        ng-model="dialog.text" ng-keydown="dialog.keydown($event)">
      </div>

      <!-- Кнопка отправки
      <div class="btn btn-primary" ng-click="dialog.send()" ng-disabled="!dialog.text || dialog.sending">
        <i class="fa fa-paper-plane"></i>
      </div>
      -->

      <div class="icon"><i class="fa fa-link"  style="color: 0.8em"></i></div>
      <div class="icon" ng-mouseover="dialog.smiles=true"><i class="fa fa-smile-o"></i></div>





    </div>

  </div>
  <!-- Диалог -->


  <!-- Уведомления -->
  <div class="notifications">
    <div class="panel panel-warning" ng-repeat="one in notification" ng-click="hideNotification(one)">
      <div class="panel-heading">{{one.title}}<span class="pull-right close">&times;</span></div>
      <div class="panel-body" ng-click="class='on'" ng-bind-html="one.text"></div>
    </div>
  </div>
  <!-- Уведомления -->

  <script type="text/ng-template" id="angular-ui-notification.html">
    <div class="ui-notification" onclick="openDialog($(this).attr('data-title'), $(this).attr('data-type'))" data-title="{{title}}" data-type="{{type}}">
      <h3 ng-show="title" ng-bind-html="title"></h3>
      <div class="message" ng-bind-html="message"></div>
    </div>
  </script>


</div>
