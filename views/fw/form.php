<?php
if(isset($title)) {
  $this->title = $title;
}


if(!isset($width)) { $width = 1000; }

$this->body['style'] = "width:{$width}px; min-height:400px; overflow:hidden";


?>

<? if(isset($form)): ?>
  <div class="container-fluid">
    <?= $form; ?>
  </div>
<? endif; ?>
