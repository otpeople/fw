<?php

$this->title = $title;
$this->assets('bootstrap');
$this->code = 503;

?>

<div class="jumbotron">
  <div class="container">
    <h2><?= $title ?></h2>
    <p><?= str_replace("\n", '<br>', $message) ?></p>
  </div>
</div>
