<?php
$this->assets('cropper');
?>


<style>
html, body {
	overflow: hidden;
	height: 100%;
	background-color: transparent;
	box-sizing: border-box;
}
body {
	padding: 40px 100px 170px 100px;
}
#img {
	height: 100%;
}
#preview {
	position: absolute;
	bottom: 37px;
	right: 3px;
	height: 150px;
	width: 150px;
	z-index: 22222;
	overflow: hidden;
	border-top: solid 2px #777;
	border-left: solid 2px #777;
}
#save {
	position: absolute;
	bottom: 3px;
	right: 3px;
	width: 150px;
	display: block;
	box-sizing: border-box;
	-webkit-writing-mode: horizontal-tb;
	text-indent: 0px;
	text-shadow: none;
	letter-spacing: normal;
	word-spacing: normal;
	align-items: flex-start;
	font: inherit;
	margin: 0;
	text-transform: none;
	-webkit-appearance: button;
	display: inline-block;
	margin-bottom: 0;
	font-weight: normal;
	text-align: center;
	vertical-align: middle;
	touch-action: manipulation;
	cursor: pointer;
	background-image: none;
	border: 1px solid transparent;
	white-space: nowrap;
	color: #fff;
	background-color: #337ab7;
	border-color: #2e6da4;
	padding: 5px 10px;
	box-shadow: none !important;
	outline: none !important;
}
@media screen and (max-width: 768px) {
	body {
		padding: 15px 15px 50px 15px;
	}
	#save {
		width: 100%;
		width: calc(100% - 6px);
	}
	#preview {
		display: none;
	}
}
.cropper-drag-box {
	background-color: transparent;
}
</style>

<img id="img" src="<?= $img ?>">
<div id="preview"></div>
<button id="save">Сохранить</button>


<script>
document.addEventListener('DOMContentLoaded', function() {

	var img = $('#img');


	img.cropper({
		aspectRatio: false,
		crop: function(data) {},
		preview : "#preview",
		zoomable : false,
		autoCrop: false,
		background: false,
	});


	$('#save').click(function() {

		var data = img.cropper("getData");

		data = JSON.stringify(data);

		$.ajax({
			url : "/fw/cropFile/<?= $crypt ?>",
			type: "post",
			data : "data="+data,
			success:function(data) {


				$('iframe', top.document).each(function() {
					$(this).contents().find('.fw-form-files-get').trigger('click');
				});


				setTimeout(function() {
					$('#fw-full-back .close', top.document).trigger('click');
				}, 300);



			}
		});

	});

});
</script>
