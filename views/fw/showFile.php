<?php
$this -> assets("fontawesome");
$this -> assets("jquery");
?>
<style type="text/css">
<!--
html { height: 100%; margin: 0; padding: 0; }
body {
	height: 100%;
	margin: 0;
	padding: 0;
	overflow: hidden;
	-webkit-user-select:none;
}
div.img {
	height: 90%;
	width: 90%;
	margin: 5% auto;
	background-image: url(<?= $img ?>);
	background-position: center center;
	background-repeat: no-repeat;
	background-size: contain;
	max-width: <?= $width ?>px;
	max-height: <?= $height ?>px;
}
.panel {
	top:0px;
	margin: 0 auto;
	width: 100%;
	height: 40px;
	text-align: center;
	margin-bottom: -40px;
}
.panel > * {
	display:inline-block !important;
	font-size: 2em !important;
	margin: 5px !important;
	margin-left: 10px !important;
	cursor: pointer !important;
	color: #eee !important;
	text-decoration: none !important;
}
</style>

<div class="panel">
	<i class="fa fa-print" onclick="printThis()" title="Печать"></i>
	<a class="fa fa-download" title="Скачать" download href="<?= $img ?>"></a>
</div>
<div class="img"></div>

<script>
	var printThis = function() {
		$('.panel').hide();
		window.print();
		$('.panel').show();
	}
</script>
