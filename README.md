php framework from mrMasly

### 1. Указываем репозиторий фреймворка в composer.json: ###

```
#!json
{
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:otpeople/fw.git"
        }
    ]
}
```
### 2. Подключаем фреймворк ###

```
#!bash
composer require mrmasly/fw
```
или в файле composer.json
```
#!json
{
    "require": {
		"mrmasly/fw": "dev-master"
    }
}
```


### 3. Создаем файр index.php в корне приложения (куда ссылается домен), а в composer autoload указываем название нашего приложения ### 
(в данном примере сайт 'mysite.ru' будет являться приложением 'app')

index.php
```
#!php
<?php
	require('vendor/autoload.php');
	fw\core\App::init([
		'mysite.ru' => 'site',
	]);
?>
```

composer.json
```
#!json
{
    "autoload": {
        "psr-4": {
            "app\\": "app"
        }
    }
}
```

### 4. Запускаем по этому домену index.php и выполняем требуемые действия ###